const db = require('./firestore-connect')

const departments = require('./departments.db')
const provinces = require('./provinces.db')
const districts = require('./districts.db')

const deleteCollection = (collName, deleted) => {
    db.collection(collName).get().then(snapshot => {
        snapshot.forEach(doc => {
            db.collection(collName).doc(doc.id).delete().then(() => {
                if (deleted) deleted()
            })
        })
    })
}

const addDocument = (collName, document) => {
    db.collection(collName)
        .doc(document.id)
        .set(document)
        .then(function(docRef) {
            console.log(`[${collName}] document written`, document.id);
        })
        .catch(function(error) {
            console.error(`[${collName}] Error adding document: `, error);
        })
}

deleteCollection('districts')
deleteCollection('departments')
deleteCollection('provinces')

const importFromObject = (collName, object) => {
    if (object && Object.keys(object).length > 0) {
        for (let p in object) {
            if (object.hasOwnProperty(p)) {
                if (object[p].length > 0) {
                    object[p].forEach(obj => {
                        addDocument(collName, obj)
                    })
                }
            }
        }
    }
}

const importDepartments = () => {
    if (departments && departments.length > 0) {
        departments.forEach(obj => {
            addDocument('departments', obj)
        })
    }
}

const importProvinces = () => {
    importFromObject('provinces', provinces)
}

const importDistricts = () => {
    importFromObject('districts', districts)
}

importDepartments()
importProvinces()
importDistricts()

