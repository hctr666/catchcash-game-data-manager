; (function (fb, dom) {

   // Your web app's Firebase configuration
   var firebaseConfig = {
      apiKey: "AIzaSyCXimXWXR4Q3VPe37dbCmkswUgXkCXyPx0",
      authDomain: "billeton-sodimac-c5271.firebaseapp.com",
      databaseURL: "https://billeton-sodimac-c5271.firebaseio.com",
      projectId: "billeton-sodimac-c5271",
      storageBucket: "billeton-sodimac-c5271.appspot.com",
      messagingSenderId: "626174022976",
      appId: "1:626174022976:web:2eaccedc70907c47"
   };

   // Initialize Firebase
   fb.initializeApp(firebaseConfig);

   // Anon auth
   fb.auth().signInAnonymously().catch(function (error) {
      console.error(error)
   });

   var auth = fb.auth()
   var db = fb.app().firestore();

   var totalRegParticipantsEl = dom.querySelector('#total-reg-participants .js-value'),
      totalGiftcardsComsumedEl = dom.querySelector('#total-gitfcards-consumed .js-value');

   function getTotalParticipants(cb) {
      auth.onAuthStateChanged(user => {
         if (user) {
            return db.collection('participants')
               .onSnapshot(function (snapshot) {
                  if (cb) cb(snapshot.size);
               });
         }
      })
   }

   function getTotalGiftcardsConsumed(cb) {
      auth.onAuthStateChanged(user => {
         if (user) {
            return db.collection('users')
               .onSnapshot(function (snapshot) {
                  let totalAmount = 0
                  snapshot.forEach(function (doc) {
                     const { giftcardAmount } = doc.data();
                     if (typeof giftcardAmount === "number") {
                        totalAmount += giftcardAmount
                     }
                  });

                  if (cb) cb(totalAmount);
               });
         }
      })
   }

   function renderTotals() {
      getTotalParticipants(function (value) {
         if (totalRegParticipantsEl) {
            totalRegParticipantsEl.innerHTML = value;
            totalRegParticipantsEl.nextElementSibling.classList.add('loader-hidden')
         }
      });

      /*getTotalGiftcardsConsumed(function (total) {
         if (totalGiftcardsComsumedEl) {
            totalGiftcardsComsumedEl.innerHTML = total
            totalGiftcardsComsumedEl.nextEq2qlementSibling.classList.add('loader-hidden')
         }
      }); */
   }

   renderTotals();

})(firebase, document); 