const dotenv = require('dotenv')
const admin = require('firebase-admin')

dotenv.config()

const ENV = process.env.NODE_ENV
const serviceAccountFile = ENV === "development" ?
    './../catchcash-game-dev-7a7ed-firebase-adminsdk-2toop-a3953c7cfc.json' :
    './../billeton-sodimac-c5271-firebase-adminsdk-f2vjg-5e9b6d75b6.json'

const dataBaseURL = ENV === "development" ? process.env.FIREBASE_DATABASE_URL_DEV : process.env.FIREBASE_DATABASE_URL
const serviceAccount = require(serviceAccountFile)

const config = {
   credential: admin.credential.cert(serviceAccount),
   dataBaseURL: dataBaseURL
}

admin.initializeApp(config)

const db = admin.firestore()

const settings = {timestampsInSnapshots: true};

db.settings(settings)

module.exports = db