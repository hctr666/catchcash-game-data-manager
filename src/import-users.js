const db = require('./firestore-connect')

const users = require('./users.db')
const DB_COLLECTION = 'users'

const addUsers = (users) => {
   if (users && users.length > 0) {
      users.map((data, index) => {
         const object = {
            document: data.document && data.document.toString(),
            attemptsAllowed: data.attemptsAllowed,
            attemptsRemaining: data.attemptsAllowed,
            attemptsDone: 0,
            active: true,
         }

         db.collection(DB_COLLECTION)
            .doc(`${object.document}`)
            .set(object)
            .then(function() {
                console.log(`[${DB_COLLECTION}] user written`, object.document);
            })
            .catch(function(error) {
                console.error(`[${DB_COLLECTION}] Error adding document: `, error);
            })
      })
   }
}


users.then(data => {
   addUsers(data)
})
