const db = require('./firestore-connect')
const ObjToCSV = require('objects-to-csv')
const path = require('path')
const fs = require('fs')

const COLLECTION = 'purchases'
const OUTPUT_FILE = './output/purchases.db.csv'

// store process arguments
//const args = process.argv.slice(2)

const outputPath = path.resolve(OUTPUT_FILE)

const exportToCSV = async (dataArray) => {
   let csv = new ObjToCSV(dataArray)

   fs.exists(outputPath, async exists => {
      exists ?
         fs.unlink(outputPath, async () => {
            await csv.toDisk(OUTPUT_FILE)
         }) :
         await csv.toDisk(OUTPUT_FILE)
   })

   console.log(await csv.toString());
}

const getPurchases = () => {
   db.collection(COLLECTION)
      .get()
      .then(snapshot => {
         if (!snapshot.empty) {
            let dataArray = []
            snapshot.forEach(doc => {
               const {
                  purchaseCode,
                  ticketSerial,
                  purchaseDate,
                  purchaseAmount,
                  paidwCMRChecked,
                  document,
                  created,
               } = doc.data()
               dataArray.push({
                  'Documento ID': document,
                  'Número ticket': purchaseCode,
                  'Serial ticket': ticketSerial,
                  'Fecha de compra': purchaseDate,
                  'Monto total': purchaseAmount,
                  '¿Pagó con CMR?': paidwCMRChecked ? 'sí' : 'no',
                  'Fecha registro': created
               })
            })

            //console.log(dataArray)
            exportToCSV(dataArray)
         }
      })
}

getPurchases()