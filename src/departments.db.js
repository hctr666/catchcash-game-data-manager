module.exports = [
   {
      "id": "2534",
      "name": "Amazonas",
      "code": "01",
      "label": "Amazonas, Per\u00fa",
      "search": "amazonas per\u00fa",
      "children_count": "7",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "2625",
      "name": "Ancash",
      "code": "02",
      "label": "Ancash, Per\u00fa",
      "search": "ancash per\u00fa",
      "children_count": "20",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "2812",
      "name": "Apurimac",
      "code": "03",
      "label": "Apurimac, Per\u00fa",
      "search": "apurimac per\u00fa",
      "children_count": "7",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "2900",
      "name": "Arequipa",
      "code": "04",
      "label": "Arequipa, Per\u00fa",
      "search": "arequipa per\u00fa",
      "children_count": "8",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "3020",
      "name": "Ayacucho",
      "code": "05",
      "label": "Ayacucho, Per\u00fa",
      "search": "ayacucho per\u00fa",
      "children_count": "11",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "3143",
      "name": "Cajamarca",
      "code": "06",
      "label": "Cajamarca, Per\u00fa",
      "search": "cajamarca per\u00fa",
      "children_count": "13",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "3292",
      "name": "Cusco",
      "code": "08",
      "label": "Cusco, Per\u00fa",
      "search": "cusco per\u00fa",
      "children_count": "13",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "3414",
      "name": "Huancavelica",
      "code": "09",
      "label": "Huancavelica, Per\u00fa",
      "search": "huancavelica per\u00fa",
      "children_count": "7",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "3518",
      "name": "Huanuco",
      "code": "10",
      "label": "Huanuco, Per\u00fa",
      "search": "huanuco per\u00fa",
      "children_count": "11",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "3606",
      "name": "Ica",
      "code": "11",
      "label": "Ica, Per\u00fa",
      "search": "ica per\u00fa",
      "children_count": "5",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "3655",
      "name": "Junin",
      "code": "12",
      "label": "Junin, Per\u00fa",
      "search": "junin per\u00fa",
      "children_count": "9",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "3788",
      "name": "La Libertad",
      "code": "13",
      "label": "La Libertad, Per\u00fa",
      "search": "la libertad per\u00fa",
      "children_count": "12",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "3884",
      "name": "Lambayeque",
      "code": "14",
      "label": "Lambayeque, Per\u00fa",
      "search": "lambayeque per\u00fa",
      "children_count": "3",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "3926",
      "name": "Lima",
      "code": "15",
      "label": "Lima, Per\u00fa",
      "search": "lima per\u00fa",
      "children_count": "10",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "4108",
      "name": "Loreto",
      "code": "16",
      "label": "Loreto, Per\u00fa",
      "search": "loreto per\u00fa",
      "children_count": "6",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "4165",
      "name": "Madre de Dios",
      "code": "17",
      "label": "Madre de Dios, Per\u00fa",
      "search": "madre de dios per\u00fa",
      "children_count": "3",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "4180",
      "name": "Moquegua",
      "code": "18",
      "label": "Moquegua, Per\u00fa",
      "search": "moquegua per\u00fa",
      "children_count": "3",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "4204",
      "name": "Pasco",
      "code": "19",
      "label": "Pasco, Per\u00fa",
      "search": "pasco per\u00fa",
      "children_count": "3",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "4236",
      "name": "Piura",
      "code": "20",
      "label": "Piura, Per\u00fa",
      "search": "piura per\u00fa",
      "children_count": "8",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "4309",
      "name": "Puno",
      "code": "21",
      "label": "Puno, Per\u00fa",
      "search": "puno per\u00fa",
      "children_count": "13",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "4431",
      "name": "San Martin",
      "code": "22",
      "label": "San Martin, Per\u00fa",
      "search": "san martin per\u00fa",
      "children_count": "10",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "4519",
      "name": "Tacna",
      "code": "23",
      "label": "Tacna, Per\u00fa",
      "search": "tacna per\u00fa",
      "children_count": "4",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "4551",
      "name": "Tumbes",
      "code": "24",
      "label": "Tumbes, Per\u00fa",
      "search": "tumbes per\u00fa",
      "children_count": "3",
      "level": "1",
      "parent_id": "2533"
   },
   {
      "id": "4567",
      "name": "Ucayali",
      "code": "25",
      "label": "Ucayali, Per\u00fa",
      "search": "ucayali per\u00fa",
      "children_count": "4",
      "level": "1",
      "parent_id": "2533"
   }
]