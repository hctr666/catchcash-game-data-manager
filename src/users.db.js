const csvToJson = require("csvtojson")
const path = require('path')

const csvFilePath = path.resolve('./src/import-users.csv')

module.exports = csvToJson()
   .fromFile(csvFilePath)
   .then(dataArr => {
      return dataArr
   })
/*module.exports = [
   {
      "document": 72324888,
      "attemptsAllowed": 4
   },
   {
      "document": 15606430,
      "attemptsAllowed": 3
   },
   {
      "document": 15601011,
      "attemptsAllowed": 5
   },
   {
      "document": "07899520",
      "attemptsAllowed": 2
   }
] */