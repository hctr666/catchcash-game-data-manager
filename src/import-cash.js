const db = require('./firestore-connect')

const cashData = require('./cash.db')
const DB_COLLECTION = 'cashAvailable'

const addCashData = (cashData) => {
   if (cashData && cashData.length > 0) {
      cashData.map(({ id, value, total }, index) => {
         const object = {
            total: total,
            value: value
         }

         db.collection(DB_COLLECTION)
            .doc(id)
            .set(object)
            .then(function(docRef) {
                console.log(`[${DB_COLLECTION}] data written`, value);
            })
            .catch(function(error) {
                console.error(`[${DB_COLLECTION}] Error adding cash document: `, error);
            })
      })
   }
}


addCashData(cashData)
