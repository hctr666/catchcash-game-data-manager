const db = require('./firestore-connect')
const ObjToCSV = require('objects-to-csv')
const path = require('path')
const fs = require('fs')

const COLLECTION = 'participants'
const OUTPUT_FILE = './output/participants.db.csv'

// store process arguments
//const args = process.argv.slice(2)

const outputPath = path.resolve(OUTPUT_FILE)

const exportToCSV = async (dataArray) => {
   let csv = new ObjToCSV(dataArray)

   fs.exists(outputPath, async exists => {
      exists ?
         fs.unlink(outputPath, async () => {
            await csv.toDisk(OUTPUT_FILE)
         }) :
         await csv.toDisk(OUTPUT_FILE)
   })

   console.log(await csv.toString());
}

const getPurchases = () => {
   db.collection(COLLECTION)
      .get()
      .then(snapshot => {
         if (!snapshot.empty) {
            let dataArray = []
            snapshot.forEach(doc => {
               const {
                  document,
                  documentType,
                  email,
                  name,
                  surname,
                  phone,
                  department,
                  province,
                  district,
                  totalPurchases,
                  dataprotectionChecked,
                  termsChecked,
                  useofinfoChecked,
                  created,
               } = doc.data()
               dataArray.push({
                  'Tipo documento': documentType,
                  'Documento': document,
                  'Nombres': name,
                  'Apellidos': surname,
                  'Email': email,
                  'Teléfono/Celular': phone,
                  'Departamento': department,
                  'Provincia': province,
                  'Distrito': district,
                  'Compras registradas': totalPurchases,
                  '¿Aceptó política protección datos?': dataprotectionChecked ? 'sí' : 'no',
                  '¿Aceptó términos?': termsChecked ? 'sí' : 'no',
                  '¿Aceptó uso información?': useofinfoChecked ? 'sí' : 'no',
                  'Fecha registro': created
               })
            })

            //console.log(dataArray)
            exportToCSV(dataArray)
         }
      })
}

getPurchases()