module.exports = {
   "2557": [
      {
         "id":"2559",
         "name":"Aramango",
         "code":"02",
         "label":"Aramango, Bagua",
         "search":"aramango bagua",
         "children_count":"0",
         "level":"3",
         "parent_id":"2557"
      },
      {
         "id":"2560",
         "name":"Copallin",
         "code":"03",
         "label":"Copallin, Bagua",
         "search":"copallin bagua",
         "children_count":"0",
         "level":"3",
         "parent_id":"2557"
      },
      {
         "id":"2561",
         "name":"El Parco",
         "code":"04",
         "label":"El Parco, Bagua",
         "search":"el parco bagua",
         "children_count":"0",
         "level":"3",
         "parent_id":"2557"
      },
      {
         "id":"2562",
         "name":"Imaza",
         "code":"05",
         "label":"Imaza, Bagua",
         "search":"imaza bagua",
         "children_count":"0",
         "level":"3",
         "parent_id":"2557"
      },
      {
         "id":"2558",
         "name":"La Peca",
         "code":"01",
         "label":"La Peca, Bagua",
         "search":"la peca bagua",
         "children_count":"0",
         "level":"3",
         "parent_id":"2557"
      }
   ],
   "2563":[
      {
         "id":"2567",
         "name":"Chisquilla",
         "code":"04",
         "label":"Chisquilla, Bongara",
         "search":"chisquilla bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      },
      {
         "id":"2568",
         "name":"Churuja",
         "code":"05",
         "label":"Churuja, Bongara",
         "search":"churuja bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      },
      {
         "id":"2565",
         "name":"Corosha",
         "code":"02",
         "label":"Corosha, Bongara",
         "search":"corosha bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      },
      {
         "id":"2566",
         "name":"Cuispes",
         "code":"03",
         "label":"Cuispes, Bongara",
         "search":"cuispes bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      },
      {
         "id":"2569",
         "name":"Florida",
         "code":"06",
         "label":"Florida, Bongara",
         "search":"florida bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      },
      {
         "id":"2575",
         "name":"Jazan",
         "code":"12",
         "label":"Jazan, Bongara",
         "search":"jazan bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      },
      {
         "id":"2564",
         "name":"Jumbilla",
         "code":"01",
         "label":"Jumbilla, Bongara",
         "search":"jumbilla bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      },
      {
         "id":"2570",
         "name":"Recta",
         "code":"07",
         "label":"Recta, Bongara",
         "search":"recta bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      },
      {
         "id":"2571",
         "name":"San Carlos",
         "code":"08",
         "label":"San Carlos, Bongara",
         "search":"san carlos bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      },
      {
         "id":"2572",
         "name":"Shipasbamba",
         "code":"09",
         "label":"Shipasbamba, Bongara",
         "search":"shipasbamba bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      },
      {
         "id":"2573",
         "name":"Valera",
         "code":"10",
         "label":"Valera, Bongara",
         "search":"valera bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      },
      {
         "id":"2574",
         "name":"Yambrasbamba",
         "code":"11",
         "label":"Yambrasbamba, Bongara",
         "search":"yambrasbamba bongara",
         "children_count":"0",
         "level":"3",
         "parent_id":"2563"
      }
   ],
   "2535":[
      {
         "id":"2537",
         "name":"Asuncion",
         "code":"02",
         "label":"Asuncion, Chachapoyas",
         "search":"asuncion chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2538",
         "name":"Balsas",
         "code":"03",
         "label":"Balsas, Chachapoyas",
         "search":"balsas chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2536",
         "name":"Chachapoyas",
         "code":"01",
         "label":"Chachapoyas, Chachapoyas",
         "search":"chachapoyas chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2539",
         "name":"Cheto",
         "code":"04",
         "label":"Cheto, Chachapoyas",
         "search":"cheto chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2540",
         "name":"Chiliquin",
         "code":"05",
         "label":"Chiliquin, Chachapoyas",
         "search":"chiliquin chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2541",
         "name":"Chuquibamba",
         "code":"06",
         "label":"Chuquibamba, Chachapoyas",
         "search":"chuquibamba chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2542",
         "name":"Granada",
         "code":"07",
         "label":"Granada, Chachapoyas",
         "search":"granada chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2543",
         "name":"Huancas",
         "code":"08",
         "label":"Huancas, Chachapoyas",
         "search":"huancas chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2544",
         "name":"La Jalca",
         "code":"09",
         "label":"La Jalca, Chachapoyas",
         "search":"la jalca chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2545",
         "name":"Leimebamba",
         "code":"10",
         "label":"Leimebamba, Chachapoyas",
         "search":"leimebamba chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2546",
         "name":"Levanto",
         "code":"11",
         "label":"Levanto, Chachapoyas",
         "search":"levanto chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2547",
         "name":"Magdalena",
         "code":"12",
         "label":"Magdalena, Chachapoyas",
         "search":"magdalena chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2548",
         "name":"Mariscal Castilla",
         "code":"13",
         "label":"Mariscal Castilla, Chachapoyas",
         "search":"mariscal castilla chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2549",
         "name":"Molinopampa",
         "code":"14",
         "label":"Molinopampa, Chachapoyas",
         "search":"molinopampa chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2550",
         "name":"Montevideo",
         "code":"15",
         "label":"Montevideo, Chachapoyas",
         "search":"montevideo chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2551",
         "name":"Olleros",
         "code":"16",
         "label":"Olleros, Chachapoyas",
         "search":"olleros chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2552",
         "name":"Quinjalca",
         "code":"17",
         "label":"Quinjalca, Chachapoyas",
         "search":"quinjalca chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2553",
         "name":"San Francisco de Daguas",
         "code":"18",
         "label":"San Francisco de Daguas, Chachapoyas",
         "search":"san francisco de daguas chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2554",
         "name":"San Isidro de Maino",
         "code":"19",
         "label":"San Isidro de Maino, Chachapoyas",
         "search":"san isidro de maino chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2555",
         "name":"Soloco",
         "code":"20",
         "label":"Soloco, Chachapoyas",
         "search":"soloco chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      },
      {
         "id":"2556",
         "name":"Sonche",
         "code":"21",
         "label":"Sonche, Chachapoyas",
         "search":"sonche chachapoyas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2535"
      }
   ],
   "2576":[
      {
         "id":"2578",
         "name":"El Cenepa",
         "code":"02",
         "label":"El Cenepa, Condorcanqui",
         "search":"el cenepa condorcanqui",
         "children_count":"0",
         "level":"3",
         "parent_id":"2576"
      },
      {
         "id":"2577",
         "name":"Nieva",
         "code":"01",
         "label":"Nieva, Condorcanqui",
         "search":"nieva condorcanqui",
         "children_count":"0",
         "level":"3",
         "parent_id":"2576"
      },
      {
         "id":"2579",
         "name":"Rio Santiago",
         "code":"03",
         "label":"Rio Santiago, Condorcanqui",
         "search":"rio santiago condorcanqui",
         "children_count":"0",
         "level":"3",
         "parent_id":"2576"
      }
   ],
   "2580":[
      {
         "id":"2582",
         "name":"Camporredondo",
         "code":"02",
         "label":"Camporredondo, Luya",
         "search":"camporredondo luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2583",
         "name":"Cocabamba",
         "code":"03",
         "label":"Cocabamba, Luya",
         "search":"cocabamba luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2584",
         "name":"Colcamar",
         "code":"04",
         "label":"Colcamar, Luya",
         "search":"colcamar luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2585",
         "name":"Conila",
         "code":"05",
         "label":"Conila, Luya",
         "search":"conila luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2586",
         "name":"Inguilpata",
         "code":"06",
         "label":"Inguilpata, Luya",
         "search":"inguilpata luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2581",
         "name":"Lamud",
         "code":"01",
         "label":"Lamud, Luya",
         "search":"lamud luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2587",
         "name":"Longuita",
         "code":"07",
         "label":"Longuita, Luya",
         "search":"longuita luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2588",
         "name":"Lonya Chico",
         "code":"08",
         "label":"Lonya Chico, Luya",
         "search":"lonya chico luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2589",
         "name":"Luya",
         "code":"09",
         "label":"Luya, Luya",
         "search":"luya luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2590",
         "name":"Luya Viejo",
         "code":"10",
         "label":"Luya Viejo, Luya",
         "search":"luya viejo luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2591",
         "name":"Maria",
         "code":"11",
         "label":"Maria, Luya",
         "search":"maria luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2592",
         "name":"Ocalli",
         "code":"12",
         "label":"Ocalli, Luya",
         "search":"ocalli luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2593",
         "name":"Ocumal",
         "code":"13",
         "label":"Ocumal, Luya",
         "search":"ocumal luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2594",
         "name":"Pisuquia",
         "code":"14",
         "label":"Pisuquia, Luya",
         "search":"pisuquia luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2595",
         "name":"Providencia",
         "code":"15",
         "label":"Providencia, Luya",
         "search":"providencia luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2596",
         "name":"San Cristobal",
         "code":"16",
         "label":"San Cristobal, Luya",
         "search":"san cristobal luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2597",
         "name":"San Francisco del Yeso",
         "code":"17",
         "label":"San Francisco del Yeso, Luya",
         "search":"san francisco del yeso luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2598",
         "name":"San Jeronimo",
         "code":"18",
         "label":"San Jeronimo, Luya",
         "search":"san jeronimo luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2599",
         "name":"San Juan de Lopecancha",
         "code":"19",
         "label":"San Juan de Lopecancha, Luya",
         "search":"san juan de lopecancha luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2600",
         "name":"Santa Catalina",
         "code":"20",
         "label":"Santa Catalina, Luya",
         "search":"santa catalina luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2601",
         "name":"Santo Tomas",
         "code":"21",
         "label":"Santo Tomas, Luya",
         "search":"santo tomas luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2602",
         "name":"Tingo",
         "code":"22",
         "label":"Tingo, Luya",
         "search":"tingo luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      },
      {
         "id":"2603",
         "name":"Trita",
         "code":"23",
         "label":"Trita, Luya",
         "search":"trita luya",
         "children_count":"0",
         "level":"3",
         "parent_id":"2580"
      }
   ],
   "2604":[
      {
         "id":"2606",
         "name":"Chirimoto",
         "code":"02",
         "label":"Chirimoto, Rodriguez de Mendoza",
         "search":"chirimoto rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      },
      {
         "id":"2607",
         "name":"Cochamal",
         "code":"03",
         "label":"Cochamal, Rodriguez de Mendoza",
         "search":"cochamal rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      },
      {
         "id":"2608",
         "name":"Huambo",
         "code":"04",
         "label":"Huambo, Rodriguez de Mendoza",
         "search":"huambo rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      },
      {
         "id":"2609",
         "name":"Limabamba",
         "code":"05",
         "label":"Limabamba, Rodriguez de Mendoza",
         "search":"limabamba rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      },
      {
         "id":"2610",
         "name":"Longar",
         "code":"06",
         "label":"Longar, Rodriguez de Mendoza",
         "search":"longar rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      },
      {
         "id":"2611",
         "name":"Mariscal Benavides",
         "code":"07",
         "label":"Mariscal Benavides, Rodriguez de Mendoza",
         "search":"mariscal benavides rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      },
      {
         "id":"2612",
         "name":"Milpuc",
         "code":"08",
         "label":"Milpuc, Rodriguez de Mendoza",
         "search":"milpuc rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      },
      {
         "id":"2613",
         "name":"Omia",
         "code":"09",
         "label":"Omia, Rodriguez de Mendoza",
         "search":"omia rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      },
      {
         "id":"2605",
         "name":"San Nicolas",
         "code":"01",
         "label":"San Nicolas, Rodriguez de Mendoza",
         "search":"san nicolas rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      },
      {
         "id":"2614",
         "name":"Santa Rosa",
         "code":"10",
         "label":"Santa Rosa, Rodriguez de Mendoza",
         "search":"santa rosa rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      },
      {
         "id":"2615",
         "name":"Totora",
         "code":"11",
         "label":"Totora, Rodriguez de Mendoza",
         "search":"totora rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      },
      {
         "id":"2616",
         "name":"Vista Alegre",
         "code":"12",
         "label":"Vista Alegre, Rodriguez de Mendoza",
         "search":"vista alegre rodriguez de mendoza",
         "children_count":"0",
         "level":"3",
         "parent_id":"2604"
      }
   ],
   "2617":[
      {
         "id":"2618",
         "name":"Bagua Grande",
         "code":"01",
         "label":"Bagua Grande, Utcubamba",
         "search":"bagua grande utcubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2617"
      },
      {
         "id":"2619",
         "name":"Cajaruro",
         "code":"02",
         "label":"Cajaruro, Utcubamba",
         "search":"cajaruro utcubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2617"
      },
      {
         "id":"2620",
         "name":"Cumba",
         "code":"03",
         "label":"Cumba, Utcubamba",
         "search":"cumba utcubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2617"
      },
      {
         "id":"2621",
         "name":"El Milagro",
         "code":"04",
         "label":"El Milagro, Utcubamba",
         "search":"el milagro utcubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2617"
      },
      {
         "id":"2622",
         "name":"Jamalca",
         "code":"05",
         "label":"Jamalca, Utcubamba",
         "search":"jamalca utcubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2617"
      },
      {
         "id":"2623",
         "name":"Lonya Grande",
         "code":"06",
         "label":"Lonya Grande, Utcubamba",
         "search":"lonya grande utcubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2617"
      },
      {
         "id":"2624",
         "name":"Yamon",
         "code":"07",
         "label":"Yamon, Utcubamba",
         "search":"yamon utcubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2617"
      }
   ],
   "2639":[
      {
         "id":"2640",
         "name":"Aija",
         "code":"01",
         "label":"Aija, Aija",
         "search":"aija aija",
         "children_count":"0",
         "level":"3",
         "parent_id":"2639"
      },
      {
         "id":"2641",
         "name":"Coris",
         "code":"02",
         "label":"Coris, Aija",
         "search":"coris aija",
         "children_count":"0",
         "level":"3",
         "parent_id":"2639"
      },
      {
         "id":"2642",
         "name":"Huacllan",
         "code":"03",
         "label":"Huacllan, Aija",
         "search":"huacllan aija",
         "children_count":"0",
         "level":"3",
         "parent_id":"2639"
      },
      {
         "id":"2643",
         "name":"La Merced",
         "code":"04",
         "label":"La Merced, Aija",
         "search":"la merced aija",
         "children_count":"0",
         "level":"3",
         "parent_id":"2639"
      },
      {
         "id":"2644",
         "name":"Succha",
         "code":"05",
         "label":"Succha, Aija",
         "search":"succha aija",
         "children_count":"0",
         "level":"3",
         "parent_id":"2639"
      }
   ],
   "2645":[
      {
         "id":"2647",
         "name":"Aczo",
         "code":"02",
         "label":"Aczo, Antonio Raymondi",
         "search":"aczo antonio raymondi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2645"
      },
      {
         "id":"2648",
         "name":"Chaccho",
         "code":"03",
         "label":"Chaccho, Antonio Raymondi",
         "search":"chaccho antonio raymondi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2645"
      },
      {
         "id":"2649",
         "name":"Chingas",
         "code":"04",
         "label":"Chingas, Antonio Raymondi",
         "search":"chingas antonio raymondi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2645"
      },
      {
         "id":"2646",
         "name":"Llamellin",
         "code":"01",
         "label":"Llamellin, Antonio Raymondi",
         "search":"llamellin antonio raymondi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2645"
      },
      {
         "id":"2650",
         "name":"Mirgas",
         "code":"05",
         "label":"Mirgas, Antonio Raymondi",
         "search":"mirgas antonio raymondi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2645"
      },
      {
         "id":"2651",
         "name":"San Juan de Rontoy",
         "code":"06",
         "label":"San Juan de Rontoy, Antonio Raymondi",
         "search":"san juan de rontoy antonio raymondi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2645"
      }
   ],
   "2652":[
      {
         "id":"2654",
         "name":"Acochaca",
         "code":"02",
         "label":"Acochaca, Asuncion",
         "search":"acochaca asuncion",
         "children_count":"0",
         "level":"3",
         "parent_id":"2652"
      },
      {
         "id":"2653",
         "name":"Chacas",
         "code":"01",
         "label":"Chacas, Asuncion",
         "search":"chacas asuncion",
         "children_count":"0",
         "level":"3",
         "parent_id":"2652"
      }
   ],
   "2655":[
      {
         "id":"2657",
         "name":"Abelardo Pardo Lezameta",
         "code":"02",
         "label":"Abelardo Pardo Lezameta, Bolognesi",
         "search":"abelardo pardo lezameta bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2658",
         "name":"Antonio Raymondi",
         "code":"03",
         "label":"Antonio Raymondi, Bolognesi",
         "search":"antonio raymondi bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2659",
         "name":"Aquia",
         "code":"04",
         "label":"Aquia, Bolognesi",
         "search":"aquia bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2660",
         "name":"Cajacay",
         "code":"05",
         "label":"Cajacay, Bolognesi",
         "search":"cajacay bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2661",
         "name":"Canis",
         "code":"06",
         "label":"Canis, Bolognesi",
         "search":"canis bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2656",
         "name":"Chiquian",
         "code":"01",
         "label":"Chiquian, Bolognesi",
         "search":"chiquian bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2662",
         "name":"Colquioc",
         "code":"07",
         "label":"Colquioc, Bolognesi",
         "search":"colquioc bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2663",
         "name":"Huallanca",
         "code":"08",
         "label":"Huallanca, Bolognesi",
         "search":"huallanca bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2664",
         "name":"Huasta",
         "code":"09",
         "label":"Huasta, Bolognesi",
         "search":"huasta bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2665",
         "name":"Huayllacayan",
         "code":"10",
         "label":"Huayllacayan, Bolognesi",
         "search":"huayllacayan bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2666",
         "name":"La Primavera",
         "code":"11",
         "label":"La Primavera, Bolognesi",
         "search":"la primavera bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2667",
         "name":"Mangas",
         "code":"12",
         "label":"Mangas, Bolognesi",
         "search":"mangas bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2668",
         "name":"Pacllon",
         "code":"13",
         "label":"Pacllon, Bolognesi",
         "search":"pacllon bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2669",
         "name":"San Miguel de Corpanqui",
         "code":"14",
         "label":"San Miguel de Corpanqui, Bolognesi",
         "search":"san miguel de corpanqui bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      },
      {
         "id":"2670",
         "name":"Ticllos",
         "code":"15",
         "label":"Ticllos, Bolognesi",
         "search":"ticllos bolognesi",
         "children_count":"0",
         "level":"3",
         "parent_id":"2655"
      }
   ],
   "2671":[
      {
         "id":"2673",
         "name":"Acopampa",
         "code":"02",
         "label":"Acopampa, Carhuaz",
         "search":"acopampa carhuaz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2671"
      },
      {
         "id":"2674",
         "name":"Amashca",
         "code":"03",
         "label":"Amashca, Carhuaz",
         "search":"amashca carhuaz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2671"
      },
      {
         "id":"2675",
         "name":"Anta",
         "code":"04",
         "label":"Anta, Carhuaz",
         "search":"anta carhuaz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2671"
      },
      {
         "id":"2676",
         "name":"Ataquero",
         "code":"05",
         "label":"Ataquero, Carhuaz",
         "search":"ataquero carhuaz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2671"
      },
      {
         "id":"2672",
         "name":"Carhuaz",
         "code":"01",
         "label":"Carhuaz, Carhuaz",
         "search":"carhuaz carhuaz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2671"
      },
      {
         "id":"2677",
         "name":"Marcara",
         "code":"06",
         "label":"Marcara, Carhuaz",
         "search":"marcara carhuaz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2671"
      },
      {
         "id":"2678",
         "name":"Pariahuanca",
         "code":"07",
         "label":"Pariahuanca, Carhuaz",
         "search":"pariahuanca carhuaz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2671"
      },
      {
         "id":"2679",
         "name":"San Miguel de Aco",
         "code":"08",
         "label":"San Miguel de Aco, Carhuaz",
         "search":"san miguel de aco carhuaz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2671"
      },
      {
         "id":"2680",
         "name":"Shilla",
         "code":"09",
         "label":"Shilla, Carhuaz",
         "search":"shilla carhuaz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2671"
      },
      {
         "id":"2681",
         "name":"Tinco",
         "code":"10",
         "label":"Tinco, Carhuaz",
         "search":"tinco carhuaz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2671"
      },
      {
         "id":"2682",
         "name":"Yungar",
         "code":"11",
         "label":"Yungar, Carhuaz",
         "search":"yungar carhuaz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2671"
      }
   ],
   "2683":[
      {
         "id":"2684",
         "name":"San Luis",
         "code":"01",
         "label":"San Luis, Carlos Fermin Fitzcarrald",
         "search":"san luis carlos fermin fitzcarrald",
         "children_count":"0",
         "level":"3",
         "parent_id":"2683"
      },
      {
         "id":"2685",
         "name":"San Nicolas",
         "code":"02",
         "label":"San Nicolas, Carlos Fermin Fitzcarrald",
         "search":"san nicolas carlos fermin fitzcarrald",
         "children_count":"0",
         "level":"3",
         "parent_id":"2683"
      },
      {
         "id":"2686",
         "name":"Yauya",
         "code":"03",
         "label":"Yauya, Carlos Fermin Fitzcarrald",
         "search":"yauya carlos fermin fitzcarrald",
         "children_count":"0",
         "level":"3",
         "parent_id":"2683"
      }
   ],
   "2687":[
      {
         "id":"2689",
         "name":"Buena Vista Alta",
         "code":"02",
         "label":"Buena Vista Alta, Casma",
         "search":"buena vista alta casma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2687"
      },
      {
         "id":"2688",
         "name":"Casma",
         "code":"01",
         "label":"Casma, Casma",
         "search":"casma casma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2687"
      },
      {
         "id":"2690",
         "name":"Comandante Noel",
         "code":"03",
         "label":"Comandante Noel, Casma",
         "search":"comandante noel casma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2687"
      },
      {
         "id":"2691",
         "name":"Yautan",
         "code":"04",
         "label":"Yautan, Casma",
         "search":"yautan casma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2687"
      }
   ],
   "2692":[
      {
         "id":"2694",
         "name":"Aco",
         "code":"02",
         "label":"Aco, Corongo",
         "search":"aco corongo",
         "children_count":"0",
         "level":"3",
         "parent_id":"2692"
      },
      {
         "id":"2695",
         "name":"Bambas",
         "code":"03",
         "label":"Bambas, Corongo",
         "search":"bambas corongo",
         "children_count":"0",
         "level":"3",
         "parent_id":"2692"
      },
      {
         "id":"2693",
         "name":"Corongo",
         "code":"01",
         "label":"Corongo, Corongo",
         "search":"corongo corongo",
         "children_count":"0",
         "level":"3",
         "parent_id":"2692"
      },
      {
         "id":"2696",
         "name":"Cusca",
         "code":"04",
         "label":"Cusca, Corongo",
         "search":"cusca corongo",
         "children_count":"0",
         "level":"3",
         "parent_id":"2692"
      },
      {
         "id":"2697",
         "name":"La Pampa",
         "code":"05",
         "label":"La Pampa, Corongo",
         "search":"la pampa corongo",
         "children_count":"0",
         "level":"3",
         "parent_id":"2692"
      },
      {
         "id":"2698",
         "name":"Yanac",
         "code":"06",
         "label":"Yanac, Corongo",
         "search":"yanac corongo",
         "children_count":"0",
         "level":"3",
         "parent_id":"2692"
      },
      {
         "id":"2699",
         "name":"Yupan",
         "code":"07",
         "label":"Yupan, Corongo",
         "search":"yupan corongo",
         "children_count":"0",
         "level":"3",
         "parent_id":"2692"
      }
   ],
   "2626":[
      {
         "id":"2628",
         "name":"Cochabamba",
         "code":"02",
         "label":"Cochabamba, Huaraz",
         "search":"cochabamba huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      },
      {
         "id":"2629",
         "name":"Colcabamba",
         "code":"03",
         "label":"Colcabamba, Huaraz",
         "search":"colcabamba huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      },
      {
         "id":"2630",
         "name":"Huanchay",
         "code":"04",
         "label":"Huanchay, Huaraz",
         "search":"huanchay huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      },
      {
         "id":"2627",
         "name":"Huaraz",
         "code":"01",
         "label":"Huaraz, Huaraz",
         "search":"huaraz huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      },
      {
         "id":"2631",
         "name":"Independencia",
         "code":"05",
         "label":"Independencia, Huaraz",
         "search":"independencia huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      },
      {
         "id":"2632",
         "name":"Jangas",
         "code":"06",
         "label":"Jangas, Huaraz",
         "search":"jangas huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      },
      {
         "id":"2633",
         "name":"La Libertad",
         "code":"07",
         "label":"La Libertad, Huaraz",
         "search":"la libertad huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      },
      {
         "id":"2634",
         "name":"Olleros",
         "code":"08",
         "label":"Olleros, Huaraz",
         "search":"olleros huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      },
      {
         "id":"2635",
         "name":"Pampas",
         "code":"09",
         "label":"Pampas, Huaraz",
         "search":"pampas huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      },
      {
         "id":"2636",
         "name":"Pariacoto",
         "code":"10",
         "label":"Pariacoto, Huaraz",
         "search":"pariacoto huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      },
      {
         "id":"2637",
         "name":"Pira",
         "code":"11",
         "label":"Pira, Huaraz",
         "search":"pira huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      },
      {
         "id":"2638",
         "name":"Tarica",
         "code":"12",
         "label":"Tarica, Huaraz",
         "search":"tarica huaraz",
         "children_count":"0",
         "level":"3",
         "parent_id":"2626"
      }
   ],
   "2700":[
      {
         "id":"2702",
         "name":"Anra",
         "code":"02",
         "label":"Anra, Huari",
         "search":"anra huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2703",
         "name":"Cajay",
         "code":"03",
         "label":"Cajay, Huari",
         "search":"cajay huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2704",
         "name":"Chavin de Huantar",
         "code":"04",
         "label":"Chavin de Huantar, Huari",
         "search":"chavin de huantar huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2705",
         "name":"Huacachi",
         "code":"05",
         "label":"Huacachi, Huari",
         "search":"huacachi huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2706",
         "name":"Huacchis",
         "code":"06",
         "label":"Huacchis, Huari",
         "search":"huacchis huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2707",
         "name":"Huachis",
         "code":"07",
         "label":"Huachis, Huari",
         "search":"huachis huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2708",
         "name":"Huantar",
         "code":"08",
         "label":"Huantar, Huari",
         "search":"huantar huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2701",
         "name":"Huari",
         "code":"01",
         "label":"Huari, Huari",
         "search":"huari huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2709",
         "name":"Masin",
         "code":"09",
         "label":"Masin, Huari",
         "search":"masin huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2710",
         "name":"Paucas",
         "code":"10",
         "label":"Paucas, Huari",
         "search":"paucas huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2711",
         "name":"Ponto",
         "code":"11",
         "label":"Ponto, Huari",
         "search":"ponto huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2712",
         "name":"Rahuapampa",
         "code":"12",
         "label":"Rahuapampa, Huari",
         "search":"rahuapampa huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2713",
         "name":"Rapayan",
         "code":"13",
         "label":"Rapayan, Huari",
         "search":"rapayan huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2714",
         "name":"San Marcos",
         "code":"14",
         "label":"San Marcos, Huari",
         "search":"san marcos huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2715",
         "name":"San Pedro de Chana",
         "code":"15",
         "label":"San Pedro de Chana, Huari",
         "search":"san pedro de chana huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      },
      {
         "id":"2716",
         "name":"Uco",
         "code":"16",
         "label":"Uco, Huari",
         "search":"uco huari",
         "children_count":"0",
         "level":"3",
         "parent_id":"2700"
      }
   ],
   "2717":[
      {
         "id":"2719",
         "name":"Cochapeti",
         "code":"02",
         "label":"Cochapeti, Huarmey",
         "search":"cochapeti huarmey",
         "children_count":"0",
         "level":"3",
         "parent_id":"2717"
      },
      {
         "id":"2720",
         "name":"Culebras",
         "code":"03",
         "label":"Culebras, Huarmey",
         "search":"culebras huarmey",
         "children_count":"0",
         "level":"3",
         "parent_id":"2717"
      },
      {
         "id":"2718",
         "name":"Huarmey",
         "code":"01",
         "label":"Huarmey, Huarmey",
         "search":"huarmey huarmey",
         "children_count":"0",
         "level":"3",
         "parent_id":"2717"
      },
      {
         "id":"2721",
         "name":"Huayan",
         "code":"04",
         "label":"Huayan, Huarmey",
         "search":"huayan huarmey",
         "children_count":"0",
         "level":"3",
         "parent_id":"2717"
      },
      {
         "id":"2722",
         "name":"Malvas",
         "code":"05",
         "label":"Malvas, Huarmey",
         "search":"malvas huarmey",
         "children_count":"0",
         "level":"3",
         "parent_id":"2717"
      }
   ],
   "2723":[
      {
         "id":"2724",
         "name":"Caraz",
         "code":"01",
         "label":"Caraz, Huaylas",
         "search":"caraz huaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2723"
      },
      {
         "id":"2725",
         "name":"Huallanca",
         "code":"02",
         "label":"Huallanca, Huaylas",
         "search":"huallanca huaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2723"
      },
      {
         "id":"2726",
         "name":"Huata",
         "code":"03",
         "label":"Huata, Huaylas",
         "search":"huata huaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2723"
      },
      {
         "id":"2727",
         "name":"Huaylas",
         "code":"04",
         "label":"Huaylas, Huaylas",
         "search":"huaylas huaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2723"
      },
      {
         "id":"2728",
         "name":"Mato",
         "code":"05",
         "label":"Mato, Huaylas",
         "search":"mato huaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2723"
      },
      {
         "id":"2729",
         "name":"Pamparomas",
         "code":"06",
         "label":"Pamparomas, Huaylas",
         "search":"pamparomas huaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2723"
      },
      {
         "id":"2730",
         "name":"Pueblo Libre",
         "code":"07",
         "label":"Pueblo Libre, Huaylas",
         "search":"pueblo libre huaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2723"
      },
      {
         "id":"2731",
         "name":"Santa Cruz",
         "code":"08",
         "label":"Santa Cruz, Huaylas",
         "search":"santa cruz huaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2723"
      },
      {
         "id":"2732",
         "name":"Santo Toribio",
         "code":"09",
         "label":"Santo Toribio, Huaylas",
         "search":"santo toribio huaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2723"
      },
      {
         "id":"2733",
         "name":"Yuracmarca",
         "code":"10",
         "label":"Yuracmarca, Huaylas",
         "search":"yuracmarca huaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2723"
      }
   ],
   "2734":[
      {
         "id":"2736",
         "name":"Casca",
         "code":"02",
         "label":"Casca, Mariscal Luzuriaga",
         "search":"casca mariscal luzuriaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"2734"
      },
      {
         "id":"2737",
         "name":"Eleazar Guzman Barron",
         "code":"03",
         "label":"Eleazar Guzman Barron, Mariscal Luzuriaga",
         "search":"eleazar guzman barron mariscal luzuriaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"2734"
      },
      {
         "id":"2738",
         "name":"Fidel Olivas Escudero",
         "code":"04",
         "label":"Fidel Olivas Escudero, Mariscal Luzuriaga",
         "search":"fidel olivas escudero mariscal luzuriaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"2734"
      },
      {
         "id":"2739",
         "name":"Llama",
         "code":"05",
         "label":"Llama, Mariscal Luzuriaga",
         "search":"llama mariscal luzuriaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"2734"
      },
      {
         "id":"2740",
         "name":"Llumpa",
         "code":"06",
         "label":"Llumpa, Mariscal Luzuriaga",
         "search":"llumpa mariscal luzuriaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"2734"
      },
      {
         "id":"2741",
         "name":"Lucma",
         "code":"07",
         "label":"Lucma, Mariscal Luzuriaga",
         "search":"lucma mariscal luzuriaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"2734"
      },
      {
         "id":"2742",
         "name":"Musga",
         "code":"08",
         "label":"Musga, Mariscal Luzuriaga",
         "search":"musga mariscal luzuriaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"2734"
      },
      {
         "id":"2735",
         "name":"Piscobamba",
         "code":"01",
         "label":"Piscobamba, Mariscal Luzuriaga",
         "search":"piscobamba mariscal luzuriaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"2734"
      }
   ],
   "2743":[
      {
         "id":"2745",
         "name":"Acas",
         "code":"02",
         "label":"Acas, Ocros",
         "search":"acas ocros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2743"
      },
      {
         "id":"2746",
         "name":"Cajamarquilla",
         "code":"03",
         "label":"Cajamarquilla, Ocros",
         "search":"cajamarquilla ocros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2743"
      },
      {
         "id":"2747",
         "name":"Carhuapampa",
         "code":"04",
         "label":"Carhuapampa, Ocros",
         "search":"carhuapampa ocros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2743"
      },
      {
         "id":"2748",
         "name":"Cochas",
         "code":"05",
         "label":"Cochas, Ocros",
         "search":"cochas ocros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2743"
      },
      {
         "id":"2749",
         "name":"Congas",
         "code":"06",
         "label":"Congas, Ocros",
         "search":"congas ocros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2743"
      },
      {
         "id":"2750",
         "name":"Llipa",
         "code":"07",
         "label":"Llipa, Ocros",
         "search":"llipa ocros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2743"
      },
      {
         "id":"2744",
         "name":"Ocros",
         "code":"01",
         "label":"Ocros, Ocros",
         "search":"ocros ocros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2743"
      },
      {
         "id":"2751",
         "name":"San Cristobal de Rajan",
         "code":"08",
         "label":"San Cristobal de Rajan, Ocros",
         "search":"san cristobal de rajan ocros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2743"
      },
      {
         "id":"2752",
         "name":"San Pedro",
         "code":"09",
         "label":"San Pedro, Ocros",
         "search":"san pedro ocros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2743"
      },
      {
         "id":"2753",
         "name":"Santiago de Chilcas",
         "code":"10",
         "label":"Santiago de Chilcas, Ocros",
         "search":"santiago de chilcas ocros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2743"
      }
   ],
   "2754":[
      {
         "id":"2756",
         "name":"Bolognesi",
         "code":"02",
         "label":"Bolognesi, Pallasca",
         "search":"bolognesi pallasca",
         "children_count":"0",
         "level":"3",
         "parent_id":"2754"
      },
      {
         "id":"2755",
         "name":"Cabana",
         "code":"01",
         "label":"Cabana, Pallasca",
         "search":"cabana pallasca",
         "children_count":"0",
         "level":"3",
         "parent_id":"2754"
      },
      {
         "id":"2757",
         "name":"Conchucos",
         "code":"03",
         "label":"Conchucos, Pallasca",
         "search":"conchucos pallasca",
         "children_count":"0",
         "level":"3",
         "parent_id":"2754"
      },
      {
         "id":"2758",
         "name":"Huacaschuque",
         "code":"04",
         "label":"Huacaschuque, Pallasca",
         "search":"huacaschuque pallasca",
         "children_count":"0",
         "level":"3",
         "parent_id":"2754"
      },
      {
         "id":"2759",
         "name":"Huandoval",
         "code":"05",
         "label":"Huandoval, Pallasca",
         "search":"huandoval pallasca",
         "children_count":"0",
         "level":"3",
         "parent_id":"2754"
      },
      {
         "id":"2760",
         "name":"Lacabamba",
         "code":"06",
         "label":"Lacabamba, Pallasca",
         "search":"lacabamba pallasca",
         "children_count":"0",
         "level":"3",
         "parent_id":"2754"
      },
      {
         "id":"2761",
         "name":"Llapo",
         "code":"07",
         "label":"Llapo, Pallasca",
         "search":"llapo pallasca",
         "children_count":"0",
         "level":"3",
         "parent_id":"2754"
      },
      {
         "id":"2762",
         "name":"Pallasca",
         "code":"08",
         "label":"Pallasca, Pallasca",
         "search":"pallasca pallasca",
         "children_count":"0",
         "level":"3",
         "parent_id":"2754"
      },
      {
         "id":"2763",
         "name":"Pampas",
         "code":"09",
         "label":"Pampas, Pallasca",
         "search":"pampas pallasca",
         "children_count":"0",
         "level":"3",
         "parent_id":"2754"
      },
      {
         "id":"2764",
         "name":"Santa Rosa",
         "code":"10",
         "label":"Santa Rosa, Pallasca",
         "search":"santa rosa pallasca",
         "children_count":"0",
         "level":"3",
         "parent_id":"2754"
      },
      {
         "id":"2765",
         "name":"Tauca",
         "code":"11",
         "label":"Tauca, Pallasca",
         "search":"tauca pallasca",
         "children_count":"0",
         "level":"3",
         "parent_id":"2754"
      }
   ],
   "2766":[
      {
         "id":"2768",
         "name":"Huayllan",
         "code":"02",
         "label":"Huayllan, Pomabamba",
         "search":"huayllan pomabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2766"
      },
      {
         "id":"2769",
         "name":"Parobamba",
         "code":"03",
         "label":"Parobamba, Pomabamba",
         "search":"parobamba pomabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2766"
      },
      {
         "id":"2767",
         "name":"Pomabamba",
         "code":"01",
         "label":"Pomabamba, Pomabamba",
         "search":"pomabamba pomabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2766"
      },
      {
         "id":"2770",
         "name":"Quinuabamba",
         "code":"04",
         "label":"Quinuabamba, Pomabamba",
         "search":"quinuabamba pomabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2766"
      }
   ],
   "2771":[
      {
         "id":"2773",
         "name":"Catac",
         "code":"02",
         "label":"Catac, Recuay",
         "search":"catac recuay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2771"
      },
      {
         "id":"2774",
         "name":"Cotaparaco",
         "code":"03",
         "label":"Cotaparaco, Recuay",
         "search":"cotaparaco recuay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2771"
      },
      {
         "id":"2775",
         "name":"Huayllapampa",
         "code":"04",
         "label":"Huayllapampa, Recuay",
         "search":"huayllapampa recuay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2771"
      },
      {
         "id":"2776",
         "name":"Llacllin",
         "code":"05",
         "label":"Llacllin, Recuay",
         "search":"llacllin recuay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2771"
      },
      {
         "id":"2777",
         "name":"Marca",
         "code":"06",
         "label":"Marca, Recuay",
         "search":"marca recuay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2771"
      },
      {
         "id":"2778",
         "name":"Pampas Chico",
         "code":"07",
         "label":"Pampas Chico, Recuay",
         "search":"pampas chico recuay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2771"
      },
      {
         "id":"2779",
         "name":"Pararin",
         "code":"08",
         "label":"Pararin, Recuay",
         "search":"pararin recuay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2771"
      },
      {
         "id":"2772",
         "name":"Recuay",
         "code":"01",
         "label":"Recuay, Recuay",
         "search":"recuay recuay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2771"
      },
      {
         "id":"2780",
         "name":"Tapacocha",
         "code":"09",
         "label":"Tapacocha, Recuay",
         "search":"tapacocha recuay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2771"
      },
      {
         "id":"2781",
         "name":"Ticapampa",
         "code":"10",
         "label":"Ticapampa, Recuay",
         "search":"ticapampa recuay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2771"
      }
   ],
   "2782":[
      {
         "id":"2784",
         "name":"Caceres del Peru",
         "code":"02",
         "label":"Caceres del Peru, Santa",
         "search":"caceres del peru santa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2782"
      },
      {
         "id":"2783",
         "name":"Chimbote",
         "code":"01",
         "label":"Chimbote, Santa",
         "search":"chimbote santa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2782"
      },
      {
         "id":"2785",
         "name":"Coishco",
         "code":"03",
         "label":"Coishco, Santa",
         "search":"coishco santa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2782"
      },
      {
         "id":"2786",
         "name":"Macate",
         "code":"04",
         "label":"Macate, Santa",
         "search":"macate santa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2782"
      },
      {
         "id":"2787",
         "name":"Moro",
         "code":"05",
         "label":"Moro, Santa",
         "search":"moro santa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2782"
      },
      {
         "id":"2788",
         "name":"Nepeqa",
         "code":"06",
         "label":"Nepeqa, Santa",
         "search":"nepeqa santa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2782"
      },
      {
         "id":"2791",
         "name":"Nuevo Chimbote",
         "code":"09",
         "label":"Nuevo Chimbote, Santa",
         "search":"nuevo chimbote santa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2782"
      },
      {
         "id":"2789",
         "name":"Samanco",
         "code":"07",
         "label":"Samanco, Santa",
         "search":"samanco santa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2782"
      },
      {
         "id":"2790",
         "name":"Santa",
         "code":"08",
         "label":"Santa, Santa",
         "search":"santa santa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2782"
      }
   ],
   "2792":[
      {
         "id":"2794",
         "name":"Acobamba",
         "code":"02",
         "label":"Acobamba, Sihuas",
         "search":"acobamba sihuas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2792"
      },
      {
         "id":"2795",
         "name":"Alfonso Ugarte",
         "code":"03",
         "label":"Alfonso Ugarte, Sihuas",
         "search":"alfonso ugarte sihuas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2792"
      },
      {
         "id":"2796",
         "name":"Cashapampa",
         "code":"04",
         "label":"Cashapampa, Sihuas",
         "search":"cashapampa sihuas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2792"
      },
      {
         "id":"2797",
         "name":"Chingalpo",
         "code":"05",
         "label":"Chingalpo, Sihuas",
         "search":"chingalpo sihuas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2792"
      },
      {
         "id":"2798",
         "name":"Huayllabamba",
         "code":"06",
         "label":"Huayllabamba, Sihuas",
         "search":"huayllabamba sihuas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2792"
      },
      {
         "id":"2799",
         "name":"Quiches",
         "code":"07",
         "label":"Quiches, Sihuas",
         "search":"quiches sihuas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2792"
      },
      {
         "id":"2800",
         "name":"Ragash",
         "code":"08",
         "label":"Ragash, Sihuas",
         "search":"ragash sihuas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2792"
      },
      {
         "id":"2801",
         "name":"San Juan",
         "code":"09",
         "label":"San Juan, Sihuas",
         "search":"san juan sihuas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2792"
      },
      {
         "id":"2802",
         "name":"Sicsibamba",
         "code":"10",
         "label":"Sicsibamba, Sihuas",
         "search":"sicsibamba sihuas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2792"
      },
      {
         "id":"2793",
         "name":"Sihuas",
         "code":"01",
         "label":"Sihuas, Sihuas",
         "search":"sihuas sihuas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2792"
      }
   ],
   "2803":[
      {
         "id":"2805",
         "name":"Cascapara",
         "code":"02",
         "label":"Cascapara, Yungay",
         "search":"cascapara yungay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2803"
      },
      {
         "id":"2806",
         "name":"Mancos",
         "code":"03",
         "label":"Mancos, Yungay",
         "search":"mancos yungay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2803"
      },
      {
         "id":"2807",
         "name":"Matacoto",
         "code":"04",
         "label":"Matacoto, Yungay",
         "search":"matacoto yungay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2803"
      },
      {
         "id":"2808",
         "name":"Quillo",
         "code":"05",
         "label":"Quillo, Yungay",
         "search":"quillo yungay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2803"
      },
      {
         "id":"2809",
         "name":"Ranrahirca",
         "code":"06",
         "label":"Ranrahirca, Yungay",
         "search":"ranrahirca yungay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2803"
      },
      {
         "id":"2810",
         "name":"Shupluy",
         "code":"07",
         "label":"Shupluy, Yungay",
         "search":"shupluy yungay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2803"
      },
      {
         "id":"2811",
         "name":"Yanama",
         "code":"08",
         "label":"Yanama, Yungay",
         "search":"yanama yungay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2803"
      },
      {
         "id":"2804",
         "name":"Yungay",
         "code":"01",
         "label":"Yungay, Yungay",
         "search":"yungay yungay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2803"
      }
   ],
   "2813":[
      {
         "id":"2814",
         "name":"Abancay",
         "code":"01",
         "label":"Abancay, Abancay",
         "search":"abancay abancay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2813"
      },
      {
         "id":"2815",
         "name":"Chacoche",
         "code":"02",
         "label":"Chacoche, Abancay",
         "search":"chacoche abancay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2813"
      },
      {
         "id":"2816",
         "name":"Circa",
         "code":"03",
         "label":"Circa, Abancay",
         "search":"circa abancay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2813"
      },
      {
         "id":"2817",
         "name":"Curahuasi",
         "code":"04",
         "label":"Curahuasi, Abancay",
         "search":"curahuasi abancay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2813"
      },
      {
         "id":"2818",
         "name":"Huanipaca",
         "code":"05",
         "label":"Huanipaca, Abancay",
         "search":"huanipaca abancay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2813"
      },
      {
         "id":"2819",
         "name":"Lambrama",
         "code":"06",
         "label":"Lambrama, Abancay",
         "search":"lambrama abancay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2813"
      },
      {
         "id":"2820",
         "name":"Pichirhua",
         "code":"07",
         "label":"Pichirhua, Abancay",
         "search":"pichirhua abancay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2813"
      },
      {
         "id":"2821",
         "name":"San Pedro de Cachora",
         "code":"08",
         "label":"San Pedro de Cachora, Abancay",
         "search":"san pedro de cachora abancay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2813"
      },
      {
         "id":"2822",
         "name":"Tamburco",
         "code":"09",
         "label":"Tamburco, Abancay",
         "search":"tamburco abancay",
         "children_count":"0",
         "level":"3",
         "parent_id":"2813"
      }
   ],
   "2823":[
      {
         "id":"2824",
         "name":"Andahuaylas",
         "code":"01",
         "label":"Andahuaylas, Andahuaylas",
         "search":"andahuaylas andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2825",
         "name":"Andarapa",
         "code":"02",
         "label":"Andarapa, Andahuaylas",
         "search":"andarapa andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2826",
         "name":"Chiara",
         "code":"03",
         "label":"Chiara, Andahuaylas",
         "search":"chiara andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2827",
         "name":"Huancarama",
         "code":"04",
         "label":"Huancarama, Andahuaylas",
         "search":"huancarama andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2828",
         "name":"Huancaray",
         "code":"05",
         "label":"Huancaray, Andahuaylas",
         "search":"huancaray andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2829",
         "name":"Huayana",
         "code":"06",
         "label":"Huayana, Andahuaylas",
         "search":"huayana andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2842",
         "name":"Kaquiabamba",
         "code":"19",
         "label":"Kaquiabamba, Andahuaylas",
         "search":"kaquiabamba andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2830",
         "name":"Kishuara",
         "code":"07",
         "label":"Kishuara, Andahuaylas",
         "search":"kishuara andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2831",
         "name":"Pacobamba",
         "code":"08",
         "label":"Pacobamba, Andahuaylas",
         "search":"pacobamba andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2832",
         "name":"Pacucha",
         "code":"09",
         "label":"Pacucha, Andahuaylas",
         "search":"pacucha andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2833",
         "name":"Pampachiri",
         "code":"10",
         "label":"Pampachiri, Andahuaylas",
         "search":"pampachiri andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2834",
         "name":"Pomacocha",
         "code":"11",
         "label":"Pomacocha, Andahuaylas",
         "search":"pomacocha andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2835",
         "name":"San Antonio de Cachi",
         "code":"12",
         "label":"San Antonio de Cachi, Andahuaylas",
         "search":"san antonio de cachi andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2836",
         "name":"San Jeronimo",
         "code":"13",
         "label":"San Jeronimo, Andahuaylas",
         "search":"san jeronimo andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2837",
         "name":"San Miguel de Chaccrampa",
         "code":"14",
         "label":"San Miguel de Chaccrampa, Andahuaylas",
         "search":"san miguel de chaccrampa andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2838",
         "name":"Santa Maria de Chicmo",
         "code":"15",
         "label":"Santa Maria de Chicmo, Andahuaylas",
         "search":"santa maria de chicmo andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2839",
         "name":"Talavera",
         "code":"16",
         "label":"Talavera, Andahuaylas",
         "search":"talavera andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2840",
         "name":"Tumay Huaraca",
         "code":"17",
         "label":"Tumay Huaraca, Andahuaylas",
         "search":"tumay huaraca andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      },
      {
         "id":"2841",
         "name":"Turpo",
         "code":"18",
         "label":"Turpo, Andahuaylas",
         "search":"turpo andahuaylas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2823"
      }
   ],
   "2843":[
      {
         "id":"2844",
         "name":"Antabamba",
         "code":"01",
         "label":"Antabamba, Antabamba",
         "search":"antabamba antabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2843"
      },
      {
         "id":"2845",
         "name":"El Oro",
         "code":"02",
         "label":"El Oro, Antabamba",
         "search":"el oro antabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2843"
      },
      {
         "id":"2846",
         "name":"Huaquirca",
         "code":"03",
         "label":"Huaquirca, Antabamba",
         "search":"huaquirca antabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2843"
      },
      {
         "id":"2847",
         "name":"Juan Espinoza Medrano",
         "code":"04",
         "label":"Juan Espinoza Medrano, Antabamba",
         "search":"juan espinoza medrano antabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2843"
      },
      {
         "id":"2848",
         "name":"Oropesa",
         "code":"05",
         "label":"Oropesa, Antabamba",
         "search":"oropesa antabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2843"
      },
      {
         "id":"2849",
         "name":"Pachaconas",
         "code":"06",
         "label":"Pachaconas, Antabamba",
         "search":"pachaconas antabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2843"
      },
      {
         "id":"2850",
         "name":"Sabaino",
         "code":"07",
         "label":"Sabaino, Antabamba",
         "search":"sabaino antabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"2843"
      }
   ],
   "2851":[
      {
         "id":"2853",
         "name":"Capaya",
         "code":"02",
         "label":"Capaya, Aymaraes",
         "search":"capaya aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2854",
         "name":"Caraybamba",
         "code":"03",
         "label":"Caraybamba, Aymaraes",
         "search":"caraybamba aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2852",
         "name":"Chalhuanca",
         "code":"01",
         "label":"Chalhuanca, Aymaraes",
         "search":"chalhuanca aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2855",
         "name":"Chapimarca",
         "code":"04",
         "label":"Chapimarca, Aymaraes",
         "search":"chapimarca aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2856",
         "name":"Colcabamba",
         "code":"05",
         "label":"Colcabamba, Aymaraes",
         "search":"colcabamba aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2857",
         "name":"Cotaruse",
         "code":"06",
         "label":"Cotaruse, Aymaraes",
         "search":"cotaruse aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2858",
         "name":"Huayllo",
         "code":"07",
         "label":"Huayllo, Aymaraes",
         "search":"huayllo aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2859",
         "name":"Justo Apu Sahuaraura",
         "code":"08",
         "label":"Justo Apu Sahuaraura, Aymaraes",
         "search":"justo apu sahuaraura aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2860",
         "name":"Lucre",
         "code":"09",
         "label":"Lucre, Aymaraes",
         "search":"lucre aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2861",
         "name":"Pocohuanca",
         "code":"10",
         "label":"Pocohuanca, Aymaraes",
         "search":"pocohuanca aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2862",
         "name":"San Juan de Chacqa",
         "code":"11",
         "label":"San Juan de Chacqa, Aymaraes",
         "search":"san juan de chacqa aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2863",
         "name":"Saqayca",
         "code":"12",
         "label":"Saqayca, Aymaraes",
         "search":"saqayca aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2864",
         "name":"Soraya",
         "code":"13",
         "label":"Soraya, Aymaraes",
         "search":"soraya aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2865",
         "name":"Tapairihua",
         "code":"14",
         "label":"Tapairihua, Aymaraes",
         "search":"tapairihua aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2866",
         "name":"Tintay",
         "code":"15",
         "label":"Tintay, Aymaraes",
         "search":"tintay aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2867",
         "name":"Toraya",
         "code":"16",
         "label":"Toraya, Aymaraes",
         "search":"toraya aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      },
      {
         "id":"2868",
         "name":"Yanaca",
         "code":"17",
         "label":"Yanaca, Aymaraes",
         "search":"yanaca aymaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"2851"
      }
   ],
   "2876":[
      {
         "id":"2878",
         "name":"Anco-Huallo",
         "code":"02",
         "label":"Anco-Huallo, Chincheros",
         "search":"anco-huallo chincheros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2876"
      },
      {
         "id":"2877",
         "name":"Chincheros",
         "code":"01",
         "label":"Chincheros, Chincheros",
         "search":"chincheros chincheros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2876"
      },
      {
         "id":"2879",
         "name":"Cocharcas",
         "code":"03",
         "label":"Cocharcas, Chincheros",
         "search":"cocharcas chincheros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2876"
      },
      {
         "id":"2880",
         "name":"Huaccana",
         "code":"04",
         "label":"Huaccana, Chincheros",
         "search":"huaccana chincheros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2876"
      },
      {
         "id":"2881",
         "name":"Ocobamba",
         "code":"05",
         "label":"Ocobamba, Chincheros",
         "search":"ocobamba chincheros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2876"
      },
      {
         "id":"2882",
         "name":"Ongoy",
         "code":"06",
         "label":"Ongoy, Chincheros",
         "search":"ongoy chincheros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2876"
      },
      {
         "id":"2884",
         "name":"Ranracancha",
         "code":"08",
         "label":"Ranracancha, Chincheros",
         "search":"ranracancha chincheros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2876"
      },
      {
         "id":"2883",
         "name":"Uranmarca",
         "code":"07",
         "label":"Uranmarca, Chincheros",
         "search":"uranmarca chincheros",
         "children_count":"0",
         "level":"3",
         "parent_id":"2876"
      }
   ],
   "2869":[
      {
         "id":"2875",
         "name":"Challhuahuacho",
         "code":"06",
         "label":"Challhuahuacho, Cotabambas",
         "search":"challhuahuacho cotabambas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2869"
      },
      {
         "id":"2871",
         "name":"Cotabambas",
         "code":"02",
         "label":"Cotabambas, Cotabambas",
         "search":"cotabambas cotabambas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2869"
      },
      {
         "id":"2872",
         "name":"Coyllurqui",
         "code":"03",
         "label":"Coyllurqui, Cotabambas",
         "search":"coyllurqui cotabambas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2869"
      },
      {
         "id":"2873",
         "name":"Haquira",
         "code":"04",
         "label":"Haquira, Cotabambas",
         "search":"haquira cotabambas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2869"
      },
      {
         "id":"2874",
         "name":"Mara",
         "code":"05",
         "label":"Mara, Cotabambas",
         "search":"mara cotabambas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2869"
      },
      {
         "id":"2870",
         "name":"Tambobamba",
         "code":"01",
         "label":"Tambobamba, Cotabambas",
         "search":"tambobamba cotabambas",
         "children_count":"0",
         "level":"3",
         "parent_id":"2869"
      }
   ],
   "2885":[
      {
         "id":"2886",
         "name":"Chuquibambilla",
         "code":"01",
         "label":"Chuquibambilla, Grau",
         "search":"chuquibambilla grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2899",
         "name":"Curasco",
         "code":"14",
         "label":"Curasco, Grau",
         "search":"curasco grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2887",
         "name":"Curpahuasi",
         "code":"02",
         "label":"Curpahuasi, Grau",
         "search":"curpahuasi grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2888",
         "name":"Gamarra",
         "code":"03",
         "label":"Gamarra, Grau",
         "search":"gamarra grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2889",
         "name":"Huayllati",
         "code":"04",
         "label":"Huayllati, Grau",
         "search":"huayllati grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2890",
         "name":"Mamara",
         "code":"05",
         "label":"Mamara, Grau",
         "search":"mamara grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2891",
         "name":"Micaela Bastidas",
         "code":"06",
         "label":"Micaela Bastidas, Grau",
         "search":"micaela bastidas grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2892",
         "name":"Pataypampa",
         "code":"07",
         "label":"Pataypampa, Grau",
         "search":"pataypampa grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2893",
         "name":"Progreso",
         "code":"08",
         "label":"Progreso, Grau",
         "search":"progreso grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2894",
         "name":"San Antonio",
         "code":"09",
         "label":"San Antonio, Grau",
         "search":"san antonio grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2895",
         "name":"Santa Rosa",
         "code":"10",
         "label":"Santa Rosa, Grau",
         "search":"santa rosa grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2896",
         "name":"Turpay",
         "code":"11",
         "label":"Turpay, Grau",
         "search":"turpay grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2897",
         "name":"Vilcabamba",
         "code":"12",
         "label":"Vilcabamba, Grau",
         "search":"vilcabamba grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      },
      {
         "id":"2898",
         "name":"Virundo",
         "code":"13",
         "label":"Virundo, Grau",
         "search":"virundo grau",
         "children_count":"0",
         "level":"3",
         "parent_id":"2885"
      }
   ],
   "2901":[
      {
         "id":"2903",
         "name":"Alto Selva Alegre",
         "code":"02",
         "label":"Alto Selva Alegre, Arequipa",
         "search":"alto selva alegre arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2902",
         "name":"Arequipa",
         "code":"01",
         "label":"Arequipa, Arequipa",
         "search":"arequipa arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2904",
         "name":"Cayma",
         "code":"03",
         "label":"Cayma, Arequipa",
         "search":"cayma arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2905",
         "name":"Cerro Colorado",
         "code":"04",
         "label":"Cerro Colorado, Arequipa",
         "search":"cerro colorado arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2906",
         "name":"Characato",
         "code":"05",
         "label":"Characato, Arequipa",
         "search":"characato arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2907",
         "name":"Chiguata",
         "code":"06",
         "label":"Chiguata, Arequipa",
         "search":"chiguata arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2908",
         "name":"Jacobo Hunter",
         "code":"07",
         "label":"Jacobo Hunter, Arequipa",
         "search":"jacobo hunter arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2930",
         "name":"Jose Luis Bustamante y Rivero",
         "code":"29",
         "label":"Jose Luis Bustamante y Rivero, Arequipa",
         "search":"jose luis bustamante y rivero arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2909",
         "name":"La Joya",
         "code":"08",
         "label":"La Joya, Arequipa",
         "search":"la joya arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2910",
         "name":"Mariano Melgar",
         "code":"09",
         "label":"Mariano Melgar, Arequipa",
         "search":"mariano melgar arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2911",
         "name":"Miraflores",
         "code":"10",
         "label":"Miraflores, Arequipa",
         "search":"miraflores arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2912",
         "name":"Mollebaya",
         "code":"11",
         "label":"Mollebaya, Arequipa",
         "search":"mollebaya arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2913",
         "name":"Paucarpata",
         "code":"12",
         "label":"Paucarpata, Arequipa",
         "search":"paucarpata arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2914",
         "name":"Pocsi",
         "code":"13",
         "label":"Pocsi, Arequipa",
         "search":"pocsi arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2915",
         "name":"Polobaya",
         "code":"14",
         "label":"Polobaya, Arequipa",
         "search":"polobaya arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2916",
         "name":"Quequeqa",
         "code":"15",
         "label":"Quequeqa, Arequipa",
         "search":"quequeqa arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2917",
         "name":"Sabandia",
         "code":"16",
         "label":"Sabandia, Arequipa",
         "search":"sabandia arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2918",
         "name":"Sachaca",
         "code":"17",
         "label":"Sachaca, Arequipa",
         "search":"sachaca arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2919",
         "name":"San Juan de Siguas",
         "code":"18",
         "label":"San Juan de Siguas, Arequipa",
         "search":"san juan de siguas arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2920",
         "name":"San Juan de Tarucani",
         "code":"19",
         "label":"San Juan de Tarucani, Arequipa",
         "search":"san juan de tarucani arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2921",
         "name":"Santa Isabel de Siguas",
         "code":"20",
         "label":"Santa Isabel de Siguas, Arequipa",
         "search":"santa isabel de siguas arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2922",
         "name":"Santa Rita de Siguas",
         "code":"21",
         "label":"Santa Rita de Siguas, Arequipa",
         "search":"santa rita de siguas arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2923",
         "name":"Socabaya",
         "code":"22",
         "label":"Socabaya, Arequipa",
         "search":"socabaya arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2924",
         "name":"Tiabaya",
         "code":"23",
         "label":"Tiabaya, Arequipa",
         "search":"tiabaya arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2925",
         "name":"Uchumayo",
         "code":"24",
         "label":"Uchumayo, Arequipa",
         "search":"uchumayo arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2926",
         "name":"Vitor",
         "code":"25",
         "label":"Vitor, Arequipa",
         "search":"vitor arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2927",
         "name":"Yanahuara",
         "code":"26",
         "label":"Yanahuara, Arequipa",
         "search":"yanahuara arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2928",
         "name":"Yarabamba",
         "code":"27",
         "label":"Yarabamba, Arequipa",
         "search":"yarabamba arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      },
      {
         "id":"2929",
         "name":"Yura",
         "code":"28",
         "label":"Yura, Arequipa",
         "search":"yura arequipa",
         "children_count":"0",
         "level":"3",
         "parent_id":"2901"
      }
   ],
   "2931":[
      {
         "id":"2932",
         "name":"Camana",
         "code":"01",
         "label":"Camana, Camana",
         "search":"camana camana",
         "children_count":"0",
         "level":"3",
         "parent_id":"2931"
      },
      {
         "id":"2933",
         "name":"Jose Maria Quimper",
         "code":"02",
         "label":"Jose Maria Quimper, Camana",
         "search":"jose maria quimper camana",
         "children_count":"0",
         "level":"3",
         "parent_id":"2931"
      },
      {
         "id":"2934",
         "name":"Mariano Nicolas Valcarcel",
         "code":"03",
         "label":"Mariano Nicolas Valcarcel, Camana",
         "search":"mariano nicolas valcarcel camana",
         "children_count":"0",
         "level":"3",
         "parent_id":"2931"
      },
      {
         "id":"2935",
         "name":"Mariscal Caceres",
         "code":"04",
         "label":"Mariscal Caceres, Camana",
         "search":"mariscal caceres camana",
         "children_count":"0",
         "level":"3",
         "parent_id":"2931"
      },
      {
         "id":"2936",
         "name":"Nicolas de Pierola",
         "code":"05",
         "label":"Nicolas de Pierola, Camana",
         "search":"nicolas de pierola camana",
         "children_count":"0",
         "level":"3",
         "parent_id":"2931"
      },
      {
         "id":"2937",
         "name":"Ocoqa",
         "code":"06",
         "label":"Ocoqa, Camana",
         "search":"ocoqa camana",
         "children_count":"0",
         "level":"3",
         "parent_id":"2931"
      },
      {
         "id":"2938",
         "name":"Quilca",
         "code":"07",
         "label":"Quilca, Camana",
         "search":"quilca camana",
         "children_count":"0",
         "level":"3",
         "parent_id":"2931"
      },
      {
         "id":"2939",
         "name":"Samuel Pastor",
         "code":"08",
         "label":"Samuel Pastor, Camana",
         "search":"samuel pastor camana",
         "children_count":"0",
         "level":"3",
         "parent_id":"2931"
      }
   ],
   "2940":[
      {
         "id":"2942",
         "name":"Acari",
         "code":"02",
         "label":"Acari, Caraveli",
         "search":"acari caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2943",
         "name":"Atico",
         "code":"03",
         "label":"Atico, Caraveli",
         "search":"atico caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2944",
         "name":"Atiquipa",
         "code":"04",
         "label":"Atiquipa, Caraveli",
         "search":"atiquipa caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2945",
         "name":"Bella Union",
         "code":"05",
         "label":"Bella Union, Caraveli",
         "search":"bella union caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2946",
         "name":"Cahuacho",
         "code":"06",
         "label":"Cahuacho, Caraveli",
         "search":"cahuacho caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2941",
         "name":"Caraveli",
         "code":"01",
         "label":"Caraveli, Caraveli",
         "search":"caraveli caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2947",
         "name":"Chala",
         "code":"07",
         "label":"Chala, Caraveli",
         "search":"chala caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2948",
         "name":"Chaparra",
         "code":"08",
         "label":"Chaparra, Caraveli",
         "search":"chaparra caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2949",
         "name":"Huanuhuanu",
         "code":"09",
         "label":"Huanuhuanu, Caraveli",
         "search":"huanuhuanu caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2950",
         "name":"Jaqui",
         "code":"10",
         "label":"Jaqui, Caraveli",
         "search":"jaqui caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2951",
         "name":"Lomas",
         "code":"11",
         "label":"Lomas, Caraveli",
         "search":"lomas caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2952",
         "name":"Quicacha",
         "code":"12",
         "label":"Quicacha, Caraveli",
         "search":"quicacha caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      },
      {
         "id":"2953",
         "name":"Yauca",
         "code":"13",
         "label":"Yauca, Caraveli",
         "search":"yauca caraveli",
         "children_count":"0",
         "level":"3",
         "parent_id":"2940"
      }
   ],
   "2954":[
      {
         "id":"2956",
         "name":"Andagua",
         "code":"02",
         "label":"Andagua, Castilla",
         "search":"andagua castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2955",
         "name":"Aplao",
         "code":"01",
         "label":"Aplao, Castilla",
         "search":"aplao castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2957",
         "name":"Ayo",
         "code":"03",
         "label":"Ayo, Castilla",
         "search":"ayo castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2958",
         "name":"Chachas",
         "code":"04",
         "label":"Chachas, Castilla",
         "search":"chachas castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2959",
         "name":"Chilcaymarca",
         "code":"05",
         "label":"Chilcaymarca, Castilla",
         "search":"chilcaymarca castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2960",
         "name":"Choco",
         "code":"06",
         "label":"Choco, Castilla",
         "search":"choco castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2961",
         "name":"Huancarqui",
         "code":"07",
         "label":"Huancarqui, Castilla",
         "search":"huancarqui castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2962",
         "name":"Machaguay",
         "code":"08",
         "label":"Machaguay, Castilla",
         "search":"machaguay castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2970",
         "name":"Majes",
         "code":"20",
         "label":"Majes, Castilla",
         "search":"majes castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2963",
         "name":"Orcopampa",
         "code":"09",
         "label":"Orcopampa, Castilla",
         "search":"orcopampa castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2964",
         "name":"Pampacolca",
         "code":"10",
         "label":"Pampacolca, Castilla",
         "search":"pampacolca castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2965",
         "name":"Tipan",
         "code":"11",
         "label":"Tipan, Castilla",
         "search":"tipan castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2966",
         "name":"Uqon",
         "code":"12",
         "label":"Uqon, Castilla",
         "search":"uqon castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2967",
         "name":"Uraca",
         "code":"13",
         "label":"Uraca, Castilla",
         "search":"uraca castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2968",
         "name":"Viraco",
         "code":"14",
         "label":"Viraco, Castilla",
         "search":"viraco castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      },
      {
         "id":"2969",
         "name":"Yanque",
         "code":"19",
         "label":"Yanque, Castilla",
         "search":"yanque castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"2954"
      }
   ],
   "2971":[
      {
         "id":"2973",
         "name":"Achoma",
         "code":"02",
         "label":"Achoma, Caylloma",
         "search":"achoma caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2974",
         "name":"Cabanaconde",
         "code":"03",
         "label":"Cabanaconde, Caylloma",
         "search":"cabanaconde caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2975",
         "name":"Callalli",
         "code":"04",
         "label":"Callalli, Caylloma",
         "search":"callalli caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2976",
         "name":"Caylloma",
         "code":"05",
         "label":"Caylloma, Caylloma",
         "search":"caylloma caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2972",
         "name":"Chivay",
         "code":"01",
         "label":"Chivay, Caylloma",
         "search":"chivay caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2977",
         "name":"Coporaque",
         "code":"06",
         "label":"Coporaque, Caylloma",
         "search":"coporaque caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2978",
         "name":"Huambo",
         "code":"07",
         "label":"Huambo, Caylloma",
         "search":"huambo caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2979",
         "name":"Huanca",
         "code":"08",
         "label":"Huanca, Caylloma",
         "search":"huanca caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2980",
         "name":"Ichupampa",
         "code":"09",
         "label":"Ichupampa, Caylloma",
         "search":"ichupampa caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2981",
         "name":"Lari",
         "code":"10",
         "label":"Lari, Caylloma",
         "search":"lari caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2982",
         "name":"Lluta",
         "code":"11",
         "label":"Lluta, Caylloma",
         "search":"lluta caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2983",
         "name":"Maca",
         "code":"12",
         "label":"Maca, Caylloma",
         "search":"maca caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2984",
         "name":"Madrigal",
         "code":"13",
         "label":"Madrigal, Caylloma",
         "search":"madrigal caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2991",
         "name":"Majes",
         "code":"20",
         "label":"Majes, Caylloma",
         "search":"majes caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2985",
         "name":"San Antonio de Chuca",
         "code":"14",
         "label":"San Antonio de Chuca, Caylloma",
         "search":"san antonio de chuca caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2986",
         "name":"Sibayo",
         "code":"15",
         "label":"Sibayo, Caylloma",
         "search":"sibayo caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2987",
         "name":"Tapay",
         "code":"16",
         "label":"Tapay, Caylloma",
         "search":"tapay caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2988",
         "name":"Tisco",
         "code":"17",
         "label":"Tisco, Caylloma",
         "search":"tisco caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2989",
         "name":"Tuti",
         "code":"18",
         "label":"Tuti, Caylloma",
         "search":"tuti caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      },
      {
         "id":"2990",
         "name":"Yanque",
         "code":"19",
         "label":"Yanque, Caylloma",
         "search":"yanque caylloma",
         "children_count":"0",
         "level":"3",
         "parent_id":"2971"
      }
   ],
   "2992":[
      {
         "id":"2994",
         "name":"Andaray",
         "code":"02",
         "label":"Andaray, Condesuyos",
         "search":"andaray condesuyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"2992"
      },
      {
         "id":"2995",
         "name":"Cayarani",
         "code":"03",
         "label":"Cayarani, Condesuyos",
         "search":"cayarani condesuyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"2992"
      },
      {
         "id":"2996",
         "name":"Chichas",
         "code":"04",
         "label":"Chichas, Condesuyos",
         "search":"chichas condesuyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"2992"
      },
      {
         "id":"2993",
         "name":"Chuquibamba",
         "code":"01",
         "label":"Chuquibamba, Condesuyos",
         "search":"chuquibamba condesuyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"2992"
      },
      {
         "id":"2997",
         "name":"Iray",
         "code":"05",
         "label":"Iray, Condesuyos",
         "search":"iray condesuyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"2992"
      },
      {
         "id":"2998",
         "name":"Rio Grande",
         "code":"06",
         "label":"Rio Grande, Condesuyos",
         "search":"rio grande condesuyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"2992"
      },
      {
         "id":"2999",
         "name":"Salamanca",
         "code":"07",
         "label":"Salamanca, Condesuyos",
         "search":"salamanca condesuyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"2992"
      },
      {
         "id":"3000",
         "name":"Yanaquihua",
         "code":"08",
         "label":"Yanaquihua, Condesuyos",
         "search":"yanaquihua condesuyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"2992"
      }
   ],
   "3001":[
      {
         "id":"3003",
         "name":"Cocachacra",
         "code":"02",
         "label":"Cocachacra, Islay",
         "search":"cocachacra islay",
         "children_count":"0",
         "level":"3",
         "parent_id":"3001"
      },
      {
         "id":"3004",
         "name":"Dean Valdivia",
         "code":"03",
         "label":"Dean Valdivia, Islay",
         "search":"dean valdivia islay",
         "children_count":"0",
         "level":"3",
         "parent_id":"3001"
      },
      {
         "id":"3005",
         "name":"Islay",
         "code":"04",
         "label":"Islay, Islay",
         "search":"islay islay",
         "children_count":"0",
         "level":"3",
         "parent_id":"3001"
      },
      {
         "id":"3006",
         "name":"Mejia",
         "code":"05",
         "label":"Mejia, Islay",
         "search":"mejia islay",
         "children_count":"0",
         "level":"3",
         "parent_id":"3001"
      },
      {
         "id":"3002",
         "name":"Mollendo",
         "code":"01",
         "label":"Mollendo, Islay",
         "search":"mollendo islay",
         "children_count":"0",
         "level":"3",
         "parent_id":"3001"
      },
      {
         "id":"3007",
         "name":"Punta de Bombon",
         "code":"06",
         "label":"Punta de Bombon, Islay",
         "search":"punta de bombon islay",
         "children_count":"0",
         "level":"3",
         "parent_id":"3001"
      }
   ],
   "3008":[
      {
         "id":"3010",
         "name":"Alca",
         "code":"02",
         "label":"Alca, La Union",
         "search":"alca la union",
         "children_count":"0",
         "level":"3",
         "parent_id":"3008"
      },
      {
         "id":"3011",
         "name":"Charcana",
         "code":"03",
         "label":"Charcana, La Union",
         "search":"charcana la union",
         "children_count":"0",
         "level":"3",
         "parent_id":"3008"
      },
      {
         "id":"3009",
         "name":"Cotahuasi",
         "code":"01",
         "label":"Cotahuasi, La Union",
         "search":"cotahuasi la union",
         "children_count":"0",
         "level":"3",
         "parent_id":"3008"
      },
      {
         "id":"3012",
         "name":"Huaynacotas",
         "code":"04",
         "label":"Huaynacotas, La Union",
         "search":"huaynacotas la union",
         "children_count":"0",
         "level":"3",
         "parent_id":"3008"
      },
      {
         "id":"3013",
         "name":"Pampamarca",
         "code":"05",
         "label":"Pampamarca, La Union",
         "search":"pampamarca la union",
         "children_count":"0",
         "level":"3",
         "parent_id":"3008"
      },
      {
         "id":"3014",
         "name":"Puyca",
         "code":"06",
         "label":"Puyca, La Union",
         "search":"puyca la union",
         "children_count":"0",
         "level":"3",
         "parent_id":"3008"
      },
      {
         "id":"3015",
         "name":"Quechualla",
         "code":"07",
         "label":"Quechualla, La Union",
         "search":"quechualla la union",
         "children_count":"0",
         "level":"3",
         "parent_id":"3008"
      },
      {
         "id":"3016",
         "name":"Sayla",
         "code":"08",
         "label":"Sayla, La Union",
         "search":"sayla la union",
         "children_count":"0",
         "level":"3",
         "parent_id":"3008"
      },
      {
         "id":"3017",
         "name":"Tauria",
         "code":"09",
         "label":"Tauria, La Union",
         "search":"tauria la union",
         "children_count":"0",
         "level":"3",
         "parent_id":"3008"
      },
      {
         "id":"3018",
         "name":"Tomepampa",
         "code":"10",
         "label":"Tomepampa, La Union",
         "search":"tomepampa la union",
         "children_count":"0",
         "level":"3",
         "parent_id":"3008"
      },
      {
         "id":"3019",
         "name":"Toro",
         "code":"11",
         "label":"Toro, La Union",
         "search":"toro la union",
         "children_count":"0",
         "level":"3",
         "parent_id":"3008"
      }
   ],
   "3037":[
      {
         "id":"3038",
         "name":"Cangallo",
         "code":"01",
         "label":"Cangallo, Cangallo",
         "search":"cangallo cangallo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3037"
      },
      {
         "id":"3039",
         "name":"Chuschi",
         "code":"02",
         "label":"Chuschi, Cangallo",
         "search":"chuschi cangallo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3037"
      },
      {
         "id":"3040",
         "name":"Los Morochucos",
         "code":"03",
         "label":"Los Morochucos, Cangallo",
         "search":"los morochucos cangallo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3037"
      },
      {
         "id":"3041",
         "name":"Maria Parado de Bellido",
         "code":"04",
         "label":"Maria Parado de Bellido, Cangallo",
         "search":"maria parado de bellido cangallo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3037"
      },
      {
         "id":"3042",
         "name":"Paras",
         "code":"05",
         "label":"Paras, Cangallo",
         "search":"paras cangallo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3037"
      },
      {
         "id":"3043",
         "name":"Totos",
         "code":"06",
         "label":"Totos, Cangallo",
         "search":"totos cangallo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3037"
      }
   ],
   "3021":[
      {
         "id":"3023",
         "name":"Acocro",
         "code":"02",
         "label":"Acocro, Huamanga",
         "search":"acocro huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3024",
         "name":"Acos Vinchos",
         "code":"03",
         "label":"Acos Vinchos, Huamanga",
         "search":"acos vinchos huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3022",
         "name":"Ayacucho",
         "code":"01",
         "label":"Ayacucho, Huamanga",
         "search":"ayacucho huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3025",
         "name":"Carmen Alto",
         "code":"04",
         "label":"Carmen Alto, Huamanga",
         "search":"carmen alto huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3026",
         "name":"Chiara",
         "code":"05",
         "label":"Chiara, Huamanga",
         "search":"chiara huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3036",
         "name":"Jes\u00fas Nazareno",
         "code":"15",
         "label":"Jes\u00fas Nazareno, Huamanga",
         "search":"jes\u00fas nazareno huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3027",
         "name":"Ocros",
         "code":"06",
         "label":"Ocros, Huamanga",
         "search":"ocros huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3028",
         "name":"Pacaycasa",
         "code":"07",
         "label":"Pacaycasa, Huamanga",
         "search":"pacaycasa huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3029",
         "name":"Quinua",
         "code":"08",
         "label":"Quinua, Huamanga",
         "search":"quinua huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3030",
         "name":"San Jose de Ticllas",
         "code":"09",
         "label":"San Jose de Ticllas, Huamanga",
         "search":"san jose de ticllas huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3031",
         "name":"San Juan Bautista",
         "code":"10",
         "label":"San Juan Bautista, Huamanga",
         "search":"san juan bautista huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3032",
         "name":"Santiago de Pischa",
         "code":"11",
         "label":"Santiago de Pischa, Huamanga",
         "search":"santiago de pischa huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3033",
         "name":"Socos",
         "code":"12",
         "label":"Socos, Huamanga",
         "search":"socos huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3034",
         "name":"Tambillo",
         "code":"13",
         "label":"Tambillo, Huamanga",
         "search":"tambillo huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      },
      {
         "id":"3035",
         "name":"Vinchos",
         "code":"14",
         "label":"Vinchos, Huamanga",
         "search":"vinchos huamanga",
         "children_count":"0",
         "level":"3",
         "parent_id":"3021"
      }
   ],
   "3044":[
      {
         "id":"3046",
         "name":"Carapo",
         "code":"02",
         "label":"Carapo, Huanca Sancos",
         "search":"carapo huanca sancos",
         "children_count":"0",
         "level":"3",
         "parent_id":"3044"
      },
      {
         "id":"3047",
         "name":"Sacsamarca",
         "code":"03",
         "label":"Sacsamarca, Huanca Sancos",
         "search":"sacsamarca huanca sancos",
         "children_count":"0",
         "level":"3",
         "parent_id":"3044"
      },
      {
         "id":"3045",
         "name":"Sancos",
         "code":"01",
         "label":"Sancos, Huanca Sancos",
         "search":"sancos huanca sancos",
         "children_count":"0",
         "level":"3",
         "parent_id":"3044"
      },
      {
         "id":"3048",
         "name":"Santiago de Lucanamarca",
         "code":"04",
         "label":"Santiago de Lucanamarca, Huanca Sancos",
         "search":"santiago de lucanamarca huanca sancos",
         "children_count":"0",
         "level":"3",
         "parent_id":"3044"
      }
   ],
   "3049":[
      {
         "id":"3051",
         "name":"Ayahuanco",
         "code":"02",
         "label":"Ayahuanco, Huanta",
         "search":"ayahuanco huanta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3049"
      },
      {
         "id":"3052",
         "name":"Huamanguilla",
         "code":"03",
         "label":"Huamanguilla, Huanta",
         "search":"huamanguilla huanta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3049"
      },
      {
         "id":"3050",
         "name":"Huanta",
         "code":"01",
         "label":"Huanta, Huanta",
         "search":"huanta huanta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3049"
      },
      {
         "id":"3053",
         "name":"Iguain",
         "code":"04",
         "label":"Iguain, Huanta",
         "search":"iguain huanta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3049"
      },
      {
         "id":"3057",
         "name":"Llochegua",
         "code":"08",
         "label":"Llochegua, Huanta",
         "search":"llochegua huanta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3049"
      },
      {
         "id":"3054",
         "name":"Luricocha",
         "code":"05",
         "label":"Luricocha, Huanta",
         "search":"luricocha huanta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3049"
      },
      {
         "id":"3055",
         "name":"Santillana",
         "code":"06",
         "label":"Santillana, Huanta",
         "search":"santillana huanta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3049"
      },
      {
         "id":"3056",
         "name":"Sivia",
         "code":"07",
         "label":"Sivia, Huanta",
         "search":"sivia huanta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3049"
      }
   ],
   "3058":[
      {
         "id":"3060",
         "name":"Anco",
         "code":"02",
         "label":"Anco, La Mar",
         "search":"anco la mar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3058"
      },
      {
         "id":"3061",
         "name":"Ayna",
         "code":"03",
         "label":"Ayna, La Mar",
         "search":"ayna la mar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3058"
      },
      {
         "id":"3062",
         "name":"Chilcas",
         "code":"04",
         "label":"Chilcas, La Mar",
         "search":"chilcas la mar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3058"
      },
      {
         "id":"3063",
         "name":"Chungui",
         "code":"05",
         "label":"Chungui, La Mar",
         "search":"chungui la mar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3058"
      },
      {
         "id":"3064",
         "name":"Luis Carranza",
         "code":"06",
         "label":"Luis Carranza, La Mar",
         "search":"luis carranza la mar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3058"
      },
      {
         "id":"3059",
         "name":"San Miguel",
         "code":"01",
         "label":"San Miguel, La Mar",
         "search":"san miguel la mar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3058"
      },
      {
         "id":"3065",
         "name":"Santa Rosa",
         "code":"07",
         "label":"Santa Rosa, La Mar",
         "search":"santa rosa la mar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3058"
      },
      {
         "id":"3066",
         "name":"Tambo",
         "code":"08",
         "label":"Tambo, La Mar",
         "search":"tambo la mar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3058"
      }
   ],
   "3067":[
      {
         "id":"3069",
         "name":"Aucara",
         "code":"02",
         "label":"Aucara, Lucanas",
         "search":"aucara lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3070",
         "name":"Cabana",
         "code":"03",
         "label":"Cabana, Lucanas",
         "search":"cabana lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3071",
         "name":"Carmen Salcedo",
         "code":"04",
         "label":"Carmen Salcedo, Lucanas",
         "search":"carmen salcedo lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3072",
         "name":"Chaviqa",
         "code":"05",
         "label":"Chaviqa, Lucanas",
         "search":"chaviqa lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3073",
         "name":"Chipao",
         "code":"06",
         "label":"Chipao, Lucanas",
         "search":"chipao lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3074",
         "name":"Huac-Huas",
         "code":"07",
         "label":"Huac-Huas, Lucanas",
         "search":"huac-huas lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3075",
         "name":"Laramate",
         "code":"08",
         "label":"Laramate, Lucanas",
         "search":"laramate lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3076",
         "name":"Leoncio Prado",
         "code":"09",
         "label":"Leoncio Prado, Lucanas",
         "search":"leoncio prado lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3077",
         "name":"Llauta",
         "code":"10",
         "label":"Llauta, Lucanas",
         "search":"llauta lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3078",
         "name":"Lucanas",
         "code":"11",
         "label":"Lucanas, Lucanas",
         "search":"lucanas lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3079",
         "name":"Ocaqa",
         "code":"12",
         "label":"Ocaqa, Lucanas",
         "search":"ocaqa lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3080",
         "name":"Otoca",
         "code":"13",
         "label":"Otoca, Lucanas",
         "search":"otoca lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3068",
         "name":"Puquio",
         "code":"01",
         "label":"Puquio, Lucanas",
         "search":"puquio lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3081",
         "name":"Saisa",
         "code":"14",
         "label":"Saisa, Lucanas",
         "search":"saisa lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3082",
         "name":"San Cristobal",
         "code":"15",
         "label":"San Cristobal, Lucanas",
         "search":"san cristobal lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3083",
         "name":"San Juan",
         "code":"16",
         "label":"San Juan, Lucanas",
         "search":"san juan lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3084",
         "name":"San Pedro",
         "code":"17",
         "label":"San Pedro, Lucanas",
         "search":"san pedro lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3085",
         "name":"San Pedro de Palco",
         "code":"18",
         "label":"San Pedro de Palco, Lucanas",
         "search":"san pedro de palco lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3086",
         "name":"Sancos",
         "code":"19",
         "label":"Sancos, Lucanas",
         "search":"sancos lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3087",
         "name":"Santa Ana de Huaycahuacho",
         "code":"20",
         "label":"Santa Ana de Huaycahuacho, Lucanas",
         "search":"santa ana de huaycahuacho lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      },
      {
         "id":"3088",
         "name":"Santa Lucia",
         "code":"21",
         "label":"Santa Lucia, Lucanas",
         "search":"santa lucia lucanas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3067"
      }
   ],
   "3089":[
      {
         "id":"3091",
         "name":"Chumpi",
         "code":"02",
         "label":"Chumpi, Parinacochas",
         "search":"chumpi parinacochas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3089"
      },
      {
         "id":"3090",
         "name":"Coracora",
         "code":"01",
         "label":"Coracora, Parinacochas",
         "search":"coracora parinacochas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3089"
      },
      {
         "id":"3092",
         "name":"Coronel Castaqeda",
         "code":"03",
         "label":"Coronel Castaqeda, Parinacochas",
         "search":"coronel castaqeda parinacochas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3089"
      },
      {
         "id":"3093",
         "name":"Pacapausa",
         "code":"04",
         "label":"Pacapausa, Parinacochas",
         "search":"pacapausa parinacochas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3089"
      },
      {
         "id":"3094",
         "name":"Pullo",
         "code":"05",
         "label":"Pullo, Parinacochas",
         "search":"pullo parinacochas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3089"
      },
      {
         "id":"3095",
         "name":"Puyusca",
         "code":"06",
         "label":"Puyusca, Parinacochas",
         "search":"puyusca parinacochas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3089"
      },
      {
         "id":"3096",
         "name":"San Francisco de Ravacayco",
         "code":"07",
         "label":"San Francisco de Ravacayco, Parinacochas",
         "search":"san francisco de ravacayco parinacochas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3089"
      },
      {
         "id":"3097",
         "name":"Upahuacho",
         "code":"08",
         "label":"Upahuacho, Parinacochas",
         "search":"upahuacho parinacochas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3089"
      }
   ],
   "3098":[
      {
         "id":"3100",
         "name":"Colta",
         "code":"02",
         "label":"Colta, Paucar del Sara Sara",
         "search":"colta paucar del sara sara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3098"
      },
      {
         "id":"3101",
         "name":"Corculla",
         "code":"03",
         "label":"Corculla, Paucar del Sara Sara",
         "search":"corculla paucar del sara sara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3098"
      },
      {
         "id":"3102",
         "name":"Lampa",
         "code":"04",
         "label":"Lampa, Paucar del Sara Sara",
         "search":"lampa paucar del sara sara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3098"
      },
      {
         "id":"3103",
         "name":"Marcabamba",
         "code":"05",
         "label":"Marcabamba, Paucar del Sara Sara",
         "search":"marcabamba paucar del sara sara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3098"
      },
      {
         "id":"3104",
         "name":"Oyolo",
         "code":"06",
         "label":"Oyolo, Paucar del Sara Sara",
         "search":"oyolo paucar del sara sara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3098"
      },
      {
         "id":"3105",
         "name":"Pararca",
         "code":"07",
         "label":"Pararca, Paucar del Sara Sara",
         "search":"pararca paucar del sara sara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3098"
      },
      {
         "id":"3099",
         "name":"Pausa",
         "code":"01",
         "label":"Pausa, Paucar del Sara Sara",
         "search":"pausa paucar del sara sara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3098"
      },
      {
         "id":"3106",
         "name":"San Javier de Alpabamba",
         "code":"08",
         "label":"San Javier de Alpabamba, Paucar del Sara Sara",
         "search":"san javier de alpabamba paucar del sara sara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3098"
      },
      {
         "id":"3107",
         "name":"San Jose de Ushua",
         "code":"09",
         "label":"San Jose de Ushua, Paucar del Sara Sara",
         "search":"san jose de ushua paucar del sara sara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3098"
      },
      {
         "id":"3108",
         "name":"Sara Sara",
         "code":"10",
         "label":"Sara Sara, Paucar del Sara Sara",
         "search":"sara sara paucar del sara sara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3098"
      }
   ],
   "3109":[
      {
         "id":"3111",
         "name":"Belen",
         "code":"02",
         "label":"Belen, Sucre",
         "search":"belen sucre",
         "children_count":"0",
         "level":"3",
         "parent_id":"3109"
      },
      {
         "id":"3112",
         "name":"Chalcos",
         "code":"03",
         "label":"Chalcos, Sucre",
         "search":"chalcos sucre",
         "children_count":"0",
         "level":"3",
         "parent_id":"3109"
      },
      {
         "id":"3113",
         "name":"Chilcayoc",
         "code":"04",
         "label":"Chilcayoc, Sucre",
         "search":"chilcayoc sucre",
         "children_count":"0",
         "level":"3",
         "parent_id":"3109"
      },
      {
         "id":"3114",
         "name":"Huacaqa",
         "code":"05",
         "label":"Huacaqa, Sucre",
         "search":"huacaqa sucre",
         "children_count":"0",
         "level":"3",
         "parent_id":"3109"
      },
      {
         "id":"3115",
         "name":"Morcolla",
         "code":"06",
         "label":"Morcolla, Sucre",
         "search":"morcolla sucre",
         "children_count":"0",
         "level":"3",
         "parent_id":"3109"
      },
      {
         "id":"3116",
         "name":"Paico",
         "code":"07",
         "label":"Paico, Sucre",
         "search":"paico sucre",
         "children_count":"0",
         "level":"3",
         "parent_id":"3109"
      },
      {
         "id":"3110",
         "name":"Querobamba",
         "code":"01",
         "label":"Querobamba, Sucre",
         "search":"querobamba sucre",
         "children_count":"0",
         "level":"3",
         "parent_id":"3109"
      },
      {
         "id":"3117",
         "name":"San Pedro de Larcay",
         "code":"08",
         "label":"San Pedro de Larcay, Sucre",
         "search":"san pedro de larcay sucre",
         "children_count":"0",
         "level":"3",
         "parent_id":"3109"
      },
      {
         "id":"3118",
         "name":"San Salvador de Quije",
         "code":"09",
         "label":"San Salvador de Quije, Sucre",
         "search":"san salvador de quije sucre",
         "children_count":"0",
         "level":"3",
         "parent_id":"3109"
      },
      {
         "id":"3119",
         "name":"Santiago de Paucaray",
         "code":"10",
         "label":"Santiago de Paucaray, Sucre",
         "search":"santiago de paucaray sucre",
         "children_count":"0",
         "level":"3",
         "parent_id":"3109"
      },
      {
         "id":"3120",
         "name":"Soras",
         "code":"11",
         "label":"Soras, Sucre",
         "search":"soras sucre",
         "children_count":"0",
         "level":"3",
         "parent_id":"3109"
      }
   ],
   "3121":[
      {
         "id":"3123",
         "name":"Alcamenca",
         "code":"02",
         "label":"Alcamenca, Victor Fajardo",
         "search":"alcamenca victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      },
      {
         "id":"3124",
         "name":"Apongo",
         "code":"03",
         "label":"Apongo, Victor Fajardo",
         "search":"apongo victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      },
      {
         "id":"3125",
         "name":"Asquipata",
         "code":"04",
         "label":"Asquipata, Victor Fajardo",
         "search":"asquipata victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      },
      {
         "id":"3126",
         "name":"Canaria",
         "code":"05",
         "label":"Canaria, Victor Fajardo",
         "search":"canaria victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      },
      {
         "id":"3127",
         "name":"Cayara",
         "code":"06",
         "label":"Cayara, Victor Fajardo",
         "search":"cayara victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      },
      {
         "id":"3128",
         "name":"Colca",
         "code":"07",
         "label":"Colca, Victor Fajardo",
         "search":"colca victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      },
      {
         "id":"3129",
         "name":"Huamanquiquia",
         "code":"08",
         "label":"Huamanquiquia, Victor Fajardo",
         "search":"huamanquiquia victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      },
      {
         "id":"3122",
         "name":"Huancapi",
         "code":"01",
         "label":"Huancapi, Victor Fajardo",
         "search":"huancapi victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      },
      {
         "id":"3130",
         "name":"Huancaraylla",
         "code":"09",
         "label":"Huancaraylla, Victor Fajardo",
         "search":"huancaraylla victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      },
      {
         "id":"3131",
         "name":"Huaya",
         "code":"10",
         "label":"Huaya, Victor Fajardo",
         "search":"huaya victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      },
      {
         "id":"3132",
         "name":"Sarhua",
         "code":"11",
         "label":"Sarhua, Victor Fajardo",
         "search":"sarhua victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      },
      {
         "id":"3133",
         "name":"Vilcanchos",
         "code":"12",
         "label":"Vilcanchos, Victor Fajardo",
         "search":"vilcanchos victor fajardo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3121"
      }
   ],
   "3134":[
      {
         "id":"3136",
         "name":"Accomarca",
         "code":"02",
         "label":"Accomarca, Vilcas Huaman",
         "search":"accomarca vilcas huaman",
         "children_count":"0",
         "level":"3",
         "parent_id":"3134"
      },
      {
         "id":"3137",
         "name":"Carhuanca",
         "code":"03",
         "label":"Carhuanca, Vilcas Huaman",
         "search":"carhuanca vilcas huaman",
         "children_count":"0",
         "level":"3",
         "parent_id":"3134"
      },
      {
         "id":"3138",
         "name":"Concepcion",
         "code":"04",
         "label":"Concepcion, Vilcas Huaman",
         "search":"concepcion vilcas huaman",
         "children_count":"0",
         "level":"3",
         "parent_id":"3134"
      },
      {
         "id":"3139",
         "name":"Huambalpa",
         "code":"05",
         "label":"Huambalpa, Vilcas Huaman",
         "search":"huambalpa vilcas huaman",
         "children_count":"0",
         "level":"3",
         "parent_id":"3134"
      },
      {
         "id":"3140",
         "name":"Independencia",
         "code":"06",
         "label":"Independencia, Vilcas Huaman",
         "search":"independencia vilcas huaman",
         "children_count":"0",
         "level":"3",
         "parent_id":"3134"
      },
      {
         "id":"3141",
         "name":"Saurama",
         "code":"07",
         "label":"Saurama, Vilcas Huaman",
         "search":"saurama vilcas huaman",
         "children_count":"0",
         "level":"3",
         "parent_id":"3134"
      },
      {
         "id":"3135",
         "name":"Vilcas Huaman",
         "code":"01",
         "label":"Vilcas Huaman, Vilcas Huaman",
         "search":"vilcas huaman vilcas huaman",
         "children_count":"0",
         "level":"3",
         "parent_id":"3134"
      },
      {
         "id":"3142",
         "name":"Vischongo",
         "code":"08",
         "label":"Vischongo, Vilcas Huaman",
         "search":"vischongo vilcas huaman",
         "children_count":"0",
         "level":"3",
         "parent_id":"3134"
      }
   ],
   "3157":[
      {
         "id":"3159",
         "name":"Cachachi",
         "code":"02",
         "label":"Cachachi, Cajabamba",
         "search":"cachachi cajabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3157"
      },
      {
         "id":"3158",
         "name":"Cajabamba",
         "code":"01",
         "label":"Cajabamba, Cajabamba",
         "search":"cajabamba cajabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3157"
      },
      {
         "id":"3160",
         "name":"Condebamba",
         "code":"03",
         "label":"Condebamba, Cajabamba",
         "search":"condebamba cajabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3157"
      },
      {
         "id":"3161",
         "name":"Sitacocha",
         "code":"04",
         "label":"Sitacocha, Cajabamba",
         "search":"sitacocha cajabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3157"
      }
   ],
   "3144":[
      {
         "id":"3146",
         "name":"Asuncion",
         "code":"02",
         "label":"Asuncion, Cajamarca",
         "search":"asuncion cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      },
      {
         "id":"3145",
         "name":"Cajamarca",
         "code":"01",
         "label":"Cajamarca, Cajamarca",
         "search":"cajamarca cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      },
      {
         "id":"3147",
         "name":"Chetilla",
         "code":"03",
         "label":"Chetilla, Cajamarca",
         "search":"chetilla cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      },
      {
         "id":"3148",
         "name":"Cospan",
         "code":"04",
         "label":"Cospan, Cajamarca",
         "search":"cospan cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      },
      {
         "id":"3149",
         "name":"Encaqada",
         "code":"05",
         "label":"Encaqada, Cajamarca",
         "search":"encaqada cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      },
      {
         "id":"3150",
         "name":"Jesus",
         "code":"06",
         "label":"Jesus, Cajamarca",
         "search":"jesus cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      },
      {
         "id":"3151",
         "name":"Llacanora",
         "code":"07",
         "label":"Llacanora, Cajamarca",
         "search":"llacanora cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      },
      {
         "id":"3152",
         "name":"Los Baqos del Inca",
         "code":"08",
         "label":"Los Baqos del Inca, Cajamarca",
         "search":"los baqos del inca cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      },
      {
         "id":"3153",
         "name":"Magdalena",
         "code":"09",
         "label":"Magdalena, Cajamarca",
         "search":"magdalena cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      },
      {
         "id":"3154",
         "name":"Matara",
         "code":"10",
         "label":"Matara, Cajamarca",
         "search":"matara cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      },
      {
         "id":"3155",
         "name":"Namora",
         "code":"11",
         "label":"Namora, Cajamarca",
         "search":"namora cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      },
      {
         "id":"3156",
         "name":"San Juan",
         "code":"12",
         "label":"San Juan, Cajamarca",
         "search":"san juan cajamarca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3144"
      }
   ],
   "3162":[
      {
         "id":"3163",
         "name":"Celendin",
         "code":"01",
         "label":"Celendin, Celendin",
         "search":"celendin celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      },
      {
         "id":"3164",
         "name":"Chumuch",
         "code":"02",
         "label":"Chumuch, Celendin",
         "search":"chumuch celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      },
      {
         "id":"3165",
         "name":"Cortegana",
         "code":"03",
         "label":"Cortegana, Celendin",
         "search":"cortegana celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      },
      {
         "id":"3166",
         "name":"Huasmin",
         "code":"04",
         "label":"Huasmin, Celendin",
         "search":"huasmin celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      },
      {
         "id":"3167",
         "name":"Jorge Chavez",
         "code":"05",
         "label":"Jorge Chavez, Celendin",
         "search":"jorge chavez celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      },
      {
         "id":"3168",
         "name":"Jose Galvez",
         "code":"06",
         "label":"Jose Galvez, Celendin",
         "search":"jose galvez celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      },
      {
         "id":"3174",
         "name":"La Libertad de Pallan",
         "code":"12",
         "label":"La Libertad de Pallan, Celendin",
         "search":"la libertad de pallan celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      },
      {
         "id":"3169",
         "name":"Miguel Iglesias",
         "code":"07",
         "label":"Miguel Iglesias, Celendin",
         "search":"miguel iglesias celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      },
      {
         "id":"3170",
         "name":"Oxamarca",
         "code":"08",
         "label":"Oxamarca, Celendin",
         "search":"oxamarca celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      },
      {
         "id":"3171",
         "name":"Sorochuco",
         "code":"09",
         "label":"Sorochuco, Celendin",
         "search":"sorochuco celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      },
      {
         "id":"3172",
         "name":"Sucre",
         "code":"10",
         "label":"Sucre, Celendin",
         "search":"sucre celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      },
      {
         "id":"3173",
         "name":"Utco",
         "code":"11",
         "label":"Utco, Celendin",
         "search":"utco celendin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3162"
      }
   ],
   "3175":[
      {
         "id":"3177",
         "name":"Anguia",
         "code":"02",
         "label":"Anguia, Chota",
         "search":"anguia chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3178",
         "name":"Chadin",
         "code":"03",
         "label":"Chadin, Chota",
         "search":"chadin chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3194",
         "name":"Chalamarca",
         "code":"19",
         "label":"Chalamarca, Chota",
         "search":"chalamarca chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3179",
         "name":"Chiguirip",
         "code":"04",
         "label":"Chiguirip, Chota",
         "search":"chiguirip chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3180",
         "name":"Chimban",
         "code":"05",
         "label":"Chimban, Chota",
         "search":"chimban chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3181",
         "name":"Choropampa",
         "code":"06",
         "label":"Choropampa, Chota",
         "search":"choropampa chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3176",
         "name":"Chota",
         "code":"01",
         "label":"Chota, Chota",
         "search":"chota chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3182",
         "name":"Cochabamba",
         "code":"07",
         "label":"Cochabamba, Chota",
         "search":"cochabamba chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3183",
         "name":"Conchan",
         "code":"08",
         "label":"Conchan, Chota",
         "search":"conchan chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3184",
         "name":"Huambos",
         "code":"09",
         "label":"Huambos, Chota",
         "search":"huambos chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3185",
         "name":"Lajas",
         "code":"10",
         "label":"Lajas, Chota",
         "search":"lajas chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3186",
         "name":"Llama",
         "code":"11",
         "label":"Llama, Chota",
         "search":"llama chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3187",
         "name":"Miracosta",
         "code":"12",
         "label":"Miracosta, Chota",
         "search":"miracosta chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3188",
         "name":"Paccha",
         "code":"13",
         "label":"Paccha, Chota",
         "search":"paccha chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3189",
         "name":"Pion",
         "code":"14",
         "label":"Pion, Chota",
         "search":"pion chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3190",
         "name":"Querocoto",
         "code":"15",
         "label":"Querocoto, Chota",
         "search":"querocoto chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3191",
         "name":"San Juan de Licupis",
         "code":"16",
         "label":"San Juan de Licupis, Chota",
         "search":"san juan de licupis chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3192",
         "name":"Tacabamba",
         "code":"17",
         "label":"Tacabamba, Chota",
         "search":"tacabamba chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      },
      {
         "id":"3193",
         "name":"Tocmoche",
         "code":"18",
         "label":"Tocmoche, Chota",
         "search":"tocmoche chota",
         "children_count":"0",
         "level":"3",
         "parent_id":"3175"
      }
   ],
   "3195":[
      {
         "id":"3197",
         "name":"Chilete",
         "code":"02",
         "label":"Chilete, Contumaza",
         "search":"chilete contumaza",
         "children_count":"0",
         "level":"3",
         "parent_id":"3195"
      },
      {
         "id":"3196",
         "name":"Contumaza",
         "code":"01",
         "label":"Contumaza, Contumaza",
         "search":"contumaza contumaza",
         "children_count":"0",
         "level":"3",
         "parent_id":"3195"
      },
      {
         "id":"3198",
         "name":"Cupisnique",
         "code":"03",
         "label":"Cupisnique, Contumaza",
         "search":"cupisnique contumaza",
         "children_count":"0",
         "level":"3",
         "parent_id":"3195"
      },
      {
         "id":"3199",
         "name":"Guzmango",
         "code":"04",
         "label":"Guzmango, Contumaza",
         "search":"guzmango contumaza",
         "children_count":"0",
         "level":"3",
         "parent_id":"3195"
      },
      {
         "id":"3200",
         "name":"San Benito",
         "code":"05",
         "label":"San Benito, Contumaza",
         "search":"san benito contumaza",
         "children_count":"0",
         "level":"3",
         "parent_id":"3195"
      },
      {
         "id":"3201",
         "name":"Santa Cruz de Toled",
         "code":"06",
         "label":"Santa Cruz de Toled, Contumaza",
         "search":"santa cruz de toled contumaza",
         "children_count":"0",
         "level":"3",
         "parent_id":"3195"
      },
      {
         "id":"3202",
         "name":"Tantarica",
         "code":"07",
         "label":"Tantarica, Contumaza",
         "search":"tantarica contumaza",
         "children_count":"0",
         "level":"3",
         "parent_id":"3195"
      },
      {
         "id":"3203",
         "name":"Yonan",
         "code":"08",
         "label":"Yonan, Contumaza",
         "search":"yonan contumaza",
         "children_count":"0",
         "level":"3",
         "parent_id":"3195"
      }
   ],
   "3204":[
      {
         "id":"3206",
         "name":"Callayuc",
         "code":"02",
         "label":"Callayuc, Cutervo",
         "search":"callayuc cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3207",
         "name":"Choros",
         "code":"03",
         "label":"Choros, Cutervo",
         "search":"choros cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3208",
         "name":"Cujillo",
         "code":"04",
         "label":"Cujillo, Cutervo",
         "search":"cujillo cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3205",
         "name":"Cutervo",
         "code":"01",
         "label":"Cutervo, Cutervo",
         "search":"cutervo cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3209",
         "name":"La Ramada",
         "code":"05",
         "label":"La Ramada, Cutervo",
         "search":"la ramada cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3210",
         "name":"Pimpingos",
         "code":"06",
         "label":"Pimpingos, Cutervo",
         "search":"pimpingos cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3211",
         "name":"Querocotillo",
         "code":"07",
         "label":"Querocotillo, Cutervo",
         "search":"querocotillo cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3212",
         "name":"San Andres de Cutervo",
         "code":"08",
         "label":"San Andres de Cutervo, Cutervo",
         "search":"san andres de cutervo cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3213",
         "name":"San Juan de Cutervo",
         "code":"09",
         "label":"San Juan de Cutervo, Cutervo",
         "search":"san juan de cutervo cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3214",
         "name":"San Luis de Lucma",
         "code":"10",
         "label":"San Luis de Lucma, Cutervo",
         "search":"san luis de lucma cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3215",
         "name":"Santa Cruz",
         "code":"11",
         "label":"Santa Cruz, Cutervo",
         "search":"santa cruz cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3216",
         "name":"Santo Domingo de la Capilla",
         "code":"12",
         "label":"Santo Domingo de la Capilla, Cutervo",
         "search":"santo domingo de la capilla cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3217",
         "name":"Santo Tomas",
         "code":"13",
         "label":"Santo Tomas, Cutervo",
         "search":"santo tomas cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3218",
         "name":"Socota",
         "code":"14",
         "label":"Socota, Cutervo",
         "search":"socota cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      },
      {
         "id":"3219",
         "name":"Toribio Casanova",
         "code":"15",
         "label":"Toribio Casanova, Cutervo",
         "search":"toribio casanova cutervo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3204"
      }
   ],
   "3220":[
      {
         "id":"3221",
         "name":"Bambamarca",
         "code":"01",
         "label":"Bambamarca, Hualgayoc",
         "search":"bambamarca hualgayoc",
         "children_count":"0",
         "level":"3",
         "parent_id":"3220"
      },
      {
         "id":"3222",
         "name":"Chugur",
         "code":"02",
         "label":"Chugur, Hualgayoc",
         "search":"chugur hualgayoc",
         "children_count":"0",
         "level":"3",
         "parent_id":"3220"
      },
      {
         "id":"3223",
         "name":"Hualgayoc",
         "code":"03",
         "label":"Hualgayoc, Hualgayoc",
         "search":"hualgayoc hualgayoc",
         "children_count":"0",
         "level":"3",
         "parent_id":"3220"
      }
   ],
   "3224":[
      {
         "id":"3226",
         "name":"Bellavista",
         "code":"02",
         "label":"Bellavista, Jaen",
         "search":"bellavista jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      },
      {
         "id":"3227",
         "name":"Chontali",
         "code":"03",
         "label":"Chontali, Jaen",
         "search":"chontali jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      },
      {
         "id":"3228",
         "name":"Colasay",
         "code":"04",
         "label":"Colasay, Jaen",
         "search":"colasay jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      },
      {
         "id":"3229",
         "name":"Huabal",
         "code":"05",
         "label":"Huabal, Jaen",
         "search":"huabal jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      },
      {
         "id":"3225",
         "name":"Jaen",
         "code":"01",
         "label":"Jaen, Jaen",
         "search":"jaen jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      },
      {
         "id":"3230",
         "name":"Las Pirias",
         "code":"06",
         "label":"Las Pirias, Jaen",
         "search":"las pirias jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      },
      {
         "id":"3231",
         "name":"Pomahuaca",
         "code":"07",
         "label":"Pomahuaca, Jaen",
         "search":"pomahuaca jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      },
      {
         "id":"3232",
         "name":"Pucara",
         "code":"08",
         "label":"Pucara, Jaen",
         "search":"pucara jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      },
      {
         "id":"3233",
         "name":"Sallique",
         "code":"09",
         "label":"Sallique, Jaen",
         "search":"sallique jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      },
      {
         "id":"3234",
         "name":"San Felipe",
         "code":"10",
         "label":"San Felipe, Jaen",
         "search":"san felipe jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      },
      {
         "id":"3235",
         "name":"San Jose del Alto",
         "code":"11",
         "label":"San Jose del Alto, Jaen",
         "search":"san jose del alto jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      },
      {
         "id":"3236",
         "name":"Santa Rosa",
         "code":"12",
         "label":"Santa Rosa, Jaen",
         "search":"santa rosa jaen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3224"
      }
   ],
   "3237":[
      {
         "id":"3239",
         "name":"Chirinos",
         "code":"02",
         "label":"Chirinos, San Ignacio",
         "search":"chirinos san ignacio",
         "children_count":"0",
         "level":"3",
         "parent_id":"3237"
      },
      {
         "id":"3240",
         "name":"Huarango",
         "code":"03",
         "label":"Huarango, San Ignacio",
         "search":"huarango san ignacio",
         "children_count":"0",
         "level":"3",
         "parent_id":"3237"
      },
      {
         "id":"3241",
         "name":"La Coipa",
         "code":"04",
         "label":"La Coipa, San Ignacio",
         "search":"la coipa san ignacio",
         "children_count":"0",
         "level":"3",
         "parent_id":"3237"
      },
      {
         "id":"3242",
         "name":"Namballe",
         "code":"05",
         "label":"Namballe, San Ignacio",
         "search":"namballe san ignacio",
         "children_count":"0",
         "level":"3",
         "parent_id":"3237"
      },
      {
         "id":"3238",
         "name":"San Ignacio",
         "code":"01",
         "label":"San Ignacio, San Ignacio",
         "search":"san ignacio san ignacio",
         "children_count":"0",
         "level":"3",
         "parent_id":"3237"
      },
      {
         "id":"3243",
         "name":"San Jose de Lourdes",
         "code":"06",
         "label":"San Jose de Lourdes, San Ignacio",
         "search":"san jose de lourdes san ignacio",
         "children_count":"0",
         "level":"3",
         "parent_id":"3237"
      },
      {
         "id":"3244",
         "name":"Tabaconas",
         "code":"07",
         "label":"Tabaconas, San Ignacio",
         "search":"tabaconas san ignacio",
         "children_count":"0",
         "level":"3",
         "parent_id":"3237"
      }
   ],
   "3245":[
      {
         "id":"3247",
         "name":"Chancay",
         "code":"02",
         "label":"Chancay, San Marcos",
         "search":"chancay san marcos",
         "children_count":"0",
         "level":"3",
         "parent_id":"3245"
      },
      {
         "id":"3248",
         "name":"Eduardo Villanueva",
         "code":"03",
         "label":"Eduardo Villanueva, San Marcos",
         "search":"eduardo villanueva san marcos",
         "children_count":"0",
         "level":"3",
         "parent_id":"3245"
      },
      {
         "id":"3249",
         "name":"Gregorio Pita",
         "code":"04",
         "label":"Gregorio Pita, San Marcos",
         "search":"gregorio pita san marcos",
         "children_count":"0",
         "level":"3",
         "parent_id":"3245"
      },
      {
         "id":"3250",
         "name":"Ichocan",
         "code":"05",
         "label":"Ichocan, San Marcos",
         "search":"ichocan san marcos",
         "children_count":"0",
         "level":"3",
         "parent_id":"3245"
      },
      {
         "id":"3251",
         "name":"Jose Manuel Quiroz",
         "code":"06",
         "label":"Jose Manuel Quiroz, San Marcos",
         "search":"jose manuel quiroz san marcos",
         "children_count":"0",
         "level":"3",
         "parent_id":"3245"
      },
      {
         "id":"3252",
         "name":"Jose Sabogal",
         "code":"07",
         "label":"Jose Sabogal, San Marcos",
         "search":"jose sabogal san marcos",
         "children_count":"0",
         "level":"3",
         "parent_id":"3245"
      },
      {
         "id":"3246",
         "name":"Pedro Galvez",
         "code":"01",
         "label":"Pedro Galvez, San Marcos",
         "search":"pedro galvez san marcos",
         "children_count":"0",
         "level":"3",
         "parent_id":"3245"
      }
   ],
   "3253":[
      {
         "id":"3255",
         "name":"Bolivar",
         "code":"02",
         "label":"Bolivar, San Miguel",
         "search":"bolivar san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3256",
         "name":"Calquis",
         "code":"03",
         "label":"Calquis, San Miguel",
         "search":"calquis san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3257",
         "name":"Catilluc",
         "code":"04",
         "label":"Catilluc, San Miguel",
         "search":"catilluc san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3258",
         "name":"El Prado",
         "code":"05",
         "label":"El Prado, San Miguel",
         "search":"el prado san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3259",
         "name":"La Florida",
         "code":"06",
         "label":"La Florida, San Miguel",
         "search":"la florida san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3260",
         "name":"Llapa",
         "code":"07",
         "label":"Llapa, San Miguel",
         "search":"llapa san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3261",
         "name":"Nanchoc",
         "code":"08",
         "label":"Nanchoc, San Miguel",
         "search":"nanchoc san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3262",
         "name":"Niepos",
         "code":"09",
         "label":"Niepos, San Miguel",
         "search":"niepos san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3263",
         "name":"San Gregorio",
         "code":"10",
         "label":"San Gregorio, San Miguel",
         "search":"san gregorio san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3254",
         "name":"San Miguel",
         "code":"01",
         "label":"San Miguel, San Miguel",
         "search":"san miguel san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3264",
         "name":"San Silvestre de Cochan",
         "code":"11",
         "label":"San Silvestre de Cochan, San Miguel",
         "search":"san silvestre de cochan san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3265",
         "name":"Tongod",
         "code":"12",
         "label":"Tongod, San Miguel",
         "search":"tongod san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      },
      {
         "id":"3266",
         "name":"Union Agua Blanca",
         "code":"13",
         "label":"Union Agua Blanca, San Miguel",
         "search":"union agua blanca san miguel",
         "children_count":"0",
         "level":"3",
         "parent_id":"3253"
      }
   ],
   "3267":[
      {
         "id":"3269",
         "name":"San Bernardino",
         "code":"02",
         "label":"San Bernardino, San Pablo",
         "search":"san bernardino san pablo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3267"
      },
      {
         "id":"3270",
         "name":"San Luis",
         "code":"03",
         "label":"San Luis, San Pablo",
         "search":"san luis san pablo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3267"
      },
      {
         "id":"3268",
         "name":"San Pablo",
         "code":"01",
         "label":"San Pablo, San Pablo",
         "search":"san pablo san pablo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3267"
      },
      {
         "id":"3271",
         "name":"Tumbaden",
         "code":"04",
         "label":"Tumbaden, San Pablo",
         "search":"tumbaden san pablo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3267"
      }
   ],
   "3272":[
      {
         "id":"3274",
         "name":"Andabamba",
         "code":"02",
         "label":"Andabamba, Santa Cruz",
         "search":"andabamba santa cruz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3272"
      },
      {
         "id":"3275",
         "name":"Catache",
         "code":"03",
         "label":"Catache, Santa Cruz",
         "search":"catache santa cruz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3272"
      },
      {
         "id":"3276",
         "name":"Chancaybaqos",
         "code":"04",
         "label":"Chancaybaqos, Santa Cruz",
         "search":"chancaybaqos santa cruz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3272"
      },
      {
         "id":"3277",
         "name":"La Esperanza",
         "code":"05",
         "label":"La Esperanza, Santa Cruz",
         "search":"la esperanza santa cruz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3272"
      },
      {
         "id":"3278",
         "name":"Ninabamba",
         "code":"06",
         "label":"Ninabamba, Santa Cruz",
         "search":"ninabamba santa cruz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3272"
      },
      {
         "id":"3279",
         "name":"Pulan",
         "code":"07",
         "label":"Pulan, Santa Cruz",
         "search":"pulan santa cruz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3272"
      },
      {
         "id":"3273",
         "name":"Santa Cruz",
         "code":"01",
         "label":"Santa Cruz, Santa Cruz",
         "search":"santa cruz santa cruz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3272"
      },
      {
         "id":"3280",
         "name":"Saucepampa",
         "code":"08",
         "label":"Saucepampa, Santa Cruz",
         "search":"saucepampa santa cruz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3272"
      },
      {
         "id":"3281",
         "name":"Sexi",
         "code":"09",
         "label":"Sexi, Santa Cruz",
         "search":"sexi santa cruz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3272"
      },
      {
         "id":"3282",
         "name":"Uticyacu",
         "code":"10",
         "label":"Uticyacu, Santa Cruz",
         "search":"uticyacu santa cruz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3272"
      },
      {
         "id":"3283",
         "name":"Yauyucan",
         "code":"11",
         "label":"Yauyucan, Santa Cruz",
         "search":"yauyucan santa cruz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3272"
      }
   ],
   "3302":[
      {
         "id":"3303",
         "name":"Acomayo",
         "code":"01",
         "label":"Acomayo, Acomayo",
         "search":"acomayo acomayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3302"
      },
      {
         "id":"3304",
         "name":"Acopia",
         "code":"02",
         "label":"Acopia, Acomayo",
         "search":"acopia acomayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3302"
      },
      {
         "id":"3305",
         "name":"Acos",
         "code":"03",
         "label":"Acos, Acomayo",
         "search":"acos acomayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3302"
      },
      {
         "id":"3306",
         "name":"Mosoc Llacta",
         "code":"04",
         "label":"Mosoc Llacta, Acomayo",
         "search":"mosoc llacta acomayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3302"
      },
      {
         "id":"3307",
         "name":"Pomacanchi",
         "code":"05",
         "label":"Pomacanchi, Acomayo",
         "search":"pomacanchi acomayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3302"
      },
      {
         "id":"3308",
         "name":"Rondocan",
         "code":"06",
         "label":"Rondocan, Acomayo",
         "search":"rondocan acomayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3302"
      },
      {
         "id":"3309",
         "name":"Sangarara",
         "code":"07",
         "label":"Sangarara, Acomayo",
         "search":"sangarara acomayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3302"
      }
   ],
   "3310":[
      {
         "id":"3312",
         "name":"Ancahuasi",
         "code":"02",
         "label":"Ancahuasi, Anta",
         "search":"ancahuasi anta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3310"
      },
      {
         "id":"3311",
         "name":"Anta",
         "code":"01",
         "label":"Anta, Anta",
         "search":"anta anta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3310"
      },
      {
         "id":"3313",
         "name":"Cachimayo",
         "code":"03",
         "label":"Cachimayo, Anta",
         "search":"cachimayo anta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3310"
      },
      {
         "id":"3314",
         "name":"Chinchaypujio",
         "code":"04",
         "label":"Chinchaypujio, Anta",
         "search":"chinchaypujio anta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3310"
      },
      {
         "id":"3315",
         "name":"Huarocondo",
         "code":"05",
         "label":"Huarocondo, Anta",
         "search":"huarocondo anta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3310"
      },
      {
         "id":"3316",
         "name":"Limatambo",
         "code":"06",
         "label":"Limatambo, Anta",
         "search":"limatambo anta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3310"
      },
      {
         "id":"3317",
         "name":"Mollepata",
         "code":"07",
         "label":"Mollepata, Anta",
         "search":"mollepata anta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3310"
      },
      {
         "id":"3318",
         "name":"Pucyura",
         "code":"08",
         "label":"Pucyura, Anta",
         "search":"pucyura anta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3310"
      },
      {
         "id":"3319",
         "name":"Zurite",
         "code":"09",
         "label":"Zurite, Anta",
         "search":"zurite anta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3310"
      }
   ],
   "3320":[
      {
         "id":"3321",
         "name":"Calca",
         "code":"01",
         "label":"Calca, Calca",
         "search":"calca calca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3320"
      },
      {
         "id":"3322",
         "name":"Coya",
         "code":"02",
         "label":"Coya, Calca",
         "search":"coya calca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3320"
      },
      {
         "id":"3323",
         "name":"Lamay",
         "code":"03",
         "label":"Lamay, Calca",
         "search":"lamay calca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3320"
      },
      {
         "id":"3324",
         "name":"Lares",
         "code":"04",
         "label":"Lares, Calca",
         "search":"lares calca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3320"
      },
      {
         "id":"3325",
         "name":"Pisac",
         "code":"05",
         "label":"Pisac, Calca",
         "search":"pisac calca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3320"
      },
      {
         "id":"3326",
         "name":"San Salvador",
         "code":"06",
         "label":"San Salvador, Calca",
         "search":"san salvador calca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3320"
      },
      {
         "id":"3327",
         "name":"Taray",
         "code":"07",
         "label":"Taray, Calca",
         "search":"taray calca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3320"
      },
      {
         "id":"3328",
         "name":"Yanatile",
         "code":"08",
         "label":"Yanatile, Calca",
         "search":"yanatile calca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3320"
      }
   ],
   "3329":[
      {
         "id":"3331",
         "name":"Checca",
         "code":"02",
         "label":"Checca, Canas",
         "search":"checca canas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3329"
      },
      {
         "id":"3332",
         "name":"Kunturkanki",
         "code":"03",
         "label":"Kunturkanki, Canas",
         "search":"kunturkanki canas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3329"
      },
      {
         "id":"3333",
         "name":"Langui",
         "code":"04",
         "label":"Langui, Canas",
         "search":"langui canas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3329"
      },
      {
         "id":"3334",
         "name":"Layo",
         "code":"05",
         "label":"Layo, Canas",
         "search":"layo canas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3329"
      },
      {
         "id":"3335",
         "name":"Pampamarca",
         "code":"06",
         "label":"Pampamarca, Canas",
         "search":"pampamarca canas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3329"
      },
      {
         "id":"3336",
         "name":"Quehue",
         "code":"07",
         "label":"Quehue, Canas",
         "search":"quehue canas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3329"
      },
      {
         "id":"3337",
         "name":"Tupac Amaru",
         "code":"08",
         "label":"Tupac Amaru, Canas",
         "search":"tupac amaru canas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3329"
      },
      {
         "id":"3330",
         "name":"Yanaoca",
         "code":"01",
         "label":"Yanaoca, Canas",
         "search":"yanaoca canas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3329"
      }
   ],
   "3338":[
      {
         "id":"3340",
         "name":"Checacupe",
         "code":"02",
         "label":"Checacupe, Canchis",
         "search":"checacupe canchis",
         "children_count":"0",
         "level":"3",
         "parent_id":"3338"
      },
      {
         "id":"3341",
         "name":"Combapata",
         "code":"03",
         "label":"Combapata, Canchis",
         "search":"combapata canchis",
         "children_count":"0",
         "level":"3",
         "parent_id":"3338"
      },
      {
         "id":"3342",
         "name":"Marangani",
         "code":"04",
         "label":"Marangani, Canchis",
         "search":"marangani canchis",
         "children_count":"0",
         "level":"3",
         "parent_id":"3338"
      },
      {
         "id":"3343",
         "name":"Pitumarca",
         "code":"05",
         "label":"Pitumarca, Canchis",
         "search":"pitumarca canchis",
         "children_count":"0",
         "level":"3",
         "parent_id":"3338"
      },
      {
         "id":"3344",
         "name":"San Pablo",
         "code":"06",
         "label":"San Pablo, Canchis",
         "search":"san pablo canchis",
         "children_count":"0",
         "level":"3",
         "parent_id":"3338"
      },
      {
         "id":"3345",
         "name":"San Pedro",
         "code":"07",
         "label":"San Pedro, Canchis",
         "search":"san pedro canchis",
         "children_count":"0",
         "level":"3",
         "parent_id":"3338"
      },
      {
         "id":"3339",
         "name":"Sicuani",
         "code":"01",
         "label":"Sicuani, Canchis",
         "search":"sicuani canchis",
         "children_count":"0",
         "level":"3",
         "parent_id":"3338"
      },
      {
         "id":"3346",
         "name":"Tinta",
         "code":"08",
         "label":"Tinta, Canchis",
         "search":"tinta canchis",
         "children_count":"0",
         "level":"3",
         "parent_id":"3338"
      }
   ],
   "3347":[
      {
         "id":"3349",
         "name":"Capacmarca",
         "code":"02",
         "label":"Capacmarca, Chumbivilcas",
         "search":"capacmarca chumbivilcas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3347"
      },
      {
         "id":"3350",
         "name":"Chamaca",
         "code":"03",
         "label":"Chamaca, Chumbivilcas",
         "search":"chamaca chumbivilcas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3347"
      },
      {
         "id":"3351",
         "name":"Colquemarca",
         "code":"04",
         "label":"Colquemarca, Chumbivilcas",
         "search":"colquemarca chumbivilcas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3347"
      },
      {
         "id":"3352",
         "name":"Livitaca",
         "code":"05",
         "label":"Livitaca, Chumbivilcas",
         "search":"livitaca chumbivilcas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3347"
      },
      {
         "id":"3353",
         "name":"Llusco",
         "code":"06",
         "label":"Llusco, Chumbivilcas",
         "search":"llusco chumbivilcas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3347"
      },
      {
         "id":"3354",
         "name":"Quiqota",
         "code":"07",
         "label":"Quiqota, Chumbivilcas",
         "search":"quiqota chumbivilcas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3347"
      },
      {
         "id":"3348",
         "name":"Santo Tomas",
         "code":"01",
         "label":"Santo Tomas, Chumbivilcas",
         "search":"santo tomas chumbivilcas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3347"
      },
      {
         "id":"3355",
         "name":"Velille",
         "code":"08",
         "label":"Velille, Chumbivilcas",
         "search":"velille chumbivilcas",
         "children_count":"0",
         "level":"3",
         "parent_id":"3347"
      }
   ],
   "3293":[
      {
         "id":"3295",
         "name":"Ccorca",
         "code":"02",
         "label":"Ccorca, Cusco",
         "search":"ccorca cusco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3293"
      },
      {
         "id":"3294",
         "name":"Cusco",
         "code":"01",
         "label":"Cusco, Cusco",
         "search":"cusco cusco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3293"
      },
      {
         "id":"3296",
         "name":"Poroy",
         "code":"03",
         "label":"Poroy, Cusco",
         "search":"poroy cusco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3293"
      },
      {
         "id":"3297",
         "name":"San Jeronimo",
         "code":"04",
         "label":"San Jeronimo, Cusco",
         "search":"san jeronimo cusco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3293"
      },
      {
         "id":"3298",
         "name":"San Sebastian",
         "code":"05",
         "label":"San Sebastian, Cusco",
         "search":"san sebastian cusco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3293"
      },
      {
         "id":"3299",
         "name":"Santiago",
         "code":"06",
         "label":"Santiago, Cusco",
         "search":"santiago cusco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3293"
      },
      {
         "id":"3300",
         "name":"Saylla",
         "code":"07",
         "label":"Saylla, Cusco",
         "search":"saylla cusco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3293"
      },
      {
         "id":"3301",
         "name":"Wanchaq",
         "code":"08",
         "label":"Wanchaq, Cusco",
         "search":"wanchaq cusco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3293"
      }
   ],
   "3356":[
      {
         "id":"3364",
         "name":"Alto Pichigua",
         "code":"08",
         "label":"Alto Pichigua, Espinar",
         "search":"alto pichigua espinar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3356"
      },
      {
         "id":"3358",
         "name":"Condoroma",
         "code":"02",
         "label":"Condoroma, Espinar",
         "search":"condoroma espinar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3356"
      },
      {
         "id":"3359",
         "name":"Coporaque",
         "code":"03",
         "label":"Coporaque, Espinar",
         "search":"coporaque espinar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3356"
      },
      {
         "id":"3357",
         "name":"Espinar",
         "code":"01",
         "label":"Espinar, Espinar",
         "search":"espinar espinar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3356"
      },
      {
         "id":"3360",
         "name":"Ocoruro",
         "code":"04",
         "label":"Ocoruro, Espinar",
         "search":"ocoruro espinar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3356"
      },
      {
         "id":"3361",
         "name":"Pallpata",
         "code":"05",
         "label":"Pallpata, Espinar",
         "search":"pallpata espinar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3356"
      },
      {
         "id":"3362",
         "name":"Pichigua",
         "code":"06",
         "label":"Pichigua, Espinar",
         "search":"pichigua espinar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3356"
      },
      {
         "id":"3363",
         "name":"Suyckutambo",
         "code":"07",
         "label":"Suyckutambo, Espinar",
         "search":"suyckutambo espinar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3356"
      }
   ],
   "3365":[
      {
         "id":"3367",
         "name":"Echarate",
         "code":"02",
         "label":"Echarate, La Convencion",
         "search":"echarate la convencion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3365"
      },
      {
         "id":"3368",
         "name":"Huayopata",
         "code":"03",
         "label":"Huayopata, La Convencion",
         "search":"huayopata la convencion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3365"
      },
      {
         "id":"3369",
         "name":"Maranura",
         "code":"04",
         "label":"Maranura, La Convencion",
         "search":"maranura la convencion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3365"
      },
      {
         "id":"3370",
         "name":"Ocobamba",
         "code":"05",
         "label":"Ocobamba, La Convencion",
         "search":"ocobamba la convencion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3365"
      },
      {
         "id":"3375",
         "name":"Pichari",
         "code":"10",
         "label":"Pichari, La Convencion",
         "search":"pichari la convencion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3365"
      },
      {
         "id":"3371",
         "name":"Quellouno",
         "code":"06",
         "label":"Quellouno, La Convencion",
         "search":"quellouno la convencion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3365"
      },
      {
         "id":"3372",
         "name":"Quimbiri",
         "code":"07",
         "label":"Quimbiri, La Convencion",
         "search":"quimbiri la convencion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3365"
      },
      {
         "id":"3366",
         "name":"Santa Ana",
         "code":"01",
         "label":"Santa Ana, La Convencion",
         "search":"santa ana la convencion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3365"
      },
      {
         "id":"3373",
         "name":"Santa Teresa",
         "code":"08",
         "label":"Santa Teresa, La Convencion",
         "search":"santa teresa la convencion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3365"
      },
      {
         "id":"3374",
         "name":"Vilcabamba",
         "code":"09",
         "label":"Vilcabamba, La Convencion",
         "search":"vilcabamba la convencion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3365"
      }
   ],
   "3376":[
      {
         "id":"3378",
         "name":"Accha",
         "code":"02",
         "label":"Accha, Paruro",
         "search":"accha paruro",
         "children_count":"0",
         "level":"3",
         "parent_id":"3376"
      },
      {
         "id":"3379",
         "name":"Ccapi",
         "code":"03",
         "label":"Ccapi, Paruro",
         "search":"ccapi paruro",
         "children_count":"0",
         "level":"3",
         "parent_id":"3376"
      },
      {
         "id":"3380",
         "name":"Colcha",
         "code":"04",
         "label":"Colcha, Paruro",
         "search":"colcha paruro",
         "children_count":"0",
         "level":"3",
         "parent_id":"3376"
      },
      {
         "id":"3381",
         "name":"Huanoquite",
         "code":"05",
         "label":"Huanoquite, Paruro",
         "search":"huanoquite paruro",
         "children_count":"0",
         "level":"3",
         "parent_id":"3376"
      },
      {
         "id":"3382",
         "name":"Omacha",
         "code":"06",
         "label":"Omacha, Paruro",
         "search":"omacha paruro",
         "children_count":"0",
         "level":"3",
         "parent_id":"3376"
      },
      {
         "id":"3383",
         "name":"Paccaritambo",
         "code":"07",
         "label":"Paccaritambo, Paruro",
         "search":"paccaritambo paruro",
         "children_count":"0",
         "level":"3",
         "parent_id":"3376"
      },
      {
         "id":"3377",
         "name":"Paruro",
         "code":"01",
         "label":"Paruro, Paruro",
         "search":"paruro paruro",
         "children_count":"0",
         "level":"3",
         "parent_id":"3376"
      },
      {
         "id":"3384",
         "name":"Pillpinto",
         "code":"08",
         "label":"Pillpinto, Paruro",
         "search":"pillpinto paruro",
         "children_count":"0",
         "level":"3",
         "parent_id":"3376"
      },
      {
         "id":"3385",
         "name":"Yaurisque",
         "code":"09",
         "label":"Yaurisque, Paruro",
         "search":"yaurisque paruro",
         "children_count":"0",
         "level":"3",
         "parent_id":"3376"
      }
   ],
   "3386":[
      {
         "id":"3388",
         "name":"Caicay",
         "code":"02",
         "label":"Caicay, Paucartambo",
         "search":"caicay paucartambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3386"
      },
      {
         "id":"3389",
         "name":"Challabamba",
         "code":"03",
         "label":"Challabamba, Paucartambo",
         "search":"challabamba paucartambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3386"
      },
      {
         "id":"3390",
         "name":"Colquepata",
         "code":"04",
         "label":"Colquepata, Paucartambo",
         "search":"colquepata paucartambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3386"
      },
      {
         "id":"3391",
         "name":"Huancarani",
         "code":"05",
         "label":"Huancarani, Paucartambo",
         "search":"huancarani paucartambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3386"
      },
      {
         "id":"3392",
         "name":"Kosqipata",
         "code":"06",
         "label":"Kosqipata, Paucartambo",
         "search":"kosqipata paucartambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3386"
      },
      {
         "id":"3387",
         "name":"Paucartambo",
         "code":"01",
         "label":"Paucartambo, Paucartambo",
         "search":"paucartambo paucartambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3386"
      }
   ],
   "3393":[
      {
         "id":"3395",
         "name":"Andahuaylillas",
         "code":"02",
         "label":"Andahuaylillas, Quispicanchi",
         "search":"andahuaylillas quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      },
      {
         "id":"3396",
         "name":"Camanti",
         "code":"03",
         "label":"Camanti, Quispicanchi",
         "search":"camanti quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      },
      {
         "id":"3397",
         "name":"Ccarhuayo",
         "code":"04",
         "label":"Ccarhuayo, Quispicanchi",
         "search":"ccarhuayo quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      },
      {
         "id":"3398",
         "name":"Ccatca",
         "code":"05",
         "label":"Ccatca, Quispicanchi",
         "search":"ccatca quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      },
      {
         "id":"3399",
         "name":"Cusipata",
         "code":"06",
         "label":"Cusipata, Quispicanchi",
         "search":"cusipata quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      },
      {
         "id":"3400",
         "name":"Huaro",
         "code":"07",
         "label":"Huaro, Quispicanchi",
         "search":"huaro quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      },
      {
         "id":"3401",
         "name":"Lucre",
         "code":"08",
         "label":"Lucre, Quispicanchi",
         "search":"lucre quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      },
      {
         "id":"3402",
         "name":"Marcapata",
         "code":"09",
         "label":"Marcapata, Quispicanchi",
         "search":"marcapata quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      },
      {
         "id":"3403",
         "name":"Ocongate",
         "code":"10",
         "label":"Ocongate, Quispicanchi",
         "search":"ocongate quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      },
      {
         "id":"3404",
         "name":"Oropesa",
         "code":"11",
         "label":"Oropesa, Quispicanchi",
         "search":"oropesa quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      },
      {
         "id":"3405",
         "name":"Quiquijana",
         "code":"12",
         "label":"Quiquijana, Quispicanchi",
         "search":"quiquijana quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      },
      {
         "id":"3394",
         "name":"Urcos",
         "code":"01",
         "label":"Urcos, Quispicanchi",
         "search":"urcos quispicanchi",
         "children_count":"0",
         "level":"3",
         "parent_id":"3393"
      }
   ],
   "3406":[
      {
         "id":"3408",
         "name":"Chinchero",
         "code":"02",
         "label":"Chinchero, Urubamba",
         "search":"chinchero urubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3406"
      },
      {
         "id":"3409",
         "name":"Huayllabamba",
         "code":"03",
         "label":"Huayllabamba, Urubamba",
         "search":"huayllabamba urubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3406"
      },
      {
         "id":"3410",
         "name":"Machupicchu",
         "code":"04",
         "label":"Machupicchu, Urubamba",
         "search":"machupicchu urubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3406"
      },
      {
         "id":"3411",
         "name":"Maras",
         "code":"05",
         "label":"Maras, Urubamba",
         "search":"maras urubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3406"
      },
      {
         "id":"3412",
         "name":"Ollantaytambo",
         "code":"06",
         "label":"Ollantaytambo, Urubamba",
         "search":"ollantaytambo urubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3406"
      },
      {
         "id":"3407",
         "name":"Urubamba",
         "code":"01",
         "label":"Urubamba, Urubamba",
         "search":"urubamba urubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3406"
      },
      {
         "id":"3413",
         "name":"Yucay",
         "code":"07",
         "label":"Yucay, Urubamba",
         "search":"yucay urubamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3406"
      }
   ],
   "3435":[
      {
         "id":"3436",
         "name":"Acobamba",
         "code":"01",
         "label":"Acobamba, Acobamba",
         "search":"acobamba acobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3435"
      },
      {
         "id":"3437",
         "name":"Andabamba",
         "code":"02",
         "label":"Andabamba, Acobamba",
         "search":"andabamba acobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3435"
      },
      {
         "id":"3438",
         "name":"Anta",
         "code":"03",
         "label":"Anta, Acobamba",
         "search":"anta acobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3435"
      },
      {
         "id":"3439",
         "name":"Caja",
         "code":"04",
         "label":"Caja, Acobamba",
         "search":"caja acobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3435"
      },
      {
         "id":"3440",
         "name":"Marcas",
         "code":"05",
         "label":"Marcas, Acobamba",
         "search":"marcas acobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3435"
      },
      {
         "id":"3441",
         "name":"Paucara",
         "code":"06",
         "label":"Paucara, Acobamba",
         "search":"paucara acobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3435"
      },
      {
         "id":"3442",
         "name":"Pomacocha",
         "code":"07",
         "label":"Pomacocha, Acobamba",
         "search":"pomacocha acobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3435"
      },
      {
         "id":"3443",
         "name":"Rosario",
         "code":"08",
         "label":"Rosario, Acobamba",
         "search":"rosario acobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3435"
      }
   ],
   "3444":[
      {
         "id":"3446",
         "name":"Anchonga",
         "code":"02",
         "label":"Anchonga, Angaraes",
         "search":"anchonga angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      },
      {
         "id":"3447",
         "name":"Callanmarca",
         "code":"03",
         "label":"Callanmarca, Angaraes",
         "search":"callanmarca angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      },
      {
         "id":"3448",
         "name":"Ccochaccasa",
         "code":"04",
         "label":"Ccochaccasa, Angaraes",
         "search":"ccochaccasa angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      },
      {
         "id":"3449",
         "name":"Chincho",
         "code":"05",
         "label":"Chincho, Angaraes",
         "search":"chincho angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      },
      {
         "id":"3450",
         "name":"Congalla",
         "code":"06",
         "label":"Congalla, Angaraes",
         "search":"congalla angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      },
      {
         "id":"3451",
         "name":"Huanca-Huanca",
         "code":"07",
         "label":"Huanca-Huanca, Angaraes",
         "search":"huanca-huanca angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      },
      {
         "id":"3452",
         "name":"Huayllay Grande",
         "code":"08",
         "label":"Huayllay Grande, Angaraes",
         "search":"huayllay grande angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      },
      {
         "id":"3453",
         "name":"Julcamarca",
         "code":"09",
         "label":"Julcamarca, Angaraes",
         "search":"julcamarca angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      },
      {
         "id":"3445",
         "name":"Lircay",
         "code":"01",
         "label":"Lircay, Angaraes",
         "search":"lircay angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      },
      {
         "id":"3454",
         "name":"San Antonio de Antaparco",
         "code":"10",
         "label":"San Antonio de Antaparco, Angaraes",
         "search":"san antonio de antaparco angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      },
      {
         "id":"3455",
         "name":"Santo Tomas de Pata",
         "code":"11",
         "label":"Santo Tomas de Pata, Angaraes",
         "search":"santo tomas de pata angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      },
      {
         "id":"3456",
         "name":"Secclla",
         "code":"12",
         "label":"Secclla, Angaraes",
         "search":"secclla angaraes",
         "children_count":"0",
         "level":"3",
         "parent_id":"3444"
      }
   ],
   "3457":[
      {
         "id":"3459",
         "name":"Arma",
         "code":"02",
         "label":"Arma, Castrovirreyna",
         "search":"arma castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3460",
         "name":"Aurahua",
         "code":"03",
         "label":"Aurahua, Castrovirreyna",
         "search":"aurahua castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3461",
         "name":"Capillas",
         "code":"04",
         "label":"Capillas, Castrovirreyna",
         "search":"capillas castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3458",
         "name":"Castrovirreyna",
         "code":"01",
         "label":"Castrovirreyna, Castrovirreyna",
         "search":"castrovirreyna castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3462",
         "name":"Chupamarca",
         "code":"05",
         "label":"Chupamarca, Castrovirreyna",
         "search":"chupamarca castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3463",
         "name":"Cocas",
         "code":"06",
         "label":"Cocas, Castrovirreyna",
         "search":"cocas castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3464",
         "name":"Huachos",
         "code":"07",
         "label":"Huachos, Castrovirreyna",
         "search":"huachos castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3465",
         "name":"Huamatambo",
         "code":"08",
         "label":"Huamatambo, Castrovirreyna",
         "search":"huamatambo castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3466",
         "name":"Mollepampa",
         "code":"09",
         "label":"Mollepampa, Castrovirreyna",
         "search":"mollepampa castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3467",
         "name":"San Juan",
         "code":"10",
         "label":"San Juan, Castrovirreyna",
         "search":"san juan castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3468",
         "name":"Santa Ana",
         "code":"11",
         "label":"Santa Ana, Castrovirreyna",
         "search":"santa ana castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3469",
         "name":"Tantara",
         "code":"12",
         "label":"Tantara, Castrovirreyna",
         "search":"tantara castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      },
      {
         "id":"3470",
         "name":"Ticrapo",
         "code":"13",
         "label":"Ticrapo, Castrovirreyna",
         "search":"ticrapo castrovirreyna",
         "children_count":"0",
         "level":"3",
         "parent_id":"3457"
      }
   ],
   "3471":[
      {
         "id":"3473",
         "name":"Anco",
         "code":"02",
         "label":"Anco, Churcampa",
         "search":"anco churcampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3471"
      },
      {
         "id":"3474",
         "name":"Chinchihuasi",
         "code":"03",
         "label":"Chinchihuasi, Churcampa",
         "search":"chinchihuasi churcampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3471"
      },
      {
         "id":"3472",
         "name":"Churcampa",
         "code":"01",
         "label":"Churcampa, Churcampa",
         "search":"churcampa churcampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3471"
      },
      {
         "id":"3475",
         "name":"El Carmen",
         "code":"04",
         "label":"El Carmen, Churcampa",
         "search":"el carmen churcampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3471"
      },
      {
         "id":"3476",
         "name":"La Merced",
         "code":"05",
         "label":"La Merced, Churcampa",
         "search":"la merced churcampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3471"
      },
      {
         "id":"3477",
         "name":"Locroja",
         "code":"06",
         "label":"Locroja, Churcampa",
         "search":"locroja churcampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3471"
      },
      {
         "id":"3481",
         "name":"Pachamarca",
         "code":"10",
         "label":"Pachamarca, Churcampa",
         "search":"pachamarca churcampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3471"
      },
      {
         "id":"3478",
         "name":"Paucarbamba",
         "code":"07",
         "label":"Paucarbamba, Churcampa",
         "search":"paucarbamba churcampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3471"
      },
      {
         "id":"3479",
         "name":"San Miguel de Mayocc",
         "code":"08",
         "label":"San Miguel de Mayocc, Churcampa",
         "search":"san miguel de mayocc churcampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3471"
      },
      {
         "id":"3480",
         "name":"San Pedro de Coris",
         "code":"09",
         "label":"San Pedro de Coris, Churcampa",
         "search":"san pedro de coris churcampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3471"
      }
   ],
   "3415":[
      {
         "id":"3417",
         "name":"Acobambilla",
         "code":"02",
         "label":"Acobambilla, Huancavelica",
         "search":"acobambilla huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3418",
         "name":"Acoria",
         "code":"03",
         "label":"Acoria, Huancavelica",
         "search":"acoria huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3433",
         "name":"Ascensi\u00f3n",
         "code":"18",
         "label":"Ascensi\u00f3n, Huancavelica",
         "search":"ascensi\u00f3n huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3419",
         "name":"Conayca",
         "code":"04",
         "label":"Conayca, Huancavelica",
         "search":"conayca huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3420",
         "name":"Cuenca",
         "code":"05",
         "label":"Cuenca, Huancavelica",
         "search":"cuenca huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3421",
         "name":"Huachocolpa",
         "code":"06",
         "label":"Huachocolpa, Huancavelica",
         "search":"huachocolpa huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3416",
         "name":"Huancavelica",
         "code":"01",
         "label":"Huancavelica, Huancavelica",
         "search":"huancavelica huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3434",
         "name":"Huando",
         "code":"19",
         "label":"Huando, Huancavelica",
         "search":"huando huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3422",
         "name":"Huayllahuara",
         "code":"07",
         "label":"Huayllahuara, Huancavelica",
         "search":"huayllahuara huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3423",
         "name":"Izcuchaca",
         "code":"08",
         "label":"Izcuchaca, Huancavelica",
         "search":"izcuchaca huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3424",
         "name":"Laria",
         "code":"09",
         "label":"Laria, Huancavelica",
         "search":"laria huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3425",
         "name":"Manta",
         "code":"10",
         "label":"Manta, Huancavelica",
         "search":"manta huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3426",
         "name":"Mariscal Caceres",
         "code":"11",
         "label":"Mariscal Caceres, Huancavelica",
         "search":"mariscal caceres huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3427",
         "name":"Moya",
         "code":"12",
         "label":"Moya, Huancavelica",
         "search":"moya huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3428",
         "name":"Nuevo Occoro",
         "code":"13",
         "label":"Nuevo Occoro, Huancavelica",
         "search":"nuevo occoro huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3429",
         "name":"Palca",
         "code":"14",
         "label":"Palca, Huancavelica",
         "search":"palca huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3430",
         "name":"Pilchaca",
         "code":"15",
         "label":"Pilchaca, Huancavelica",
         "search":"pilchaca huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3431",
         "name":"Vilca",
         "code":"16",
         "label":"Vilca, Huancavelica",
         "search":"vilca huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      },
      {
         "id":"3432",
         "name":"Yauli",
         "code":"17",
         "label":"Yauli, Huancavelica",
         "search":"yauli huancavelica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3415"
      }
   ],
   "3482":[
      {
         "id":"3484",
         "name":"Ayavi",
         "code":"02",
         "label":"Ayavi, Huaytara",
         "search":"ayavi huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3485",
         "name":"Cordova",
         "code":"03",
         "label":"Cordova, Huaytara",
         "search":"cordova huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3486",
         "name":"Huayacundo Arma",
         "code":"04",
         "label":"Huayacundo Arma, Huaytara",
         "search":"huayacundo arma huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3483",
         "name":"Huaytara",
         "code":"01",
         "label":"Huaytara, Huaytara",
         "search":"huaytara huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3487",
         "name":"Laramarca",
         "code":"05",
         "label":"Laramarca, Huaytara",
         "search":"laramarca huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3488",
         "name":"Ocoyo",
         "code":"06",
         "label":"Ocoyo, Huaytara",
         "search":"ocoyo huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3489",
         "name":"Pilpichaca",
         "code":"07",
         "label":"Pilpichaca, Huaytara",
         "search":"pilpichaca huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3490",
         "name":"Querco",
         "code":"08",
         "label":"Querco, Huaytara",
         "search":"querco huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3491",
         "name":"Quito-Arma",
         "code":"09",
         "label":"Quito-Arma, Huaytara",
         "search":"quito-arma huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3492",
         "name":"San Antonio de Cusicancha",
         "code":"10",
         "label":"San Antonio de Cusicancha, Huaytara",
         "search":"san antonio de cusicancha huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3493",
         "name":"San Francisco de Sangayaico",
         "code":"11",
         "label":"San Francisco de Sangayaico, Huaytara",
         "search":"san francisco de sangayaico huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3494",
         "name":"San Isidro",
         "code":"12",
         "label":"San Isidro, Huaytara",
         "search":"san isidro huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3495",
         "name":"Santiago de Chocorvos",
         "code":"13",
         "label":"Santiago de Chocorvos, Huaytara",
         "search":"santiago de chocorvos huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3496",
         "name":"Santiago de Quirahuara",
         "code":"14",
         "label":"Santiago de Quirahuara, Huaytara",
         "search":"santiago de quirahuara huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3497",
         "name":"Santo Domingo de Capillas",
         "code":"15",
         "label":"Santo Domingo de Capillas, Huaytara",
         "search":"santo domingo de capillas huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      },
      {
         "id":"3498",
         "name":"Tambo",
         "code":"16",
         "label":"Tambo, Huaytara",
         "search":"tambo huaytara",
         "children_count":"0",
         "level":"3",
         "parent_id":"3482"
      }
   ],
   "3499":[
      {
         "id":"3501",
         "name":"Acostambo",
         "code":"02",
         "label":"Acostambo, Tayacaja",
         "search":"acostambo tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3502",
         "name":"Acraquia",
         "code":"03",
         "label":"Acraquia, Tayacaja",
         "search":"acraquia tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3503",
         "name":"Ahuaycha",
         "code":"04",
         "label":"Ahuaycha, Tayacaja",
         "search":"ahuaycha tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3504",
         "name":"Colcabamba",
         "code":"05",
         "label":"Colcabamba, Tayacaja",
         "search":"colcabamba tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3505",
         "name":"Daniel Hernandez",
         "code":"06",
         "label":"Daniel Hernandez, Tayacaja",
         "search":"daniel hernandez tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3506",
         "name":"Huachocolpa",
         "code":"07",
         "label":"Huachocolpa, Tayacaja",
         "search":"huachocolpa tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3507",
         "name":"Huando",
         "code":"08",
         "label":"Huando, Tayacaja",
         "search":"huando tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3508",
         "name":"Huaribamba",
         "code":"09",
         "label":"Huaribamba, Tayacaja",
         "search":"huaribamba tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3511",
         "name":"Pachamarca",
         "code":"12",
         "label":"Pachamarca, Tayacaja",
         "search":"pachamarca tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3500",
         "name":"Pampas",
         "code":"01",
         "label":"Pampas, Tayacaja",
         "search":"pampas tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3510",
         "name":"Pazos",
         "code":"11",
         "label":"Pazos, Tayacaja",
         "search":"pazos tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3509",
         "name":"Qahuimpuquio",
         "code":"10",
         "label":"Qahuimpuquio, Tayacaja",
         "search":"qahuimpuquio tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3512",
         "name":"Quishuar",
         "code":"13",
         "label":"Quishuar, Tayacaja",
         "search":"quishuar tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3513",
         "name":"Salcabamba",
         "code":"14",
         "label":"Salcabamba, Tayacaja",
         "search":"salcabamba tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3514",
         "name":"Salcahuasi",
         "code":"15",
         "label":"Salcahuasi, Tayacaja",
         "search":"salcahuasi tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3515",
         "name":"San Marcos de Rocchac",
         "code":"16",
         "label":"San Marcos de Rocchac, Tayacaja",
         "search":"san marcos de rocchac tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3516",
         "name":"Surcubamba",
         "code":"17",
         "label":"Surcubamba, Tayacaja",
         "search":"surcubamba tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      },
      {
         "id":"3517",
         "name":"Tintay Puncu",
         "code":"18",
         "label":"Tintay Puncu, Tayacaja",
         "search":"tintay puncu tayacaja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3499"
      }
   ],
   "3531":[
      {
         "id":"3532",
         "name":"Ambo",
         "code":"01",
         "label":"Ambo, Ambo",
         "search":"ambo ambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3531"
      },
      {
         "id":"3533",
         "name":"Cayna",
         "code":"02",
         "label":"Cayna, Ambo",
         "search":"cayna ambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3531"
      },
      {
         "id":"3534",
         "name":"Colpas",
         "code":"03",
         "label":"Colpas, Ambo",
         "search":"colpas ambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3531"
      },
      {
         "id":"3535",
         "name":"Conchamarca",
         "code":"04",
         "label":"Conchamarca, Ambo",
         "search":"conchamarca ambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3531"
      },
      {
         "id":"3536",
         "name":"Huacar",
         "code":"05",
         "label":"Huacar, Ambo",
         "search":"huacar ambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3531"
      },
      {
         "id":"3537",
         "name":"San Francisco",
         "code":"06",
         "label":"San Francisco, Ambo",
         "search":"san francisco ambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3531"
      },
      {
         "id":"3538",
         "name":"San Rafael",
         "code":"07",
         "label":"San Rafael, Ambo",
         "search":"san rafael ambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3531"
      },
      {
         "id":"3539",
         "name":"Tomay Kichwa",
         "code":"08",
         "label":"Tomay Kichwa, Ambo",
         "search":"tomay kichwa ambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3531"
      }
   ],
   "3540":[
      {
         "id":"3542",
         "name":"Chuquis",
         "code":"07",
         "label":"Chuquis, Dos de Mayo",
         "search":"chuquis dos de mayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3540"
      },
      {
         "id":"3541",
         "name":"La Union",
         "code":"01",
         "label":"La Union, Dos de Mayo",
         "search":"la union dos de mayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3540"
      },
      {
         "id":"3543",
         "name":"Marias",
         "code":"11",
         "label":"Marias, Dos de Mayo",
         "search":"marias dos de mayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3540"
      },
      {
         "id":"3544",
         "name":"Pachas",
         "code":"13",
         "label":"Pachas, Dos de Mayo",
         "search":"pachas dos de mayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3540"
      },
      {
         "id":"3545",
         "name":"Quivilla",
         "code":"16",
         "label":"Quivilla, Dos de Mayo",
         "search":"quivilla dos de mayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3540"
      },
      {
         "id":"3546",
         "name":"Ripan",
         "code":"17",
         "label":"Ripan, Dos de Mayo",
         "search":"ripan dos de mayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3540"
      },
      {
         "id":"3547",
         "name":"Shunqui",
         "code":"21",
         "label":"Shunqui, Dos de Mayo",
         "search":"shunqui dos de mayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3540"
      },
      {
         "id":"3548",
         "name":"Sillapata",
         "code":"22",
         "label":"Sillapata, Dos de Mayo",
         "search":"sillapata dos de mayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3540"
      },
      {
         "id":"3549",
         "name":"Yanas",
         "code":"23",
         "label":"Yanas, Dos de Mayo",
         "search":"yanas dos de mayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3540"
      }
   ],
   "3550":[
      {
         "id":"3552",
         "name":"Canchabamba",
         "code":"02",
         "label":"Canchabamba, Huacaybamba",
         "search":"canchabamba huacaybamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3550"
      },
      {
         "id":"3553",
         "name":"Cochabamba",
         "code":"03",
         "label":"Cochabamba, Huacaybamba",
         "search":"cochabamba huacaybamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3550"
      },
      {
         "id":"3551",
         "name":"Huacaybamba",
         "code":"01",
         "label":"Huacaybamba, Huacaybamba",
         "search":"huacaybamba huacaybamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3550"
      },
      {
         "id":"3554",
         "name":"Pinra",
         "code":"04",
         "label":"Pinra, Huacaybamba",
         "search":"pinra huacaybamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"3550"
      }
   ],
   "3555":[
      {
         "id":"3557",
         "name":"Arancay",
         "code":"02",
         "label":"Arancay, Huamalies",
         "search":"arancay huamalies",
         "children_count":"0",
         "level":"3",
         "parent_id":"3555"
      },
      {
         "id":"3558",
         "name":"Chavin de Pariarca",
         "code":"03",
         "label":"Chavin de Pariarca, Huamalies",
         "search":"chavin de pariarca huamalies",
         "children_count":"0",
         "level":"3",
         "parent_id":"3555"
      },
      {
         "id":"3559",
         "name":"Jacas Grande",
         "code":"04",
         "label":"Jacas Grande, Huamalies",
         "search":"jacas grande huamalies",
         "children_count":"0",
         "level":"3",
         "parent_id":"3555"
      },
      {
         "id":"3560",
         "name":"Jircan",
         "code":"05",
         "label":"Jircan, Huamalies",
         "search":"jircan huamalies",
         "children_count":"0",
         "level":"3",
         "parent_id":"3555"
      },
      {
         "id":"3556",
         "name":"Llata",
         "code":"01",
         "label":"Llata, Huamalies",
         "search":"llata huamalies",
         "children_count":"0",
         "level":"3",
         "parent_id":"3555"
      },
      {
         "id":"3561",
         "name":"Miraflores",
         "code":"06",
         "label":"Miraflores, Huamalies",
         "search":"miraflores huamalies",
         "children_count":"0",
         "level":"3",
         "parent_id":"3555"
      },
      {
         "id":"3562",
         "name":"Monzon",
         "code":"07",
         "label":"Monzon, Huamalies",
         "search":"monzon huamalies",
         "children_count":"0",
         "level":"3",
         "parent_id":"3555"
      },
      {
         "id":"3563",
         "name":"Punchao",
         "code":"08",
         "label":"Punchao, Huamalies",
         "search":"punchao huamalies",
         "children_count":"0",
         "level":"3",
         "parent_id":"3555"
      },
      {
         "id":"3564",
         "name":"Puqos",
         "code":"09",
         "label":"Puqos, Huamalies",
         "search":"puqos huamalies",
         "children_count":"0",
         "level":"3",
         "parent_id":"3555"
      },
      {
         "id":"3565",
         "name":"Singa",
         "code":"10",
         "label":"Singa, Huamalies",
         "search":"singa huamalies",
         "children_count":"0",
         "level":"3",
         "parent_id":"3555"
      },
      {
         "id":"3566",
         "name":"Tantamayo",
         "code":"11",
         "label":"Tantamayo, Huamalies",
         "search":"tantamayo huamalies",
         "children_count":"0",
         "level":"3",
         "parent_id":"3555"
      }
   ],
   "3519":[
      {
         "id":"3521",
         "name":"Amarilis",
         "code":"02",
         "label":"Amarilis, Huanuco",
         "search":"amarilis huanuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3519"
      },
      {
         "id":"3522",
         "name":"Chinchao",
         "code":"03",
         "label":"Chinchao, Huanuco",
         "search":"chinchao huanuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3519"
      },
      {
         "id":"3523",
         "name":"Churubamba",
         "code":"04",
         "label":"Churubamba, Huanuco",
         "search":"churubamba huanuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3519"
      },
      {
         "id":"3520",
         "name":"Huanuco",
         "code":"01",
         "label":"Huanuco, Huanuco",
         "search":"huanuco huanuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3519"
      },
      {
         "id":"3524",
         "name":"Margos",
         "code":"05",
         "label":"Margos, Huanuco",
         "search":"margos huanuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3519"
      },
      {
         "id":"3530",
         "name":"Pillcomarca",
         "code":"11",
         "label":"Pillcomarca, Huanuco",
         "search":"pillcomarca huanuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3519"
      },
      {
         "id":"3525",
         "name":"Quisqui",
         "code":"06",
         "label":"Quisqui, Huanuco",
         "search":"quisqui huanuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3519"
      },
      {
         "id":"3526",
         "name":"San Francisco de Cayran",
         "code":"07",
         "label":"San Francisco de Cayran, Huanuco",
         "search":"san francisco de cayran huanuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3519"
      },
      {
         "id":"3527",
         "name":"San Pedro de Chaulan",
         "code":"08",
         "label":"San Pedro de Chaulan, Huanuco",
         "search":"san pedro de chaulan huanuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3519"
      },
      {
         "id":"3528",
         "name":"Santa Maria del Valle",
         "code":"09",
         "label":"Santa Maria del Valle, Huanuco",
         "search":"santa maria del valle huanuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3519"
      },
      {
         "id":"3529",
         "name":"Yarumayo",
         "code":"10",
         "label":"Yarumayo, Huanuco",
         "search":"yarumayo huanuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3519"
      }
   ],
   "3589":[
      {
         "id":"3591",
         "name":"Baqos",
         "code":"02",
         "label":"Baqos, Lauricocha",
         "search":"baqos lauricocha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3589"
      },
      {
         "id":"3590",
         "name":"Jesus",
         "code":"01",
         "label":"Jesus, Lauricocha",
         "search":"jesus lauricocha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3589"
      },
      {
         "id":"3592",
         "name":"Jivia",
         "code":"03",
         "label":"Jivia, Lauricocha",
         "search":"jivia lauricocha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3589"
      },
      {
         "id":"3593",
         "name":"Queropalca",
         "code":"04",
         "label":"Queropalca, Lauricocha",
         "search":"queropalca lauricocha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3589"
      },
      {
         "id":"3594",
         "name":"Rondos",
         "code":"05",
         "label":"Rondos, Lauricocha",
         "search":"rondos lauricocha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3589"
      },
      {
         "id":"3595",
         "name":"San Francisco de Asis",
         "code":"06",
         "label":"San Francisco de Asis, Lauricocha",
         "search":"san francisco de asis lauricocha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3589"
      },
      {
         "id":"3596",
         "name":"San Miguel de Cauri",
         "code":"07",
         "label":"San Miguel de Cauri, Lauricocha",
         "search":"san miguel de cauri lauricocha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3589"
      }
   ],
   "3567":[
      {
         "id":"3569",
         "name":"Daniel Alomias Robles",
         "code":"02",
         "label":"Daniel Alomias Robles, Leoncio Prado",
         "search":"daniel alomias robles leoncio prado",
         "children_count":"0",
         "level":"3",
         "parent_id":"3567"
      },
      {
         "id":"3570",
         "name":"Hermilio Valdizan",
         "code":"03",
         "label":"Hermilio Valdizan, Leoncio Prado",
         "search":"hermilio valdizan leoncio prado",
         "children_count":"0",
         "level":"3",
         "parent_id":"3567"
      },
      {
         "id":"3571",
         "name":"Jose Crespo y Castillo",
         "code":"04",
         "label":"Jose Crespo y Castillo, Leoncio Prado",
         "search":"jose crespo y castillo leoncio prado",
         "children_count":"0",
         "level":"3",
         "parent_id":"3567"
      },
      {
         "id":"3572",
         "name":"Luyando",
         "code":"05",
         "label":"Luyando, Leoncio Prado",
         "search":"luyando leoncio prado",
         "children_count":"0",
         "level":"3",
         "parent_id":"3567"
      },
      {
         "id":"3573",
         "name":"Mariano Damaso Beraun",
         "code":"06",
         "label":"Mariano Damaso Beraun, Leoncio Prado",
         "search":"mariano damaso beraun leoncio prado",
         "children_count":"0",
         "level":"3",
         "parent_id":"3567"
      },
      {
         "id":"3568",
         "name":"Rupa-Rupa",
         "code":"01",
         "label":"Rupa-Rupa, Leoncio Prado",
         "search":"rupa-rupa leoncio prado",
         "children_count":"0",
         "level":"3",
         "parent_id":"3567"
      }
   ],
   "3574":[
      {
         "id":"3576",
         "name":"Cholon",
         "code":"02",
         "label":"Cholon, Maraqon",
         "search":"cholon maraqon",
         "children_count":"0",
         "level":"3",
         "parent_id":"3574"
      },
      {
         "id":"3575",
         "name":"Huacrachuco",
         "code":"01",
         "label":"Huacrachuco, Maraqon",
         "search":"huacrachuco maraqon",
         "children_count":"0",
         "level":"3",
         "parent_id":"3574"
      },
      {
         "id":"3577",
         "name":"San Buenaventura",
         "code":"03",
         "label":"San Buenaventura, Maraqon",
         "search":"san buenaventura maraqon",
         "children_count":"0",
         "level":"3",
         "parent_id":"3574"
      }
   ],
   "3578":[
      {
         "id":"3580",
         "name":"Chaglla",
         "code":"02",
         "label":"Chaglla, Pachitea",
         "search":"chaglla pachitea",
         "children_count":"0",
         "level":"3",
         "parent_id":"3578"
      },
      {
         "id":"3581",
         "name":"Molino",
         "code":"03",
         "label":"Molino, Pachitea",
         "search":"molino pachitea",
         "children_count":"0",
         "level":"3",
         "parent_id":"3578"
      },
      {
         "id":"3579",
         "name":"Panao",
         "code":"01",
         "label":"Panao, Pachitea",
         "search":"panao pachitea",
         "children_count":"0",
         "level":"3",
         "parent_id":"3578"
      },
      {
         "id":"3582",
         "name":"Umari",
         "code":"04",
         "label":"Umari, Pachitea",
         "search":"umari pachitea",
         "children_count":"0",
         "level":"3",
         "parent_id":"3578"
      }
   ],
   "3583":[
      {
         "id":"3585",
         "name":"Codo del Pozuzo",
         "code":"02",
         "label":"Codo del Pozuzo, Puerto Inca",
         "search":"codo del pozuzo puerto inca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3583"
      },
      {
         "id":"3586",
         "name":"Honoria",
         "code":"03",
         "label":"Honoria, Puerto Inca",
         "search":"honoria puerto inca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3583"
      },
      {
         "id":"3584",
         "name":"Puerto Inca",
         "code":"01",
         "label":"Puerto Inca, Puerto Inca",
         "search":"puerto inca puerto inca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3583"
      },
      {
         "id":"3587",
         "name":"Tournavista",
         "code":"04",
         "label":"Tournavista, Puerto Inca",
         "search":"tournavista puerto inca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3583"
      },
      {
         "id":"3588",
         "name":"Yuyapichis",
         "code":"05",
         "label":"Yuyapichis, Puerto Inca",
         "search":"yuyapichis puerto inca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3583"
      }
   ],
   "3597":[
      {
         "id":"3599",
         "name":"Cahuac",
         "code":"02",
         "label":"Cahuac, Yarowilca",
         "search":"cahuac yarowilca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3597"
      },
      {
         "id":"3600",
         "name":"Chacabamba",
         "code":"03",
         "label":"Chacabamba, Yarowilca",
         "search":"chacabamba yarowilca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3597"
      },
      {
         "id":"3598",
         "name":"Chavinillo",
         "code":"01",
         "label":"Chavinillo, Yarowilca",
         "search":"chavinillo yarowilca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3597"
      },
      {
         "id":"3605",
         "name":"Choras",
         "code":"08",
         "label":"Choras, Yarowilca",
         "search":"choras yarowilca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3597"
      },
      {
         "id":"3601",
         "name":"Chupan",
         "code":"04",
         "label":"Chupan, Yarowilca",
         "search":"chupan yarowilca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3597"
      },
      {
         "id":"3602",
         "name":"Jacas Chico",
         "code":"05",
         "label":"Jacas Chico, Yarowilca",
         "search":"jacas chico yarowilca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3597"
      },
      {
         "id":"3603",
         "name":"Obas",
         "code":"06",
         "label":"Obas, Yarowilca",
         "search":"obas yarowilca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3597"
      },
      {
         "id":"3604",
         "name":"Pampamarca",
         "code":"07",
         "label":"Pampamarca, Yarowilca",
         "search":"pampamarca yarowilca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3597"
      }
   ],
   "3622":[
      {
         "id":"3624",
         "name":"Alto Laran",
         "code":"02",
         "label":"Alto Laran, Chincha",
         "search":"alto laran chincha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3622"
      },
      {
         "id":"3625",
         "name":"Chavin",
         "code":"03",
         "label":"Chavin, Chincha",
         "search":"chavin chincha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3622"
      },
      {
         "id":"3623",
         "name":"Chincha Alta",
         "code":"01",
         "label":"Chincha Alta, Chincha",
         "search":"chincha alta chincha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3622"
      },
      {
         "id":"3626",
         "name":"Chincha Baja",
         "code":"04",
         "label":"Chincha Baja, Chincha",
         "search":"chincha baja chincha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3622"
      },
      {
         "id":"3627",
         "name":"El Carmen",
         "code":"05",
         "label":"El Carmen, Chincha",
         "search":"el carmen chincha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3622"
      },
      {
         "id":"3628",
         "name":"Grocio Prado",
         "code":"06",
         "label":"Grocio Prado, Chincha",
         "search":"grocio prado chincha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3622"
      },
      {
         "id":"3629",
         "name":"Pueblo Nuevo",
         "code":"07",
         "label":"Pueblo Nuevo, Chincha",
         "search":"pueblo nuevo chincha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3622"
      },
      {
         "id":"3630",
         "name":"San Juan de Yanac",
         "code":"08",
         "label":"San Juan de Yanac, Chincha",
         "search":"san juan de yanac chincha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3622"
      },
      {
         "id":"3631",
         "name":"San Pedro de Huacarpana",
         "code":"09",
         "label":"San Pedro de Huacarpana, Chincha",
         "search":"san pedro de huacarpana chincha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3622"
      },
      {
         "id":"3632",
         "name":"Sunampe",
         "code":"10",
         "label":"Sunampe, Chincha",
         "search":"sunampe chincha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3622"
      },
      {
         "id":"3633",
         "name":"Tambo de Mora",
         "code":"11",
         "label":"Tambo de Mora, Chincha",
         "search":"tambo de mora chincha",
         "children_count":"0",
         "level":"3",
         "parent_id":"3622"
      }
   ],
   "3607":[
      {
         "id":"3608",
         "name":"Ica",
         "code":"01",
         "label":"Ica, Ica",
         "search":"ica ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3609",
         "name":"La Tinguiqa",
         "code":"02",
         "label":"La Tinguiqa, Ica",
         "search":"la tinguiqa ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3610",
         "name":"Los Aquijes",
         "code":"03",
         "label":"Los Aquijes, Ica",
         "search":"los aquijes ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3611",
         "name":"Ocucaje",
         "code":"04",
         "label":"Ocucaje, Ica",
         "search":"ocucaje ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3612",
         "name":"Pachacutec",
         "code":"05",
         "label":"Pachacutec, Ica",
         "search":"pachacutec ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3613",
         "name":"Parcona",
         "code":"06",
         "label":"Parcona, Ica",
         "search":"parcona ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3614",
         "name":"Pueblo Nuevo",
         "code":"07",
         "label":"Pueblo Nuevo, Ica",
         "search":"pueblo nuevo ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3615",
         "name":"Salas",
         "code":"08",
         "label":"Salas, Ica",
         "search":"salas ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3616",
         "name":"San Jose de los Molinos",
         "code":"09",
         "label":"San Jose de los Molinos, Ica",
         "search":"san jose de los molinos ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3617",
         "name":"San Juan Bautista",
         "code":"10",
         "label":"San Juan Bautista, Ica",
         "search":"san juan bautista ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3618",
         "name":"Santiago",
         "code":"11",
         "label":"Santiago, Ica",
         "search":"santiago ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3619",
         "name":"Subtanjalla",
         "code":"12",
         "label":"Subtanjalla, Ica",
         "search":"subtanjalla ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3620",
         "name":"Tate",
         "code":"13",
         "label":"Tate, Ica",
         "search":"tate ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      },
      {
         "id":"3621",
         "name":"Yauca del Rosario  1\/",
         "code":"14",
         "label":"Yauca del Rosario  1\/, Ica",
         "search":"yauca del rosario  1\/ ica",
         "children_count":"0",
         "level":"3",
         "parent_id":"3607"
      }
   ],
   "3634":[
      {
         "id":"3636",
         "name":"Changuillo",
         "code":"02",
         "label":"Changuillo, Nazca",
         "search":"changuillo nazca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3634"
      },
      {
         "id":"3637",
         "name":"El Ingenio",
         "code":"03",
         "label":"El Ingenio, Nazca",
         "search":"el ingenio nazca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3634"
      },
      {
         "id":"3638",
         "name":"Marcona",
         "code":"04",
         "label":"Marcona, Nazca",
         "search":"marcona nazca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3634"
      },
      {
         "id":"3635",
         "name":"Nazca",
         "code":"01",
         "label":"Nazca, Nazca",
         "search":"nazca nazca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3634"
      },
      {
         "id":"3639",
         "name":"Vista Alegre",
         "code":"05",
         "label":"Vista Alegre, Nazca",
         "search":"vista alegre nazca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3634"
      }
   ],
   "3640":[
      {
         "id":"3642",
         "name":"Llipata",
         "code":"02",
         "label":"Llipata, Palpa",
         "search":"llipata palpa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3640"
      },
      {
         "id":"3641",
         "name":"Palpa",
         "code":"01",
         "label":"Palpa, Palpa",
         "search":"palpa palpa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3640"
      },
      {
         "id":"3643",
         "name":"Rio Grande",
         "code":"03",
         "label":"Rio Grande, Palpa",
         "search":"rio grande palpa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3640"
      },
      {
         "id":"3644",
         "name":"Santa Cruz",
         "code":"04",
         "label":"Santa Cruz, Palpa",
         "search":"santa cruz palpa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3640"
      },
      {
         "id":"3645",
         "name":"Tibillo",
         "code":"05",
         "label":"Tibillo, Palpa",
         "search":"tibillo palpa",
         "children_count":"0",
         "level":"3",
         "parent_id":"3640"
      }
   ],
   "3646":[
      {
         "id":"3648",
         "name":"Huancano",
         "code":"02",
         "label":"Huancano, Pisco",
         "search":"huancano pisco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3646"
      },
      {
         "id":"3649",
         "name":"Humay",
         "code":"03",
         "label":"Humay, Pisco",
         "search":"humay pisco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3646"
      },
      {
         "id":"3650",
         "name":"Independencia",
         "code":"04",
         "label":"Independencia, Pisco",
         "search":"independencia pisco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3646"
      },
      {
         "id":"3651",
         "name":"Paracas",
         "code":"05",
         "label":"Paracas, Pisco",
         "search":"paracas pisco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3646"
      },
      {
         "id":"3647",
         "name":"Pisco",
         "code":"01",
         "label":"Pisco, Pisco",
         "search":"pisco pisco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3646"
      },
      {
         "id":"3652",
         "name":"San Andres",
         "code":"06",
         "label":"San Andres, Pisco",
         "search":"san andres pisco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3646"
      },
      {
         "id":"3653",
         "name":"San Clemente",
         "code":"07",
         "label":"San Clemente, Pisco",
         "search":"san clemente pisco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3646"
      },
      {
         "id":"3654",
         "name":"Tupac Amaru Inca",
         "code":"08",
         "label":"Tupac Amaru Inca, Pisco",
         "search":"tupac amaru inca pisco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3646"
      }
   ],
   "3701":[
      {
         "id":"3702",
         "name":"Chanchamayo",
         "code":"01",
         "label":"Chanchamayo, Chanchamayo",
         "search":"chanchamayo chanchamayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3701"
      },
      {
         "id":"3703",
         "name":"Perene",
         "code":"02",
         "label":"Perene, Chanchamayo",
         "search":"perene chanchamayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3701"
      },
      {
         "id":"3704",
         "name":"Pichanaqui",
         "code":"03",
         "label":"Pichanaqui, Chanchamayo",
         "search":"pichanaqui chanchamayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3701"
      },
      {
         "id":"3705",
         "name":"San Luis de Shuaro",
         "code":"04",
         "label":"San Luis de Shuaro, Chanchamayo",
         "search":"san luis de shuaro chanchamayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3701"
      },
      {
         "id":"3706",
         "name":"San Ramon",
         "code":"05",
         "label":"San Ramon, Chanchamayo",
         "search":"san ramon chanchamayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3701"
      },
      {
         "id":"3707",
         "name":"Vitoc",
         "code":"06",
         "label":"Vitoc, Chanchamayo",
         "search":"vitoc chanchamayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3701"
      }
   ],
   "3778":[
      {
         "id":"3780",
         "name":"Ahuac",
         "code":"02",
         "label":"Ahuac, Chupaca",
         "search":"ahuac chupaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3778"
      },
      {
         "id":"3781",
         "name":"Chongos Bajo",
         "code":"03",
         "label":"Chongos Bajo, Chupaca",
         "search":"chongos bajo chupaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3778"
      },
      {
         "id":"3779",
         "name":"Chupaca",
         "code":"01",
         "label":"Chupaca, Chupaca",
         "search":"chupaca chupaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3778"
      },
      {
         "id":"3782",
         "name":"Huachac",
         "code":"04",
         "label":"Huachac, Chupaca",
         "search":"huachac chupaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3778"
      },
      {
         "id":"3783",
         "name":"Huamancaca Chico",
         "code":"05",
         "label":"Huamancaca Chico, Chupaca",
         "search":"huamancaca chico chupaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3778"
      },
      {
         "id":"3784",
         "name":"San Juan de Iscos",
         "code":"06",
         "label":"San Juan de Iscos, Chupaca",
         "search":"san juan de iscos chupaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3778"
      },
      {
         "id":"3785",
         "name":"San Juan de Jarpa",
         "code":"07",
         "label":"San Juan de Jarpa, Chupaca",
         "search":"san juan de jarpa chupaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3778"
      },
      {
         "id":"3786",
         "name":"Tres de Diciembre",
         "code":"08",
         "label":"Tres de Diciembre, Chupaca",
         "search":"tres de diciembre chupaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3778"
      },
      {
         "id":"3787",
         "name":"Yanacancha",
         "code":"09",
         "label":"Yanacancha, Chupaca",
         "search":"yanacancha chupaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3778"
      }
   ],
   "3685":[
      {
         "id":"3687",
         "name":"Aco",
         "code":"02",
         "label":"Aco, Concepcion",
         "search":"aco concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3688",
         "name":"Andamarca",
         "code":"03",
         "label":"Andamarca, Concepcion",
         "search":"andamarca concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3689",
         "name":"Chambara",
         "code":"04",
         "label":"Chambara, Concepcion",
         "search":"chambara concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3690",
         "name":"Cochas",
         "code":"05",
         "label":"Cochas, Concepcion",
         "search":"cochas concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3691",
         "name":"Comas",
         "code":"06",
         "label":"Comas, Concepcion",
         "search":"comas concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3686",
         "name":"Concepcion",
         "code":"01",
         "label":"Concepcion, Concepcion",
         "search":"concepcion concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3692",
         "name":"Heroinas Toledo",
         "code":"07",
         "label":"Heroinas Toledo, Concepcion",
         "search":"heroinas toledo concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3693",
         "name":"Manzanares",
         "code":"08",
         "label":"Manzanares, Concepcion",
         "search":"manzanares concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3694",
         "name":"Mariscal Castilla",
         "code":"09",
         "label":"Mariscal Castilla, Concepcion",
         "search":"mariscal castilla concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3695",
         "name":"Matahuasi",
         "code":"10",
         "label":"Matahuasi, Concepcion",
         "search":"matahuasi concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3696",
         "name":"Mito",
         "code":"11",
         "label":"Mito, Concepcion",
         "search":"mito concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3697",
         "name":"Nueve de Julio",
         "code":"12",
         "label":"Nueve de Julio, Concepcion",
         "search":"nueve de julio concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3698",
         "name":"Orcotuna",
         "code":"13",
         "label":"Orcotuna, Concepcion",
         "search":"orcotuna concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3699",
         "name":"San Jose de Quero",
         "code":"14",
         "label":"San Jose de Quero, Concepcion",
         "search":"san jose de quero concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      },
      {
         "id":"3700",
         "name":"Santa Rosa de Ocopa",
         "code":"15",
         "label":"Santa Rosa de Ocopa, Concepcion",
         "search":"santa rosa de ocopa concepcion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3685"
      }
   ],
   "3656":[
      {
         "id":"3658",
         "name":"Carhuacallanga",
         "code":"04",
         "label":"Carhuacallanga, Huancayo",
         "search":"carhuacallanga huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3659",
         "name":"Chacapampa",
         "code":"05",
         "label":"Chacapampa, Huancayo",
         "search":"chacapampa huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3660",
         "name":"Chicche",
         "code":"06",
         "label":"Chicche, Huancayo",
         "search":"chicche huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3661",
         "name":"Chilca",
         "code":"07",
         "label":"Chilca, Huancayo",
         "search":"chilca huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3662",
         "name":"Chongos Alto",
         "code":"08",
         "label":"Chongos Alto, Huancayo",
         "search":"chongos alto huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3663",
         "name":"Chupuro",
         "code":"11",
         "label":"Chupuro, Huancayo",
         "search":"chupuro huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3664",
         "name":"Colca",
         "code":"12",
         "label":"Colca, Huancayo",
         "search":"colca huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3665",
         "name":"Cullhuas",
         "code":"13",
         "label":"Cullhuas, Huancayo",
         "search":"cullhuas huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3666",
         "name":"El Tambo",
         "code":"14",
         "label":"El Tambo, Huancayo",
         "search":"el tambo huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3667",
         "name":"Huacrapuquio",
         "code":"16",
         "label":"Huacrapuquio, Huancayo",
         "search":"huacrapuquio huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3668",
         "name":"Hualhuas",
         "code":"17",
         "label":"Hualhuas, Huancayo",
         "search":"hualhuas huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3669",
         "name":"Huancan",
         "code":"19",
         "label":"Huancan, Huancayo",
         "search":"huancan huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3657",
         "name":"Huancayo",
         "code":"01",
         "label":"Huancayo, Huancayo",
         "search":"huancayo huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3670",
         "name":"Huasicancha",
         "code":"20",
         "label":"Huasicancha, Huancayo",
         "search":"huasicancha huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3671",
         "name":"Huayucachi",
         "code":"21",
         "label":"Huayucachi, Huancayo",
         "search":"huayucachi huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3672",
         "name":"Ingenio",
         "code":"22",
         "label":"Ingenio, Huancayo",
         "search":"ingenio huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3673",
         "name":"Pariahuanca",
         "code":"24",
         "label":"Pariahuanca, Huancayo",
         "search":"pariahuanca huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3674",
         "name":"Pilcomayo",
         "code":"25",
         "label":"Pilcomayo, Huancayo",
         "search":"pilcomayo huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3675",
         "name":"Pucara",
         "code":"26",
         "label":"Pucara, Huancayo",
         "search":"pucara huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3676",
         "name":"Quichuay",
         "code":"27",
         "label":"Quichuay, Huancayo",
         "search":"quichuay huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3677",
         "name":"Quilcas",
         "code":"28",
         "label":"Quilcas, Huancayo",
         "search":"quilcas huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3678",
         "name":"San Agustin",
         "code":"29",
         "label":"San Agustin, Huancayo",
         "search":"san agustin huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3679",
         "name":"San Jeronimo de Tunan",
         "code":"30",
         "label":"San Jeronimo de Tunan, Huancayo",
         "search":"san jeronimo de tunan huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3683",
         "name":"Santo Domingo de Acobamba",
         "code":"35",
         "label":"Santo Domingo de Acobamba, Huancayo",
         "search":"santo domingo de acobamba huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3681",
         "name":"Sapallanga",
         "code":"33",
         "label":"Sapallanga, Huancayo",
         "search":"sapallanga huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3680",
         "name":"Saqo",
         "code":"32",
         "label":"Saqo, Huancayo",
         "search":"saqo huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3682",
         "name":"Sicaya",
         "code":"34",
         "label":"Sicaya, Huancayo",
         "search":"sicaya huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      },
      {
         "id":"3684",
         "name":"Viques",
         "code":"36",
         "label":"Viques, Huancayo",
         "search":"viques huancayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3656"
      }
   ],
   "3708":[
      {
         "id":"3710",
         "name":"Acolla",
         "code":"02",
         "label":"Acolla, Jauja",
         "search":"acolla jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3711",
         "name":"Apata",
         "code":"03",
         "label":"Apata, Jauja",
         "search":"apata jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3712",
         "name":"Ataura",
         "code":"04",
         "label":"Ataura, Jauja",
         "search":"ataura jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3713",
         "name":"Canchayllo",
         "code":"05",
         "label":"Canchayllo, Jauja",
         "search":"canchayllo jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3714",
         "name":"Curicaca",
         "code":"06",
         "label":"Curicaca, Jauja",
         "search":"curicaca jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3715",
         "name":"El Mantaro",
         "code":"07",
         "label":"El Mantaro, Jauja",
         "search":"el mantaro jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3716",
         "name":"Huamali",
         "code":"08",
         "label":"Huamali, Jauja",
         "search":"huamali jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3717",
         "name":"Huaripampa",
         "code":"09",
         "label":"Huaripampa, Jauja",
         "search":"huaripampa jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3718",
         "name":"Huertas",
         "code":"10",
         "label":"Huertas, Jauja",
         "search":"huertas jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3719",
         "name":"Janjaillo",
         "code":"11",
         "label":"Janjaillo, Jauja",
         "search":"janjaillo jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3709",
         "name":"Jauja",
         "code":"01",
         "label":"Jauja, Jauja",
         "search":"jauja jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3720",
         "name":"Julcan",
         "code":"12",
         "label":"Julcan, Jauja",
         "search":"julcan jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3721",
         "name":"Leonor Ordoqez",
         "code":"13",
         "label":"Leonor Ordoqez, Jauja",
         "search":"leonor ordoqez jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3722",
         "name":"Llocllapampa",
         "code":"14",
         "label":"Llocllapampa, Jauja",
         "search":"llocllapampa jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3723",
         "name":"Marco",
         "code":"15",
         "label":"Marco, Jauja",
         "search":"marco jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3724",
         "name":"Masma",
         "code":"16",
         "label":"Masma, Jauja",
         "search":"masma jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3725",
         "name":"Masma Chicche",
         "code":"17",
         "label":"Masma Chicche, Jauja",
         "search":"masma chicche jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3726",
         "name":"Molinos",
         "code":"18",
         "label":"Molinos, Jauja",
         "search":"molinos jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3727",
         "name":"Monobamba",
         "code":"19",
         "label":"Monobamba, Jauja",
         "search":"monobamba jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3728",
         "name":"Muqui",
         "code":"20",
         "label":"Muqui, Jauja",
         "search":"muqui jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3729",
         "name":"Muquiyauyo",
         "code":"21",
         "label":"Muquiyauyo, Jauja",
         "search":"muquiyauyo jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3730",
         "name":"Paca",
         "code":"22",
         "label":"Paca, Jauja",
         "search":"paca jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3731",
         "name":"Paccha",
         "code":"23",
         "label":"Paccha, Jauja",
         "search":"paccha jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3732",
         "name":"Pancan",
         "code":"24",
         "label":"Pancan, Jauja",
         "search":"pancan jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3733",
         "name":"Parco",
         "code":"25",
         "label":"Parco, Jauja",
         "search":"parco jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3734",
         "name":"Pomacancha",
         "code":"26",
         "label":"Pomacancha, Jauja",
         "search":"pomacancha jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3735",
         "name":"Ricran",
         "code":"27",
         "label":"Ricran, Jauja",
         "search":"ricran jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3736",
         "name":"San Lorenzo",
         "code":"28",
         "label":"San Lorenzo, Jauja",
         "search":"san lorenzo jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3737",
         "name":"San Pedro de Chunan",
         "code":"29",
         "label":"San Pedro de Chunan, Jauja",
         "search":"san pedro de chunan jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3738",
         "name":"Sausa",
         "code":"30",
         "label":"Sausa, Jauja",
         "search":"sausa jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3739",
         "name":"Sincos",
         "code":"31",
         "label":"Sincos, Jauja",
         "search":"sincos jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3740",
         "name":"Tunan Marca",
         "code":"32",
         "label":"Tunan Marca, Jauja",
         "search":"tunan marca jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3741",
         "name":"Yauli",
         "code":"33",
         "label":"Yauli, Jauja",
         "search":"yauli jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      },
      {
         "id":"3742",
         "name":"Yauyos",
         "code":"34",
         "label":"Yauyos, Jauja",
         "search":"yauyos jauja",
         "children_count":"0",
         "level":"3",
         "parent_id":"3708"
      }
   ],
   "3743":[
      {
         "id":"3745",
         "name":"Carhuamayo",
         "code":"02",
         "label":"Carhuamayo, Junin",
         "search":"carhuamayo junin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3743"
      },
      {
         "id":"3744",
         "name":"Junin",
         "code":"01",
         "label":"Junin, Junin",
         "search":"junin junin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3743"
      },
      {
         "id":"3746",
         "name":"Ondores",
         "code":"03",
         "label":"Ondores, Junin",
         "search":"ondores junin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3743"
      },
      {
         "id":"3747",
         "name":"Ulcumayo",
         "code":"04",
         "label":"Ulcumayo, Junin",
         "search":"ulcumayo junin",
         "children_count":"0",
         "level":"3",
         "parent_id":"3743"
      }
   ],
   "3748":[
      {
         "id":"3750",
         "name":"Coviriali",
         "code":"02",
         "label":"Coviriali, Satipo",
         "search":"coviriali satipo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3748"
      },
      {
         "id":"3751",
         "name":"Llaylla",
         "code":"03",
         "label":"Llaylla, Satipo",
         "search":"llaylla satipo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3748"
      },
      {
         "id":"3752",
         "name":"Mazamari",
         "code":"04",
         "label":"Mazamari, Satipo",
         "search":"mazamari satipo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3748"
      },
      {
         "id":"3753",
         "name":"Pampa Hermosa",
         "code":"05",
         "label":"Pampa Hermosa, Satipo",
         "search":"pampa hermosa satipo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3748"
      },
      {
         "id":"3754",
         "name":"Pangoa",
         "code":"06",
         "label":"Pangoa, Satipo",
         "search":"pangoa satipo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3748"
      },
      {
         "id":"3755",
         "name":"Rio Negro",
         "code":"07",
         "label":"Rio Negro, Satipo",
         "search":"rio negro satipo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3748"
      },
      {
         "id":"3756",
         "name":"Rio Tambo",
         "code":"08",
         "label":"Rio Tambo, Satipo",
         "search":"rio tambo satipo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3748"
      },
      {
         "id":"3749",
         "name":"Satipo",
         "code":"01",
         "label":"Satipo, Satipo",
         "search":"satipo satipo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3748"
      }
   ],
   "3757":[
      {
         "id":"3759",
         "name":"Acobamba",
         "code":"02",
         "label":"Acobamba, Tarma",
         "search":"acobamba tarma",
         "children_count":"0",
         "level":"3",
         "parent_id":"3757"
      },
      {
         "id":"3760",
         "name":"Huaricolca",
         "code":"03",
         "label":"Huaricolca, Tarma",
         "search":"huaricolca tarma",
         "children_count":"0",
         "level":"3",
         "parent_id":"3757"
      },
      {
         "id":"3761",
         "name":"Huasahuasi",
         "code":"04",
         "label":"Huasahuasi, Tarma",
         "search":"huasahuasi tarma",
         "children_count":"0",
         "level":"3",
         "parent_id":"3757"
      },
      {
         "id":"3762",
         "name":"La Union",
         "code":"05",
         "label":"La Union, Tarma",
         "search":"la union tarma",
         "children_count":"0",
         "level":"3",
         "parent_id":"3757"
      },
      {
         "id":"3763",
         "name":"Palca",
         "code":"06",
         "label":"Palca, Tarma",
         "search":"palca tarma",
         "children_count":"0",
         "level":"3",
         "parent_id":"3757"
      },
      {
         "id":"3764",
         "name":"Palcamayo",
         "code":"07",
         "label":"Palcamayo, Tarma",
         "search":"palcamayo tarma",
         "children_count":"0",
         "level":"3",
         "parent_id":"3757"
      },
      {
         "id":"3765",
         "name":"San Pedro de Cajas",
         "code":"08",
         "label":"San Pedro de Cajas, Tarma",
         "search":"san pedro de cajas tarma",
         "children_count":"0",
         "level":"3",
         "parent_id":"3757"
      },
      {
         "id":"3766",
         "name":"Tapo",
         "code":"09",
         "label":"Tapo, Tarma",
         "search":"tapo tarma",
         "children_count":"0",
         "level":"3",
         "parent_id":"3757"
      },
      {
         "id":"3758",
         "name":"Tarma",
         "code":"01",
         "label":"Tarma, Tarma",
         "search":"tarma tarma",
         "children_count":"0",
         "level":"3",
         "parent_id":"3757"
      }
   ],
   "3767":[
      {
         "id":"3769",
         "name":"Chacapalpa",
         "code":"02",
         "label":"Chacapalpa, Yauli",
         "search":"chacapalpa yauli",
         "children_count":"0",
         "level":"3",
         "parent_id":"3767"
      },
      {
         "id":"3770",
         "name":"Huay-Huay",
         "code":"03",
         "label":"Huay-Huay, Yauli",
         "search":"huay-huay yauli",
         "children_count":"0",
         "level":"3",
         "parent_id":"3767"
      },
      {
         "id":"3768",
         "name":"La Oroya",
         "code":"01",
         "label":"La Oroya, Yauli",
         "search":"la oroya yauli",
         "children_count":"0",
         "level":"3",
         "parent_id":"3767"
      },
      {
         "id":"3771",
         "name":"Marcapomacocha",
         "code":"04",
         "label":"Marcapomacocha, Yauli",
         "search":"marcapomacocha yauli",
         "children_count":"0",
         "level":"3",
         "parent_id":"3767"
      },
      {
         "id":"3772",
         "name":"Morococha",
         "code":"05",
         "label":"Morococha, Yauli",
         "search":"morococha yauli",
         "children_count":"0",
         "level":"3",
         "parent_id":"3767"
      },
      {
         "id":"3773",
         "name":"Paccha",
         "code":"06",
         "label":"Paccha, Yauli",
         "search":"paccha yauli",
         "children_count":"0",
         "level":"3",
         "parent_id":"3767"
      },
      {
         "id":"3774",
         "name":"Santa Barbara de Carhuacayan",
         "code":"07",
         "label":"Santa Barbara de Carhuacayan, Yauli",
         "search":"santa barbara de carhuacayan yauli",
         "children_count":"0",
         "level":"3",
         "parent_id":"3767"
      },
      {
         "id":"3775",
         "name":"Santa Rosa de Sacco",
         "code":"08",
         "label":"Santa Rosa de Sacco, Yauli",
         "search":"santa rosa de sacco yauli",
         "children_count":"0",
         "level":"3",
         "parent_id":"3767"
      },
      {
         "id":"3776",
         "name":"Suitucancha",
         "code":"09",
         "label":"Suitucancha, Yauli",
         "search":"suitucancha yauli",
         "children_count":"0",
         "level":"3",
         "parent_id":"3767"
      },
      {
         "id":"3777",
         "name":"Yauli",
         "code":"10",
         "label":"Yauli, Yauli",
         "search":"yauli yauli",
         "children_count":"0",
         "level":"3",
         "parent_id":"3767"
      }
   ],
   "3801":[
      {
         "id":"3802",
         "name":"Ascope",
         "code":"01",
         "label":"Ascope, Ascope",
         "search":"ascope ascope",
         "children_count":"0",
         "level":"3",
         "parent_id":"3801"
      },
      {
         "id":"3809",
         "name":"Casa Grande",
         "code":"08",
         "label":"Casa Grande, Ascope",
         "search":"casa grande ascope",
         "children_count":"0",
         "level":"3",
         "parent_id":"3801"
      },
      {
         "id":"3803",
         "name":"Chicama",
         "code":"02",
         "label":"Chicama, Ascope",
         "search":"chicama ascope",
         "children_count":"0",
         "level":"3",
         "parent_id":"3801"
      },
      {
         "id":"3804",
         "name":"Chocope",
         "code":"03",
         "label":"Chocope, Ascope",
         "search":"chocope ascope",
         "children_count":"0",
         "level":"3",
         "parent_id":"3801"
      },
      {
         "id":"3805",
         "name":"Magdalena de Cao",
         "code":"04",
         "label":"Magdalena de Cao, Ascope",
         "search":"magdalena de cao ascope",
         "children_count":"0",
         "level":"3",
         "parent_id":"3801"
      },
      {
         "id":"3806",
         "name":"Paijan",
         "code":"05",
         "label":"Paijan, Ascope",
         "search":"paijan ascope",
         "children_count":"0",
         "level":"3",
         "parent_id":"3801"
      },
      {
         "id":"3807",
         "name":"Razuri",
         "code":"06",
         "label":"Razuri, Ascope",
         "search":"razuri ascope",
         "children_count":"0",
         "level":"3",
         "parent_id":"3801"
      },
      {
         "id":"3808",
         "name":"Santiago de Cao",
         "code":"07",
         "label":"Santiago de Cao, Ascope",
         "search":"santiago de cao ascope",
         "children_count":"0",
         "level":"3",
         "parent_id":"3801"
      }
   ],
   "3810":[
      {
         "id":"3812",
         "name":"Bambamarca",
         "code":"02",
         "label":"Bambamarca, Bolivar",
         "search":"bambamarca bolivar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3810"
      },
      {
         "id":"3811",
         "name":"Bolivar",
         "code":"01",
         "label":"Bolivar, Bolivar",
         "search":"bolivar bolivar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3810"
      },
      {
         "id":"3813",
         "name":"Condormarca",
         "code":"03",
         "label":"Condormarca, Bolivar",
         "search":"condormarca bolivar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3810"
      },
      {
         "id":"3814",
         "name":"Longotea",
         "code":"04",
         "label":"Longotea, Bolivar",
         "search":"longotea bolivar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3810"
      },
      {
         "id":"3815",
         "name":"Uchumarca",
         "code":"05",
         "label":"Uchumarca, Bolivar",
         "search":"uchumarca bolivar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3810"
      },
      {
         "id":"3816",
         "name":"Ucuncha",
         "code":"06",
         "label":"Ucuncha, Bolivar",
         "search":"ucuncha bolivar",
         "children_count":"0",
         "level":"3",
         "parent_id":"3810"
      }
   ],
   "3817":[
      {
         "id":"3818",
         "name":"Chepen",
         "code":"01",
         "label":"Chepen, Chepen",
         "search":"chepen chepen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3817"
      },
      {
         "id":"3819",
         "name":"Pacanga",
         "code":"02",
         "label":"Pacanga, Chepen",
         "search":"pacanga chepen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3817"
      },
      {
         "id":"3820",
         "name":"Pueblo Nuevo",
         "code":"03",
         "label":"Pueblo Nuevo, Chepen",
         "search":"pueblo nuevo chepen",
         "children_count":"0",
         "level":"3",
         "parent_id":"3817"
      }
   ],
   "3875":[
      {
         "id":"3876",
         "name":"Cascas",
         "code":"01",
         "label":"Cascas, Gran Chimu",
         "search":"cascas gran chimu",
         "children_count":"0",
         "level":"3",
         "parent_id":"3875"
      },
      {
         "id":"3877",
         "name":"Lucma",
         "code":"02",
         "label":"Lucma, Gran Chimu",
         "search":"lucma gran chimu",
         "children_count":"0",
         "level":"3",
         "parent_id":"3875"
      },
      {
         "id":"3878",
         "name":"Marmot",
         "code":"03",
         "label":"Marmot, Gran Chimu",
         "search":"marmot gran chimu",
         "children_count":"0",
         "level":"3",
         "parent_id":"3875"
      },
      {
         "id":"3879",
         "name":"Sayapullo",
         "code":"04",
         "label":"Sayapullo, Gran Chimu",
         "search":"sayapullo gran chimu",
         "children_count":"0",
         "level":"3",
         "parent_id":"3875"
      }
   ],
   "3821":[
      {
         "id":"3823",
         "name":"Calamarca",
         "code":"02",
         "label":"Calamarca, Julcan",
         "search":"calamarca julcan",
         "children_count":"0",
         "level":"3",
         "parent_id":"3821"
      },
      {
         "id":"3824",
         "name":"Carabamba",
         "code":"03",
         "label":"Carabamba, Julcan",
         "search":"carabamba julcan",
         "children_count":"0",
         "level":"3",
         "parent_id":"3821"
      },
      {
         "id":"3825",
         "name":"Huaso",
         "code":"04",
         "label":"Huaso, Julcan",
         "search":"huaso julcan",
         "children_count":"0",
         "level":"3",
         "parent_id":"3821"
      },
      {
         "id":"3822",
         "name":"Julcan",
         "code":"01",
         "label":"Julcan, Julcan",
         "search":"julcan julcan",
         "children_count":"0",
         "level":"3",
         "parent_id":"3821"
      }
   ],
   "3826":[
      {
         "id":"3828",
         "name":"Agallpampa",
         "code":"02",
         "label":"Agallpampa, Otuzco",
         "search":"agallpampa otuzco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3826"
      },
      {
         "id":"3829",
         "name":"Charat",
         "code":"04",
         "label":"Charat, Otuzco",
         "search":"charat otuzco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3826"
      },
      {
         "id":"3830",
         "name":"Huaranchal",
         "code":"05",
         "label":"Huaranchal, Otuzco",
         "search":"huaranchal otuzco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3826"
      },
      {
         "id":"3831",
         "name":"La Cuesta",
         "code":"06",
         "label":"La Cuesta, Otuzco",
         "search":"la cuesta otuzco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3826"
      },
      {
         "id":"3832",
         "name":"Mache",
         "code":"08",
         "label":"Mache, Otuzco",
         "search":"mache otuzco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3826"
      },
      {
         "id":"3827",
         "name":"Otuzco",
         "code":"01",
         "label":"Otuzco, Otuzco",
         "search":"otuzco otuzco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3826"
      },
      {
         "id":"3833",
         "name":"Paranday",
         "code":"10",
         "label":"Paranday, Otuzco",
         "search":"paranday otuzco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3826"
      },
      {
         "id":"3834",
         "name":"Salpo",
         "code":"11",
         "label":"Salpo, Otuzco",
         "search":"salpo otuzco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3826"
      },
      {
         "id":"3835",
         "name":"Sinsicap",
         "code":"13",
         "label":"Sinsicap, Otuzco",
         "search":"sinsicap otuzco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3826"
      },
      {
         "id":"3836",
         "name":"Usquil",
         "code":"14",
         "label":"Usquil, Otuzco",
         "search":"usquil otuzco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3826"
      }
   ],
   "3837":[
      {
         "id":"3839",
         "name":"Guadalupe",
         "code":"02",
         "label":"Guadalupe, Pacasmayo",
         "search":"guadalupe pacasmayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3837"
      },
      {
         "id":"3840",
         "name":"Jequetepeque",
         "code":"03",
         "label":"Jequetepeque, Pacasmayo",
         "search":"jequetepeque pacasmayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3837"
      },
      {
         "id":"3841",
         "name":"Pacasmayo",
         "code":"04",
         "label":"Pacasmayo, Pacasmayo",
         "search":"pacasmayo pacasmayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3837"
      },
      {
         "id":"3842",
         "name":"San Jose",
         "code":"05",
         "label":"San Jose, Pacasmayo",
         "search":"san jose pacasmayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3837"
      },
      {
         "id":"3838",
         "name":"San Pedro de Lloc",
         "code":"01",
         "label":"San Pedro de Lloc, Pacasmayo",
         "search":"san pedro de lloc pacasmayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3837"
      }
   ],
   "3843":[
      {
         "id":"3845",
         "name":"Buldibuyo",
         "code":"02",
         "label":"Buldibuyo, Pataz",
         "search":"buldibuyo pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3846",
         "name":"Chillia",
         "code":"03",
         "label":"Chillia, Pataz",
         "search":"chillia pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3847",
         "name":"Huancaspata",
         "code":"04",
         "label":"Huancaspata, Pataz",
         "search":"huancaspata pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3848",
         "name":"Huaylillas",
         "code":"05",
         "label":"Huaylillas, Pataz",
         "search":"huaylillas pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3849",
         "name":"Huayo",
         "code":"06",
         "label":"Huayo, Pataz",
         "search":"huayo pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3850",
         "name":"Ongon",
         "code":"07",
         "label":"Ongon, Pataz",
         "search":"ongon pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3851",
         "name":"Parcoy",
         "code":"08",
         "label":"Parcoy, Pataz",
         "search":"parcoy pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3852",
         "name":"Pataz",
         "code":"09",
         "label":"Pataz, Pataz",
         "search":"pataz pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3853",
         "name":"Pias",
         "code":"10",
         "label":"Pias, Pataz",
         "search":"pias pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3854",
         "name":"Santiago de Challas",
         "code":"11",
         "label":"Santiago de Challas, Pataz",
         "search":"santiago de challas pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3855",
         "name":"Taurija",
         "code":"12",
         "label":"Taurija, Pataz",
         "search":"taurija pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3844",
         "name":"Tayabamba",
         "code":"01",
         "label":"Tayabamba, Pataz",
         "search":"tayabamba pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      },
      {
         "id":"3856",
         "name":"Urpay",
         "code":"13",
         "label":"Urpay, Pataz",
         "search":"urpay pataz",
         "children_count":"0",
         "level":"3",
         "parent_id":"3843"
      }
   ],
   "3857":[
      {
         "id":"3859",
         "name":"Chugay",
         "code":"02",
         "label":"Chugay, Sanchez Carrion",
         "search":"chugay sanchez carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3857"
      },
      {
         "id":"3860",
         "name":"Cochorco",
         "code":"03",
         "label":"Cochorco, Sanchez Carrion",
         "search":"cochorco sanchez carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3857"
      },
      {
         "id":"3861",
         "name":"Curgos",
         "code":"04",
         "label":"Curgos, Sanchez Carrion",
         "search":"curgos sanchez carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3857"
      },
      {
         "id":"3858",
         "name":"Huamachuco",
         "code":"01",
         "label":"Huamachuco, Sanchez Carrion",
         "search":"huamachuco sanchez carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3857"
      },
      {
         "id":"3862",
         "name":"Marcabal",
         "code":"05",
         "label":"Marcabal, Sanchez Carrion",
         "search":"marcabal sanchez carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3857"
      },
      {
         "id":"3863",
         "name":"Sanagoran",
         "code":"06",
         "label":"Sanagoran, Sanchez Carrion",
         "search":"sanagoran sanchez carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3857"
      },
      {
         "id":"3864",
         "name":"Sarin",
         "code":"07",
         "label":"Sarin, Sanchez Carrion",
         "search":"sarin sanchez carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3857"
      },
      {
         "id":"3865",
         "name":"Sartimbamba",
         "code":"08",
         "label":"Sartimbamba, Sanchez Carrion",
         "search":"sartimbamba sanchez carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"3857"
      }
   ],
   "3866":[
      {
         "id":"3868",
         "name":"Angasmarca",
         "code":"02",
         "label":"Angasmarca, Santiago de Chuco",
         "search":"angasmarca santiago de chuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3866"
      },
      {
         "id":"3869",
         "name":"Cachicadan",
         "code":"03",
         "label":"Cachicadan, Santiago de Chuco",
         "search":"cachicadan santiago de chuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3866"
      },
      {
         "id":"3870",
         "name":"Mollebamba",
         "code":"04",
         "label":"Mollebamba, Santiago de Chuco",
         "search":"mollebamba santiago de chuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3866"
      },
      {
         "id":"3871",
         "name":"Mollepata",
         "code":"05",
         "label":"Mollepata, Santiago de Chuco",
         "search":"mollepata santiago de chuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3866"
      },
      {
         "id":"3872",
         "name":"Quiruvilca",
         "code":"06",
         "label":"Quiruvilca, Santiago de Chuco",
         "search":"quiruvilca santiago de chuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3866"
      },
      {
         "id":"3873",
         "name":"Santa Cruz de Chuca",
         "code":"07",
         "label":"Santa Cruz de Chuca, Santiago de Chuco",
         "search":"santa cruz de chuca santiago de chuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3866"
      },
      {
         "id":"3867",
         "name":"Santiago de Chuco",
         "code":"01",
         "label":"Santiago de Chuco, Santiago de Chuco",
         "search":"santiago de chuco santiago de chuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3866"
      },
      {
         "id":"3874",
         "name":"Sitabamba",
         "code":"08",
         "label":"Sitabamba, Santiago de Chuco",
         "search":"sitabamba santiago de chuco",
         "children_count":"0",
         "level":"3",
         "parent_id":"3866"
      }
   ],
   "3789":[
      {
         "id":"3791",
         "name":"El Porvenir",
         "code":"02",
         "label":"El Porvenir, Trujillo",
         "search":"el porvenir trujillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3789"
      },
      {
         "id":"3792",
         "name":"Florencia de Mora",
         "code":"03",
         "label":"Florencia de Mora, Trujillo",
         "search":"florencia de mora trujillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3789"
      },
      {
         "id":"3793",
         "name":"Huanchaco",
         "code":"04",
         "label":"Huanchaco, Trujillo",
         "search":"huanchaco trujillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3789"
      },
      {
         "id":"3794",
         "name":"La Esperanza",
         "code":"05",
         "label":"La Esperanza, Trujillo",
         "search":"la esperanza trujillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3789"
      },
      {
         "id":"3795",
         "name":"Laredo",
         "code":"06",
         "label":"Laredo, Trujillo",
         "search":"laredo trujillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3789"
      },
      {
         "id":"3796",
         "name":"Moche",
         "code":"07",
         "label":"Moche, Trujillo",
         "search":"moche trujillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3789"
      },
      {
         "id":"3797",
         "name":"Poroto",
         "code":"08",
         "label":"Poroto, Trujillo",
         "search":"poroto trujillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3789"
      },
      {
         "id":"3798",
         "name":"Salaverry",
         "code":"09",
         "label":"Salaverry, Trujillo",
         "search":"salaverry trujillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3789"
      },
      {
         "id":"3799",
         "name":"Simbal",
         "code":"10",
         "label":"Simbal, Trujillo",
         "search":"simbal trujillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3789"
      },
      {
         "id":"3790",
         "name":"Trujillo",
         "code":"01",
         "label":"Trujillo, Trujillo",
         "search":"trujillo trujillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3789"
      },
      {
         "id":"3800",
         "name":"Victor Larco Herrera",
         "code":"11",
         "label":"Victor Larco Herrera, Trujillo",
         "search":"victor larco herrera trujillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3789"
      }
   ],
   "3880":[
      {
         "id":"3882",
         "name":"Chao",
         "code":"02",
         "label":"Chao, Viru",
         "search":"chao viru",
         "children_count":"0",
         "level":"3",
         "parent_id":"3880"
      },
      {
         "id":"3883",
         "name":"Guadalupito",
         "code":"03",
         "label":"Guadalupito, Viru",
         "search":"guadalupito viru",
         "children_count":"0",
         "level":"3",
         "parent_id":"3880"
      },
      {
         "id":"3881",
         "name":"Viru",
         "code":"01",
         "label":"Viru, Viru",
         "search":"viru viru",
         "children_count":"0",
         "level":"3",
         "parent_id":"3880"
      }
   ],
   "3885":[
      {
         "id":"3901",
         "name":"Cayalt\u00ed",
         "code":"16",
         "label":"Cayalt\u00ed, Chiclayo",
         "search":"cayalt\u00ed chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3886",
         "name":"Chiclayo",
         "code":"01",
         "label":"Chiclayo, Chiclayo",
         "search":"chiclayo chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3887",
         "name":"Chongoyape",
         "code":"02",
         "label":"Chongoyape, Chiclayo",
         "search":"chongoyape chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3888",
         "name":"Eten",
         "code":"03",
         "label":"Eten, Chiclayo",
         "search":"eten chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3889",
         "name":"Eten Puerto",
         "code":"04",
         "label":"Eten Puerto, Chiclayo",
         "search":"eten puerto chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3890",
         "name":"Jose Leonardo Ortiz",
         "code":"05",
         "label":"Jose Leonardo Ortiz, Chiclayo",
         "search":"jose leonardo ortiz chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3891",
         "name":"La Victoria",
         "code":"06",
         "label":"La Victoria, Chiclayo",
         "search":"la victoria chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3892",
         "name":"Lagunas",
         "code":"07",
         "label":"Lagunas, Chiclayo",
         "search":"lagunas chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3893",
         "name":"Monsefu",
         "code":"08",
         "label":"Monsefu, Chiclayo",
         "search":"monsefu chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3894",
         "name":"Nueva Arica",
         "code":"09",
         "label":"Nueva Arica, Chiclayo",
         "search":"nueva arica chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3895",
         "name":"Oyotun",
         "code":"10",
         "label":"Oyotun, Chiclayo",
         "search":"oyotun chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3902",
         "name":"Patapo",
         "code":"17",
         "label":"Patapo, Chiclayo",
         "search":"patapo chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3896",
         "name":"Picsi",
         "code":"11",
         "label":"Picsi, Chiclayo",
         "search":"picsi chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3897",
         "name":"Pimentel",
         "code":"12",
         "label":"Pimentel, Chiclayo",
         "search":"pimentel chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3903",
         "name":"Pomalca",
         "code":"18",
         "label":"Pomalca, Chiclayo",
         "search":"pomalca chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3904",
         "name":"Pucal\u00e1",
         "code":"19",
         "label":"Pucal\u00e1, Chiclayo",
         "search":"pucal\u00e1 chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3898",
         "name":"Reque",
         "code":"13",
         "label":"Reque, Chiclayo",
         "search":"reque chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3899",
         "name":"Santa Rosa",
         "code":"14",
         "label":"Santa Rosa, Chiclayo",
         "search":"santa rosa chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3900",
         "name":"Saqa",
         "code":"15",
         "label":"Saqa, Chiclayo",
         "search":"saqa chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      },
      {
         "id":"3905",
         "name":"Tum\u00e1n",
         "code":"20",
         "label":"Tum\u00e1n, Chiclayo",
         "search":"tum\u00e1n chiclayo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3885"
      }
   ],
   "3906":[
      {
         "id":"3908",
         "name":"Caqaris",
         "code":"02",
         "label":"Caqaris, Ferreqafe",
         "search":"caqaris ferreqafe",
         "children_count":"0",
         "level":"3",
         "parent_id":"3906"
      },
      {
         "id":"3907",
         "name":"Ferreqafe",
         "code":"01",
         "label":"Ferreqafe, Ferreqafe",
         "search":"ferreqafe ferreqafe",
         "children_count":"0",
         "level":"3",
         "parent_id":"3906"
      },
      {
         "id":"3909",
         "name":"Incahuasi",
         "code":"03",
         "label":"Incahuasi, Ferreqafe",
         "search":"incahuasi ferreqafe",
         "children_count":"0",
         "level":"3",
         "parent_id":"3906"
      },
      {
         "id":"3910",
         "name":"Manuel Antonio Mesones Muro",
         "code":"04",
         "label":"Manuel Antonio Mesones Muro, Ferreqafe",
         "search":"manuel antonio mesones muro ferreqafe",
         "children_count":"0",
         "level":"3",
         "parent_id":"3906"
      },
      {
         "id":"3911",
         "name":"Pitipo",
         "code":"05",
         "label":"Pitipo, Ferreqafe",
         "search":"pitipo ferreqafe",
         "children_count":"0",
         "level":"3",
         "parent_id":"3906"
      },
      {
         "id":"3912",
         "name":"Pueblo Nuevo",
         "code":"06",
         "label":"Pueblo Nuevo, Ferreqafe",
         "search":"pueblo nuevo ferreqafe",
         "children_count":"0",
         "level":"3",
         "parent_id":"3906"
      }
   ],
   "3913":[
      {
         "id":"3915",
         "name":"Chochope",
         "code":"02",
         "label":"Chochope, Lambayeque",
         "search":"chochope lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      },
      {
         "id":"3916",
         "name":"Illimo",
         "code":"03",
         "label":"Illimo, Lambayeque",
         "search":"illimo lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      },
      {
         "id":"3917",
         "name":"Jayanca",
         "code":"04",
         "label":"Jayanca, Lambayeque",
         "search":"jayanca lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      },
      {
         "id":"3914",
         "name":"Lambayeque",
         "code":"01",
         "label":"Lambayeque, Lambayeque",
         "search":"lambayeque lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      },
      {
         "id":"3918",
         "name":"Mochumi",
         "code":"05",
         "label":"Mochumi, Lambayeque",
         "search":"mochumi lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      },
      {
         "id":"3919",
         "name":"Morrope",
         "code":"06",
         "label":"Morrope, Lambayeque",
         "search":"morrope lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      },
      {
         "id":"3920",
         "name":"Motupe",
         "code":"07",
         "label":"Motupe, Lambayeque",
         "search":"motupe lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      },
      {
         "id":"3921",
         "name":"Olmos",
         "code":"08",
         "label":"Olmos, Lambayeque",
         "search":"olmos lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      },
      {
         "id":"3922",
         "name":"Pacora",
         "code":"09",
         "label":"Pacora, Lambayeque",
         "search":"pacora lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      },
      {
         "id":"3923",
         "name":"Salas",
         "code":"10",
         "label":"Salas, Lambayeque",
         "search":"salas lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      },
      {
         "id":"3924",
         "name":"San Jose",
         "code":"11",
         "label":"San Jose, Lambayeque",
         "search":"san jose lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      },
      {
         "id":"3925",
         "name":"Tucume",
         "code":"12",
         "label":"Tucume, Lambayeque",
         "search":"tucume lambayeque",
         "children_count":"0",
         "level":"3",
         "parent_id":"3913"
      }
   ],
   "3971":[
      {
         "id":"3972",
         "name":"Barranca",
         "code":"01",
         "label":"Barranca, Barranca",
         "search":"barranca barranca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3971"
      },
      {
         "id":"3973",
         "name":"Paramonga",
         "code":"02",
         "label":"Paramonga, Barranca",
         "search":"paramonga barranca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3971"
      },
      {
         "id":"3974",
         "name":"Pativilca",
         "code":"03",
         "label":"Pativilca, Barranca",
         "search":"pativilca barranca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3971"
      },
      {
         "id":"3975",
         "name":"Supe",
         "code":"04",
         "label":"Supe, Barranca",
         "search":"supe barranca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3971"
      },
      {
         "id":"3976",
         "name":"Supe Puerto",
         "code":"05",
         "label":"Supe Puerto, Barranca",
         "search":"supe puerto barranca",
         "children_count":"0",
         "level":"3",
         "parent_id":"3971"
      }
   ],
   "3977":[
      {
         "id":"3978",
         "name":"Cajatambo",
         "code":"01",
         "label":"Cajatambo, Cajatambo",
         "search":"cajatambo cajatambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3977"
      },
      {
         "id":"3979",
         "name":"Copa",
         "code":"02",
         "label":"Copa, Cajatambo",
         "search":"copa cajatambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3977"
      },
      {
         "id":"3980",
         "name":"Gorgor",
         "code":"03",
         "label":"Gorgor, Cajatambo",
         "search":"gorgor cajatambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3977"
      },
      {
         "id":"3981",
         "name":"Huancapon",
         "code":"04",
         "label":"Huancapon, Cajatambo",
         "search":"huancapon cajatambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3977"
      },
      {
         "id":"3982",
         "name":"Manas",
         "code":"05",
         "label":"Manas, Cajatambo",
         "search":"manas cajatambo",
         "children_count":"0",
         "level":"3",
         "parent_id":"3977"
      }
   ],
   "3285":[
      {
         "id":"3287",
         "name":"Bellavista",
         "code":"02",
         "label":"Bellavista, Callao",
         "search":"bellavista callao",
         "children_count":"0",
         "level":"3",
         "parent_id":"3285"
      },
      {
         "id":"3286",
         "name":"Callao",
         "code":"01",
         "label":"Callao, Callao",
         "search":"callao callao",
         "children_count":"0",
         "level":"3",
         "parent_id":"3285"
      },
      {
         "id":"3288",
         "name":"Carmen de la Legua Reynoso",
         "code":"03",
         "label":"Carmen de la Legua Reynoso, Callao",
         "search":"carmen de la legua reynoso callao",
         "children_count":"0",
         "level":"3",
         "parent_id":"3285"
      },
      {
         "id":"3289",
         "name":"La Perla",
         "code":"04",
         "label":"La Perla, Callao",
         "search":"la perla callao",
         "children_count":"0",
         "level":"3",
         "parent_id":"3285"
      },
      {
         "id":"3290",
         "name":"La Punta",
         "code":"05",
         "label":"La Punta, Callao",
         "search":"la punta callao",
         "children_count":"0",
         "level":"3",
         "parent_id":"3285"
      },
      {
         "id":"3291",
         "name":"Ventanilla",
         "code":"06",
         "label":"Ventanilla, Callao",
         "search":"ventanilla callao",
         "children_count":"0",
         "level":"3",
         "parent_id":"3285"
      }
   ],
   "3991":[
      {
         "id":"3993",
         "name":"Asia",
         "code":"02",
         "label":"Asia, Caqete",
         "search":"asia caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"3994",
         "name":"Calango",
         "code":"03",
         "label":"Calango, Caqete",
         "search":"calango caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"3995",
         "name":"Cerro Azul",
         "code":"04",
         "label":"Cerro Azul, Caqete",
         "search":"cerro azul caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"3996",
         "name":"Chilca",
         "code":"05",
         "label":"Chilca, Caqete",
         "search":"chilca caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"3997",
         "name":"Coayllo",
         "code":"06",
         "label":"Coayllo, Caqete",
         "search":"coayllo caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"3998",
         "name":"Imperial",
         "code":"07",
         "label":"Imperial, Caqete",
         "search":"imperial caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"3999",
         "name":"Lunahuana",
         "code":"08",
         "label":"Lunahuana, Caqete",
         "search":"lunahuana caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"4000",
         "name":"Mala",
         "code":"09",
         "label":"Mala, Caqete",
         "search":"mala caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"4001",
         "name":"Nuevo Imperial",
         "code":"10",
         "label":"Nuevo Imperial, Caqete",
         "search":"nuevo imperial caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"4002",
         "name":"Pacaran",
         "code":"11",
         "label":"Pacaran, Caqete",
         "search":"pacaran caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"4003",
         "name":"Quilmana",
         "code":"12",
         "label":"Quilmana, Caqete",
         "search":"quilmana caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"4004",
         "name":"San Antonio",
         "code":"13",
         "label":"San Antonio, Caqete",
         "search":"san antonio caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"4005",
         "name":"San Luis",
         "code":"14",
         "label":"San Luis, Caqete",
         "search":"san luis caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"3992",
         "name":"San Vicente de Ca\u00f1ete",
         "code":"01",
         "label":"San Vicente de Ca\u00f1ete, Ca\u00f1ete",
         "search":"san vicente de ca\u00f1ete ca\u00f1ete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"4006",
         "name":"Santa Cruz de Flores",
         "code":"15",
         "label":"Santa Cruz de Flores, Caqete",
         "search":"santa cruz de flores caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      },
      {
         "id":"4007",
         "name":"Zuqiga",
         "code":"16",
         "label":"Zuqiga, Caqete",
         "search":"zuqiga caqete",
         "children_count":"0",
         "level":"3",
         "parent_id":"3991"
      }
   ],
   "3983":[
      {
         "id":"3985",
         "name":"Arahuay",
         "code":"02",
         "label":"Arahuay, Canta",
         "search":"arahuay canta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3983"
      },
      {
         "id":"3984",
         "name":"Canta",
         "code":"01",
         "label":"Canta, Canta",
         "search":"canta canta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3983"
      },
      {
         "id":"3986",
         "name":"Huamantanga",
         "code":"03",
         "label":"Huamantanga, Canta",
         "search":"huamantanga canta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3983"
      },
      {
         "id":"3987",
         "name":"Huaros",
         "code":"04",
         "label":"Huaros, Canta",
         "search":"huaros canta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3983"
      },
      {
         "id":"3988",
         "name":"Lachaqui",
         "code":"05",
         "label":"Lachaqui, Canta",
         "search":"lachaqui canta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3983"
      },
      {
         "id":"3989",
         "name":"San Buenaventura",
         "code":"06",
         "label":"San Buenaventura, Canta",
         "search":"san buenaventura canta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3983"
      },
      {
         "id":"3990",
         "name":"Santa Rosa de Quives",
         "code":"07",
         "label":"Santa Rosa de Quives, Canta",
         "search":"santa rosa de quives canta",
         "children_count":"0",
         "level":"3",
         "parent_id":"3983"
      }
   ],
   "4008":[
      {
         "id":"4010",
         "name":"Atavillos Alto",
         "code":"02",
         "label":"Atavillos Alto, Huaral",
         "search":"atavillos alto huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      },
      {
         "id":"4011",
         "name":"Atavillos Bajo",
         "code":"03",
         "label":"Atavillos Bajo, Huaral",
         "search":"atavillos bajo huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      },
      {
         "id":"4012",
         "name":"Aucallama",
         "code":"04",
         "label":"Aucallama, Huaral",
         "search":"aucallama huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      },
      {
         "id":"4013",
         "name":"Chancay",
         "code":"05",
         "label":"Chancay, Huaral",
         "search":"chancay huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      },
      {
         "id":"4009",
         "name":"Huaral",
         "code":"01",
         "label":"Huaral, Huaral",
         "search":"huaral huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      },
      {
         "id":"4014",
         "name":"Ihuari",
         "code":"06",
         "label":"Ihuari, Huaral",
         "search":"ihuari huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      },
      {
         "id":"4015",
         "name":"Lampian",
         "code":"07",
         "label":"Lampian, Huaral",
         "search":"lampian huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      },
      {
         "id":"4016",
         "name":"Pacaraos",
         "code":"08",
         "label":"Pacaraos, Huaral",
         "search":"pacaraos huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      },
      {
         "id":"4017",
         "name":"San Miguel de Acos",
         "code":"09",
         "label":"San Miguel de Acos, Huaral",
         "search":"san miguel de acos huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      },
      {
         "id":"4018",
         "name":"Santa Cruz de Andamarca",
         "code":"10",
         "label":"Santa Cruz de Andamarca, Huaral",
         "search":"santa cruz de andamarca huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      },
      {
         "id":"4019",
         "name":"Sumbilca",
         "code":"11",
         "label":"Sumbilca, Huaral",
         "search":"sumbilca huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      },
      {
         "id":"4020",
         "name":"Veintisiete de Noviembre",
         "code":"12",
         "label":"Veintisiete de Noviembre, Huaral",
         "search":"veintisiete de noviembre huaral",
         "children_count":"0",
         "level":"3",
         "parent_id":"4008"
      }
   ],
   "4021":[
      {
         "id":"4023",
         "name":"Antioquia",
         "code":"02",
         "label":"Antioquia, Huarochiri",
         "search":"antioquia huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4024",
         "name":"Callahuanca",
         "code":"03",
         "label":"Callahuanca, Huarochiri",
         "search":"callahuanca huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4025",
         "name":"Carampoma",
         "code":"04",
         "label":"Carampoma, Huarochiri",
         "search":"carampoma huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4026",
         "name":"Chicla",
         "code":"05",
         "label":"Chicla, Huarochiri",
         "search":"chicla huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4027",
         "name":"Cuenca",
         "code":"06",
         "label":"Cuenca, Huarochiri",
         "search":"cuenca huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4028",
         "name":"Huachupampa",
         "code":"07",
         "label":"Huachupampa, Huarochiri",
         "search":"huachupampa huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4029",
         "name":"Huanza",
         "code":"08",
         "label":"Huanza, Huarochiri",
         "search":"huanza huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4030",
         "name":"Huarochiri",
         "code":"09",
         "label":"Huarochiri, Huarochiri",
         "search":"huarochiri huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4031",
         "name":"Lahuaytambo",
         "code":"10",
         "label":"Lahuaytambo, Huarochiri",
         "search":"lahuaytambo huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4032",
         "name":"Langa",
         "code":"11",
         "label":"Langa, Huarochiri",
         "search":"langa huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4033",
         "name":"Laraos",
         "code":"12",
         "label":"Laraos, Huarochiri",
         "search":"laraos huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4034",
         "name":"Mariatana",
         "code":"13",
         "label":"Mariatana, Huarochiri",
         "search":"mariatana huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4022",
         "name":"Matucana",
         "code":"01",
         "label":"Matucana, Huarochiri",
         "search":"matucana huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4035",
         "name":"Ricardo Palma",
         "code":"14",
         "label":"Ricardo Palma, Huarochiri",
         "search":"ricardo palma huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4036",
         "name":"San Andres de Tupicocha",
         "code":"15",
         "label":"San Andres de Tupicocha, Huarochiri",
         "search":"san andres de tupicocha huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4037",
         "name":"San Antonio",
         "code":"16",
         "label":"San Antonio, Huarochiri",
         "search":"san antonio huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4038",
         "name":"San Bartolome",
         "code":"17",
         "label":"San Bartolome, Huarochiri",
         "search":"san bartolome huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4039",
         "name":"San Damian",
         "code":"18",
         "label":"San Damian, Huarochiri",
         "search":"san damian huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4040",
         "name":"San Juan de Iris",
         "code":"19",
         "label":"San Juan de Iris, Huarochiri",
         "search":"san juan de iris huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4041",
         "name":"San Juan de Tantaranche",
         "code":"20",
         "label":"San Juan de Tantaranche, Huarochiri",
         "search":"san juan de tantaranche huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4042",
         "name":"San Lorenzo de Quinti",
         "code":"21",
         "label":"San Lorenzo de Quinti, Huarochiri",
         "search":"san lorenzo de quinti huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4043",
         "name":"San Mateo",
         "code":"22",
         "label":"San Mateo, Huarochiri",
         "search":"san mateo huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4044",
         "name":"San Mateo de Otao",
         "code":"23",
         "label":"San Mateo de Otao, Huarochiri",
         "search":"san mateo de otao huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4045",
         "name":"San Pedro de Casta",
         "code":"24",
         "label":"San Pedro de Casta, Huarochiri",
         "search":"san pedro de casta huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4046",
         "name":"San Pedro de Huancayre",
         "code":"25",
         "label":"San Pedro de Huancayre, Huarochiri",
         "search":"san pedro de huancayre huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4047",
         "name":"Sangallaya",
         "code":"26",
         "label":"Sangallaya, Huarochiri",
         "search":"sangallaya huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4048",
         "name":"Santa Cruz de Cocachacra",
         "code":"27",
         "label":"Santa Cruz de Cocachacra, Huarochiri",
         "search":"santa cruz de cocachacra huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4049",
         "name":"Santa Eulalia",
         "code":"28",
         "label":"Santa Eulalia, Huarochiri",
         "search":"santa eulalia huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4050",
         "name":"Santiago de Anchucaya",
         "code":"29",
         "label":"Santiago de Anchucaya, Huarochiri",
         "search":"santiago de anchucaya huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4051",
         "name":"Santiago de Tuna",
         "code":"30",
         "label":"Santiago de Tuna, Huarochiri",
         "search":"santiago de tuna huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4052",
         "name":"Santo Domingo de los Olleros",
         "code":"31",
         "label":"Santo Domingo de los Olleros, Huarochiri",
         "search":"santo domingo de los olleros huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      },
      {
         "id":"4053",
         "name":"Surco",
         "code":"32",
         "label":"Surco, Huarochiri",
         "search":"surco huarochiri",
         "children_count":"0",
         "level":"3",
         "parent_id":"4021"
      }
   ],
   "4054":[
      {
         "id":"4056",
         "name":"Ambar",
         "code":"02",
         "label":"Ambar, Huaura",
         "search":"ambar huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      },
      {
         "id":"4057",
         "name":"Caleta de Carquin",
         "code":"03",
         "label":"Caleta de Carquin, Huaura",
         "search":"caleta de carquin huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      },
      {
         "id":"4058",
         "name":"Checras",
         "code":"04",
         "label":"Checras, Huaura",
         "search":"checras huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      },
      {
         "id":"4055",
         "name":"Huacho",
         "code":"01",
         "label":"Huacho, Huaura",
         "search":"huacho huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      },
      {
         "id":"4059",
         "name":"Hualmay",
         "code":"05",
         "label":"Hualmay, Huaura",
         "search":"hualmay huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      },
      {
         "id":"4060",
         "name":"Huaura",
         "code":"06",
         "label":"Huaura, Huaura",
         "search":"huaura huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      },
      {
         "id":"4061",
         "name":"Leoncio Prado",
         "code":"07",
         "label":"Leoncio Prado, Huaura",
         "search":"leoncio prado huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      },
      {
         "id":"4062",
         "name":"Paccho",
         "code":"08",
         "label":"Paccho, Huaura",
         "search":"paccho huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      },
      {
         "id":"4063",
         "name":"Santa Leonor",
         "code":"09",
         "label":"Santa Leonor, Huaura",
         "search":"santa leonor huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      },
      {
         "id":"4064",
         "name":"Santa Maria",
         "code":"10",
         "label":"Santa Maria, Huaura",
         "search":"santa maria huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      },
      {
         "id":"4065",
         "name":"Sayan",
         "code":"11",
         "label":"Sayan, Huaura",
         "search":"sayan huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      },
      {
         "id":"4066",
         "name":"Vegueta",
         "code":"12",
         "label":"Vegueta, Huaura",
         "search":"vegueta huaura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4054"
      }
   ],
   "3927":[
      {
         "id":"3929",
         "name":"Ancon",
         "code":"02",
         "label":"Ancon, Lima",
         "search":"ancon lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3930",
         "name":"Ate",
         "code":"03",
         "label":"Ate, Lima",
         "search":"ate lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3931",
         "name":"Barranco",
         "code":"04",
         "label":"Barranco, Lima",
         "search":"barranco lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3932",
         "name":"Bre\u00f1a",
         "code":"05",
         "label":"Bre\u00f1a, Lima",
         "search":"brena lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3933",
         "name":"Carabayllo",
         "code":"06",
         "label":"Carabayllo, Lima",
         "search":"carabayllo lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3928",
         "name":"Cercado de Lima",
         "code":"01",
         "label":"Cercado de Lima, Lima",
         "search":"cercado de lima lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3934",
         "name":"Chaclacayo",
         "code":"07",
         "label":"Chaclacayo, Lima",
         "search":"chaclacayo lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3935",
         "name":"Chorrillos",
         "code":"08",
         "label":"Chorrillos, Lima",
         "search":"chorrillos lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3936",
         "name":"Cieneguilla",
         "code":"09",
         "label":"Cieneguilla, Lima",
         "search":"cieneguilla lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3937",
         "name":"Comas",
         "code":"10",
         "label":"Comas, Lima",
         "search":"comas lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3938",
         "name":"El Agustino",
         "code":"11",
         "label":"El Agustino, Lima",
         "search":"el agustino lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3939",
         "name":"Independencia",
         "code":"12",
         "label":"Independencia, Lima",
         "search":"independencia lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3940",
         "name":"Jesus Maria",
         "code":"13",
         "label":"Jesus Maria, Lima",
         "search":"jesus maria lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3941",
         "name":"La Molina",
         "code":"14",
         "label":"La Molina, Lima",
         "search":"la molina lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3942",
         "name":"La Victoria",
         "code":"15",
         "label":"La Victoria, Lima",
         "search":"la victoria lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3943",
         "name":"Lince",
         "code":"16",
         "label":"Lince, Lima",
         "search":"lince lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3944",
         "name":"Los Olivos",
         "code":"17",
         "label":"Los Olivos, Lima",
         "search":"los olivos lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3945",
         "name":"Lurigancho",
         "code":"18",
         "label":"Lurigancho, Lima",
         "search":"lurigancho lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3946",
         "name":"Lurin",
         "code":"19",
         "label":"Lurin, Lima",
         "search":"lurin lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3947",
         "name":"Magdalena del Mar",
         "code":"20",
         "label":"Magdalena del Mar, Lima",
         "search":"magdalena del mar lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3949",
         "name":"Miraflores",
         "code":"22",
         "label":"Miraflores, Lima",
         "search":"miraflores lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3950",
         "name":"Pachacamac",
         "code":"23",
         "label":"Pachacamac, Lima",
         "search":"pachacamac lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3951",
         "name":"Pucusana",
         "code":"24",
         "label":"Pucusana, Lima",
         "search":"pucusana lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3948",
         "name":"Pueblo Libre",
         "code":"21",
         "label":"Pueblo Libre, Lima",
         "search":"pueblo libre lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3952",
         "name":"Puente Piedra",
         "code":"25",
         "label":"Puente Piedra, Lima",
         "search":"puente piedra lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3953",
         "name":"Punta Hermosa",
         "code":"26",
         "label":"Punta Hermosa, Lima",
         "search":"punta hermosa lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3954",
         "name":"Punta Negra",
         "code":"27",
         "label":"Punta Negra, Lima",
         "search":"punta negra lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3955",
         "name":"Rimac",
         "code":"28",
         "label":"Rimac, Lima",
         "search":"rimac lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3956",
         "name":"San Bartolo",
         "code":"29",
         "label":"San Bartolo, Lima",
         "search":"san bartolo lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3957",
         "name":"San Borja",
         "code":"30",
         "label":"San Borja, Lima",
         "search":"san borja lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3958",
         "name":"San Isidro",
         "code":"31",
         "label":"San Isidro, Lima",
         "search":"san isidro lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3959",
         "name":"San Juan de Lurigancho",
         "code":"32",
         "label":"San Juan de Lurigancho, Lima",
         "search":"san juan de lurigancho lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3960",
         "name":"San Juan de Miraflores",
         "code":"33",
         "label":"San Juan de Miraflores, Lima",
         "search":"san juan de miraflores lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3961",
         "name":"San Luis",
         "code":"34",
         "label":"San Luis, Lima",
         "search":"san luis lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3962",
         "name":"San Martin de Porres",
         "code":"35",
         "label":"San Martin de Porres, Lima",
         "search":"san martin de porres lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3963",
         "name":"San Miguel",
         "code":"36",
         "label":"San Miguel, Lima",
         "search":"san miguel lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3964",
         "name":"Santa Anita",
         "code":"37",
         "label":"Santa Anita, Lima",
         "search":"santa anita lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3965",
         "name":"Santa Maria del Mar",
         "code":"38",
         "label":"Santa Maria del Mar, Lima",
         "search":"santa maria del mar lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3966",
         "name":"Santa Rosa",
         "code":"39",
         "label":"Santa Rosa, Lima",
         "search":"santa rosa lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3967",
         "name":"Santiago de Surco",
         "code":"40",
         "label":"Santiago de Surco, Lima",
         "search":"santiago de surco lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3968",
         "name":"Surquillo",
         "code":"41",
         "label":"Surquillo, Lima",
         "search":"surquillo lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3969",
         "name":"Villa El Salvador",
         "code":"42",
         "label":"Villa El Salvador, Lima",
         "search":"villa el salvador lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      },
      {
         "id":"3970",
         "name":"Villa Maria del Triunfo",
         "code":"43",
         "label":"Villa Maria del Triunfo, Lima",
         "search":"villa maria del triunfo lima",
         "children_count":"0",
         "level":"3",
         "parent_id":"3927"
      }
   ],
   "4067":[
      {
         "id":"4069",
         "name":"Andajes",
         "code":"02",
         "label":"Andajes, Oyon",
         "search":"andajes oyon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4067"
      },
      {
         "id":"4070",
         "name":"Caujul",
         "code":"03",
         "label":"Caujul, Oyon",
         "search":"caujul oyon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4067"
      },
      {
         "id":"4071",
         "name":"Cochamarca",
         "code":"04",
         "label":"Cochamarca, Oyon",
         "search":"cochamarca oyon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4067"
      },
      {
         "id":"4072",
         "name":"Navan",
         "code":"05",
         "label":"Navan, Oyon",
         "search":"navan oyon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4067"
      },
      {
         "id":"4068",
         "name":"Oyon",
         "code":"01",
         "label":"Oyon, Oyon",
         "search":"oyon oyon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4067"
      },
      {
         "id":"4073",
         "name":"Pachangara",
         "code":"06",
         "label":"Pachangara, Oyon",
         "search":"pachangara oyon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4067"
      }
   ],
   "4074":[
      {
         "id":"4076",
         "name":"Alis",
         "code":"02",
         "label":"Alis, Yauyos",
         "search":"alis yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4077",
         "name":"Ayauca",
         "code":"03",
         "label":"Ayauca, Yauyos",
         "search":"ayauca yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4078",
         "name":"Ayaviri",
         "code":"04",
         "label":"Ayaviri, Yauyos",
         "search":"ayaviri yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4079",
         "name":"Azangaro",
         "code":"05",
         "label":"Azangaro, Yauyos",
         "search":"azangaro yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4080",
         "name":"Cacra",
         "code":"06",
         "label":"Cacra, Yauyos",
         "search":"cacra yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4081",
         "name":"Carania",
         "code":"07",
         "label":"Carania, Yauyos",
         "search":"carania yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4082",
         "name":"Catahuasi",
         "code":"08",
         "label":"Catahuasi, Yauyos",
         "search":"catahuasi yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4083",
         "name":"Chocos",
         "code":"09",
         "label":"Chocos, Yauyos",
         "search":"chocos yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4084",
         "name":"Cochas",
         "code":"10",
         "label":"Cochas, Yauyos",
         "search":"cochas yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4085",
         "name":"Colonia",
         "code":"11",
         "label":"Colonia, Yauyos",
         "search":"colonia yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4086",
         "name":"Hongos",
         "code":"12",
         "label":"Hongos, Yauyos",
         "search":"hongos yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4087",
         "name":"Huampara",
         "code":"13",
         "label":"Huampara, Yauyos",
         "search":"huampara yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4088",
         "name":"Huancaya",
         "code":"14",
         "label":"Huancaya, Yauyos",
         "search":"huancaya yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4089",
         "name":"Huangascar",
         "code":"15",
         "label":"Huangascar, Yauyos",
         "search":"huangascar yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4090",
         "name":"Huantan",
         "code":"16",
         "label":"Huantan, Yauyos",
         "search":"huantan yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4091",
         "name":"Huaqec",
         "code":"17",
         "label":"Huaqec, Yauyos",
         "search":"huaqec yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4092",
         "name":"Laraos",
         "code":"18",
         "label":"Laraos, Yauyos",
         "search":"laraos yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4093",
         "name":"Lincha",
         "code":"19",
         "label":"Lincha, Yauyos",
         "search":"lincha yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4094",
         "name":"Madean",
         "code":"20",
         "label":"Madean, Yauyos",
         "search":"madean yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4095",
         "name":"Miraflores",
         "code":"21",
         "label":"Miraflores, Yauyos",
         "search":"miraflores yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4096",
         "name":"Omas",
         "code":"22",
         "label":"Omas, Yauyos",
         "search":"omas yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4097",
         "name":"Putinza",
         "code":"23",
         "label":"Putinza, Yauyos",
         "search":"putinza yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4098",
         "name":"Quinches",
         "code":"24",
         "label":"Quinches, Yauyos",
         "search":"quinches yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4099",
         "name":"Quinocay",
         "code":"25",
         "label":"Quinocay, Yauyos",
         "search":"quinocay yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4100",
         "name":"San Joaquin",
         "code":"26",
         "label":"San Joaquin, Yauyos",
         "search":"san joaquin yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4101",
         "name":"San Pedro de Pilas",
         "code":"27",
         "label":"San Pedro de Pilas, Yauyos",
         "search":"san pedro de pilas yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4102",
         "name":"Tanta",
         "code":"28",
         "label":"Tanta, Yauyos",
         "search":"tanta yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4103",
         "name":"Tauripampa",
         "code":"29",
         "label":"Tauripampa, Yauyos",
         "search":"tauripampa yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4104",
         "name":"Tomas",
         "code":"30",
         "label":"Tomas, Yauyos",
         "search":"tomas yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4105",
         "name":"Tupe",
         "code":"31",
         "label":"Tupe, Yauyos",
         "search":"tupe yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4106",
         "name":"Viqac",
         "code":"32",
         "label":"Viqac, Yauyos",
         "search":"viqac yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4107",
         "name":"Vitis",
         "code":"33",
         "label":"Vitis, Yauyos",
         "search":"vitis yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      },
      {
         "id":"4075",
         "name":"Yauyos",
         "code":"01",
         "label":"Yauyos, Yauyos",
         "search":"yauyos yauyos",
         "children_count":"0",
         "level":"3",
         "parent_id":"4074"
      }
   ],
   "4123":[
      {
         "id":"4125",
         "name":"Balsapuerto",
         "code":"02",
         "label":"Balsapuerto, Alto Amazonas",
         "search":"balsapuerto alto amazonas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4123"
      },
      {
         "id":"4126",
         "name":"Barranca",
         "code":"03",
         "label":"Barranca, Alto Amazonas",
         "search":"barranca alto amazonas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4123"
      },
      {
         "id":"4127",
         "name":"Cahuapanas",
         "code":"04",
         "label":"Cahuapanas, Alto Amazonas",
         "search":"cahuapanas alto amazonas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4123"
      },
      {
         "id":"4128",
         "name":"Jeberos",
         "code":"05",
         "label":"Jeberos, Alto Amazonas",
         "search":"jeberos alto amazonas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4123"
      },
      {
         "id":"4129",
         "name":"Lagunas",
         "code":"06",
         "label":"Lagunas, Alto Amazonas",
         "search":"lagunas alto amazonas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4123"
      },
      {
         "id":"4130",
         "name":"Manseriche",
         "code":"07",
         "label":"Manseriche, Alto Amazonas",
         "search":"manseriche alto amazonas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4123"
      },
      {
         "id":"4131",
         "name":"Morona",
         "code":"08",
         "label":"Morona, Alto Amazonas",
         "search":"morona alto amazonas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4123"
      },
      {
         "id":"4132",
         "name":"Pastaza",
         "code":"09",
         "label":"Pastaza, Alto Amazonas",
         "search":"pastaza alto amazonas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4123"
      },
      {
         "id":"4133",
         "name":"Santa Cruz",
         "code":"10",
         "label":"Santa Cruz, Alto Amazonas",
         "search":"santa cruz alto amazonas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4123"
      },
      {
         "id":"4134",
         "name":"Teniente Cesar Lopez Rojas",
         "code":"11",
         "label":"Teniente Cesar Lopez Rojas, Alto Amazonas",
         "search":"teniente cesar lopez rojas alto amazonas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4123"
      },
      {
         "id":"4124",
         "name":"Yurimaguas",
         "code":"01",
         "label":"Yurimaguas, Alto Amazonas",
         "search":"yurimaguas alto amazonas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4123"
      }
   ],
   "4135":[
      {
         "id":"4136",
         "name":"Nauta",
         "code":"01",
         "label":"Nauta, Loreto",
         "search":"nauta loreto",
         "children_count":"0",
         "level":"3",
         "parent_id":"4135"
      },
      {
         "id":"4137",
         "name":"Parinari",
         "code":"02",
         "label":"Parinari, Loreto",
         "search":"parinari loreto",
         "children_count":"0",
         "level":"3",
         "parent_id":"4135"
      },
      {
         "id":"4138",
         "name":"Tigre",
         "code":"03",
         "label":"Tigre, Loreto",
         "search":"tigre loreto",
         "children_count":"0",
         "level":"3",
         "parent_id":"4135"
      },
      {
         "id":"4139",
         "name":"Trompeteros",
         "code":"04",
         "label":"Trompeteros, Loreto",
         "search":"trompeteros loreto",
         "children_count":"0",
         "level":"3",
         "parent_id":"4135"
      },
      {
         "id":"4140",
         "name":"Urarinas",
         "code":"05",
         "label":"Urarinas, Loreto",
         "search":"urarinas loreto",
         "children_count":"0",
         "level":"3",
         "parent_id":"4135"
      }
   ],
   "4141":[
      {
         "id":"4143",
         "name":"Pebas",
         "code":"02",
         "label":"Pebas, Mariscal Ramon Castilla",
         "search":"pebas mariscal ramon castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"4141"
      },
      {
         "id":"4142",
         "name":"Ramon Castilla",
         "code":"01",
         "label":"Ramon Castilla, Mariscal Ramon Castilla",
         "search":"ramon castilla mariscal ramon castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"4141"
      },
      {
         "id":"4145",
         "name":"San Pablo",
         "code":"04",
         "label":"San Pablo, Mariscal Ramon Castilla",
         "search":"san pablo mariscal ramon castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"4141"
      },
      {
         "id":"4144",
         "name":"Yavari",
         "code":"03",
         "label":"Yavari, Mariscal Ramon Castilla",
         "search":"yavari mariscal ramon castilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"4141"
      }
   ],
   "4109":[
      {
         "id":"4111",
         "name":"Alto Nanay",
         "code":"02",
         "label":"Alto Nanay, Maynas",
         "search":"alto nanay maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4121",
         "name":"Bel\u00e9n",
         "code":"12",
         "label":"Bel\u00e9n, Maynas",
         "search":"bel\u00e9n maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4112",
         "name":"Fernando Lores",
         "code":"03",
         "label":"Fernando Lores, Maynas",
         "search":"fernando lores maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4113",
         "name":"Indiana",
         "code":"04",
         "label":"Indiana, Maynas",
         "search":"indiana maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4110",
         "name":"Iquitos",
         "code":"01",
         "label":"Iquitos, Maynas",
         "search":"iquitos maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4114",
         "name":"Las Amazonas",
         "code":"05",
         "label":"Las Amazonas, Maynas",
         "search":"las amazonas maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4115",
         "name":"Mazan",
         "code":"06",
         "label":"Mazan, Maynas",
         "search":"mazan maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4116",
         "name":"Napo",
         "code":"07",
         "label":"Napo, Maynas",
         "search":"napo maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4117",
         "name":"Punchana",
         "code":"08",
         "label":"Punchana, Maynas",
         "search":"punchana maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4118",
         "name":"Putumayo",
         "code":"09",
         "label":"Putumayo, Maynas",
         "search":"putumayo maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4122",
         "name":"San Juan Bautista",
         "code":"13",
         "label":"San Juan Bautista, Maynas",
         "search":"san juan bautista maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4119",
         "name":"Torres Causana",
         "code":"10",
         "label":"Torres Causana, Maynas",
         "search":"torres causana maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      },
      {
         "id":"4120",
         "name":"Yaquerana",
         "code":"11",
         "label":"Yaquerana, Maynas",
         "search":"yaquerana maynas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4109"
      }
   ],
   "4146":[
      {
         "id":"4148",
         "name":"Alto Tapiche",
         "code":"02",
         "label":"Alto Tapiche, Requena",
         "search":"alto tapiche requena",
         "children_count":"0",
         "level":"3",
         "parent_id":"4146"
      },
      {
         "id":"4149",
         "name":"Capelo",
         "code":"03",
         "label":"Capelo, Requena",
         "search":"capelo requena",
         "children_count":"0",
         "level":"3",
         "parent_id":"4146"
      },
      {
         "id":"4150",
         "name":"Emilio San Martin",
         "code":"04",
         "label":"Emilio San Martin, Requena",
         "search":"emilio san martin requena",
         "children_count":"0",
         "level":"3",
         "parent_id":"4146"
      },
      {
         "id":"4156",
         "name":"Jenaro Herrera",
         "code":"10",
         "label":"Jenaro Herrera, Requena",
         "search":"jenaro herrera requena",
         "children_count":"0",
         "level":"3",
         "parent_id":"4146"
      },
      {
         "id":"4151",
         "name":"Maquia",
         "code":"05",
         "label":"Maquia, Requena",
         "search":"maquia requena",
         "children_count":"0",
         "level":"3",
         "parent_id":"4146"
      },
      {
         "id":"4152",
         "name":"Puinahua",
         "code":"06",
         "label":"Puinahua, Requena",
         "search":"puinahua requena",
         "children_count":"0",
         "level":"3",
         "parent_id":"4146"
      },
      {
         "id":"4147",
         "name":"Requena",
         "code":"01",
         "label":"Requena, Requena",
         "search":"requena requena",
         "children_count":"0",
         "level":"3",
         "parent_id":"4146"
      },
      {
         "id":"4153",
         "name":"Saquena",
         "code":"07",
         "label":"Saquena, Requena",
         "search":"saquena requena",
         "children_count":"0",
         "level":"3",
         "parent_id":"4146"
      },
      {
         "id":"4154",
         "name":"Soplin",
         "code":"08",
         "label":"Soplin, Requena",
         "search":"soplin requena",
         "children_count":"0",
         "level":"3",
         "parent_id":"4146"
      },
      {
         "id":"4155",
         "name":"Tapiche",
         "code":"09",
         "label":"Tapiche, Requena",
         "search":"tapiche requena",
         "children_count":"0",
         "level":"3",
         "parent_id":"4146"
      },
      {
         "id":"4157",
         "name":"Yaquerana",
         "code":"11",
         "label":"Yaquerana, Requena",
         "search":"yaquerana requena",
         "children_count":"0",
         "level":"3",
         "parent_id":"4146"
      }
   ],
   "4158":[
      {
         "id":"4159",
         "name":"Contamana",
         "code":"01",
         "label":"Contamana, Ucayali",
         "search":"contamana ucayali",
         "children_count":"0",
         "level":"3",
         "parent_id":"4158"
      },
      {
         "id":"4160",
         "name":"Inahuaya",
         "code":"02",
         "label":"Inahuaya, Ucayali",
         "search":"inahuaya ucayali",
         "children_count":"0",
         "level":"3",
         "parent_id":"4158"
      },
      {
         "id":"4161",
         "name":"Padre Marquez",
         "code":"03",
         "label":"Padre Marquez, Ucayali",
         "search":"padre marquez ucayali",
         "children_count":"0",
         "level":"3",
         "parent_id":"4158"
      },
      {
         "id":"4162",
         "name":"Pampa Hermosa",
         "code":"04",
         "label":"Pampa Hermosa, Ucayali",
         "search":"pampa hermosa ucayali",
         "children_count":"0",
         "level":"3",
         "parent_id":"4158"
      },
      {
         "id":"4163",
         "name":"Sarayacu",
         "code":"05",
         "label":"Sarayacu, Ucayali",
         "search":"sarayacu ucayali",
         "children_count":"0",
         "level":"3",
         "parent_id":"4158"
      },
      {
         "id":"4164",
         "name":"Vargas Guerra",
         "code":"06",
         "label":"Vargas Guerra, Ucayali",
         "search":"vargas guerra ucayali",
         "children_count":"0",
         "level":"3",
         "parent_id":"4158"
      }
   ],
   "4171":[
      {
         "id":"4173",
         "name":"Fitzcarrald",
         "code":"02",
         "label":"Fitzcarrald, Manu",
         "search":"fitzcarrald manu",
         "children_count":"0",
         "level":"3",
         "parent_id":"4171"
      },
      {
         "id":"4175",
         "name":"Huepetuhe",
         "code":"04",
         "label":"Huepetuhe, Manu",
         "search":"huepetuhe manu",
         "children_count":"0",
         "level":"3",
         "parent_id":"4171"
      },
      {
         "id":"4174",
         "name":"Madre de Dios",
         "code":"03",
         "label":"Madre de Dios, Manu",
         "search":"madre de dios manu",
         "children_count":"0",
         "level":"3",
         "parent_id":"4171"
      },
      {
         "id":"4172",
         "name":"Manu",
         "code":"01",
         "label":"Manu, Manu",
         "search":"manu manu",
         "children_count":"0",
         "level":"3",
         "parent_id":"4171"
      }
   ],
   "4176":[
      {
         "id":"4178",
         "name":"Iberia",
         "code":"02",
         "label":"Iberia, Tahuamanu",
         "search":"iberia tahuamanu",
         "children_count":"0",
         "level":"3",
         "parent_id":"4176"
      },
      {
         "id":"4177",
         "name":"Iqapari",
         "code":"01",
         "label":"Iqapari, Tahuamanu",
         "search":"iqapari tahuamanu",
         "children_count":"0",
         "level":"3",
         "parent_id":"4176"
      },
      {
         "id":"4179",
         "name":"Tahuamanu",
         "code":"03",
         "label":"Tahuamanu, Tahuamanu",
         "search":"tahuamanu tahuamanu",
         "children_count":"0",
         "level":"3",
         "parent_id":"4176"
      }
   ],
   "4166":[
      {
         "id":"4168",
         "name":"Inambari",
         "code":"02",
         "label":"Inambari, Tambopata",
         "search":"inambari tambopata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4166"
      },
      {
         "id":"4170",
         "name":"Laberinto",
         "code":"04",
         "label":"Laberinto, Tambopata",
         "search":"laberinto tambopata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4166"
      },
      {
         "id":"4169",
         "name":"Las Piedras",
         "code":"03",
         "label":"Las Piedras, Tambopata",
         "search":"las piedras tambopata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4166"
      },
      {
         "id":"4167",
         "name":"Tambopata",
         "code":"01",
         "label":"Tambopata, Tambopata",
         "search":"tambopata tambopata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4166"
      }
   ],
   "4188":[
      {
         "id":"4190",
         "name":"Chojata",
         "code":"02",
         "label":"Chojata, General Sanchez Cerro",
         "search":"chojata general sanchez cerro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4188"
      },
      {
         "id":"4191",
         "name":"Coalaque",
         "code":"03",
         "label":"Coalaque, General Sanchez Cerro",
         "search":"coalaque general sanchez cerro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4188"
      },
      {
         "id":"4192",
         "name":"Ichuqa",
         "code":"04",
         "label":"Ichuqa, General Sanchez Cerro",
         "search":"ichuqa general sanchez cerro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4188"
      },
      {
         "id":"4193",
         "name":"La Capilla",
         "code":"05",
         "label":"La Capilla, General Sanchez Cerro",
         "search":"la capilla general sanchez cerro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4188"
      },
      {
         "id":"4194",
         "name":"Lloque",
         "code":"06",
         "label":"Lloque, General Sanchez Cerro",
         "search":"lloque general sanchez cerro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4188"
      },
      {
         "id":"4195",
         "name":"Matalaque",
         "code":"07",
         "label":"Matalaque, General Sanchez Cerro",
         "search":"matalaque general sanchez cerro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4188"
      },
      {
         "id":"4189",
         "name":"Omate",
         "code":"01",
         "label":"Omate, General Sanchez Cerro",
         "search":"omate general sanchez cerro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4188"
      },
      {
         "id":"4196",
         "name":"Puquina",
         "code":"08",
         "label":"Puquina, General Sanchez Cerro",
         "search":"puquina general sanchez cerro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4188"
      },
      {
         "id":"4197",
         "name":"Quinistaquillas",
         "code":"09",
         "label":"Quinistaquillas, General Sanchez Cerro",
         "search":"quinistaquillas general sanchez cerro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4188"
      },
      {
         "id":"4198",
         "name":"Ubinas",
         "code":"10",
         "label":"Ubinas, General Sanchez Cerro",
         "search":"ubinas general sanchez cerro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4188"
      },
      {
         "id":"4199",
         "name":"Yunga",
         "code":"11",
         "label":"Yunga, General Sanchez Cerro",
         "search":"yunga general sanchez cerro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4188"
      }
   ],
   "4200":[
      {
         "id":"4202",
         "name":"El Algarrobal",
         "code":"02",
         "label":"El Algarrobal, Ilo",
         "search":"el algarrobal ilo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4200"
      },
      {
         "id":"4201",
         "name":"Ilo",
         "code":"01",
         "label":"Ilo, Ilo",
         "search":"ilo ilo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4200"
      },
      {
         "id":"4203",
         "name":"Pacocha",
         "code":"03",
         "label":"Pacocha, Ilo",
         "search":"pacocha ilo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4200"
      }
   ],
   "4181":[
      {
         "id":"4183",
         "name":"Carumas",
         "code":"02",
         "label":"Carumas, Mariscal Nieto",
         "search":"carumas mariscal nieto",
         "children_count":"0",
         "level":"3",
         "parent_id":"4181"
      },
      {
         "id":"4184",
         "name":"Cuchumbaya",
         "code":"03",
         "label":"Cuchumbaya, Mariscal Nieto",
         "search":"cuchumbaya mariscal nieto",
         "children_count":"0",
         "level":"3",
         "parent_id":"4181"
      },
      {
         "id":"4182",
         "name":"Moquegua",
         "code":"01",
         "label":"Moquegua, Mariscal Nieto",
         "search":"moquegua mariscal nieto",
         "children_count":"0",
         "level":"3",
         "parent_id":"4181"
      },
      {
         "id":"4185",
         "name":"Samegua",
         "code":"04",
         "label":"Samegua, Mariscal Nieto",
         "search":"samegua mariscal nieto",
         "children_count":"0",
         "level":"3",
         "parent_id":"4181"
      },
      {
         "id":"4186",
         "name":"San Cristobal",
         "code":"05",
         "label":"San Cristobal, Mariscal Nieto",
         "search":"san cristobal mariscal nieto",
         "children_count":"0",
         "level":"3",
         "parent_id":"4181"
      },
      {
         "id":"4187",
         "name":"Torata",
         "code":"06",
         "label":"Torata, Mariscal Nieto",
         "search":"torata mariscal nieto",
         "children_count":"0",
         "level":"3",
         "parent_id":"4181"
      }
   ],
   "4219":[
      {
         "id":"4221",
         "name":"Chacayan",
         "code":"02",
         "label":"Chacayan, Daniel Alcides Carrion",
         "search":"chacayan daniel alcides carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"4219"
      },
      {
         "id":"4222",
         "name":"Goyllarisquizga",
         "code":"03",
         "label":"Goyllarisquizga, Daniel Alcides Carrion",
         "search":"goyllarisquizga daniel alcides carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"4219"
      },
      {
         "id":"4223",
         "name":"Paucar",
         "code":"04",
         "label":"Paucar, Daniel Alcides Carrion",
         "search":"paucar daniel alcides carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"4219"
      },
      {
         "id":"4224",
         "name":"San Pedro de Pillao",
         "code":"05",
         "label":"San Pedro de Pillao, Daniel Alcides Carrion",
         "search":"san pedro de pillao daniel alcides carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"4219"
      },
      {
         "id":"4225",
         "name":"Santa Ana de Tusi",
         "code":"06",
         "label":"Santa Ana de Tusi, Daniel Alcides Carrion",
         "search":"santa ana de tusi daniel alcides carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"4219"
      },
      {
         "id":"4226",
         "name":"Tapuc",
         "code":"07",
         "label":"Tapuc, Daniel Alcides Carrion",
         "search":"tapuc daniel alcides carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"4219"
      },
      {
         "id":"4227",
         "name":"Vilcabamba",
         "code":"08",
         "label":"Vilcabamba, Daniel Alcides Carrion",
         "search":"vilcabamba daniel alcides carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"4219"
      },
      {
         "id":"4220",
         "name":"Yanahuanca",
         "code":"01",
         "label":"Yanahuanca, Daniel Alcides Carrion",
         "search":"yanahuanca daniel alcides carrion",
         "children_count":"0",
         "level":"3",
         "parent_id":"4219"
      }
   ],
   "4228":[
      {
         "id":"4230",
         "name":"Chontabamba",
         "code":"02",
         "label":"Chontabamba, Oxapampa",
         "search":"chontabamba oxapampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4228"
      },
      {
         "id":"4231",
         "name":"Huancabamba",
         "code":"03",
         "label":"Huancabamba, Oxapampa",
         "search":"huancabamba oxapampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4228"
      },
      {
         "id":"4229",
         "name":"Oxapampa",
         "code":"01",
         "label":"Oxapampa, Oxapampa",
         "search":"oxapampa oxapampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4228"
      },
      {
         "id":"4232",
         "name":"Palcazu",
         "code":"04",
         "label":"Palcazu, Oxapampa",
         "search":"palcazu oxapampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4228"
      },
      {
         "id":"4233",
         "name":"Pozuzo",
         "code":"05",
         "label":"Pozuzo, Oxapampa",
         "search":"pozuzo oxapampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4228"
      },
      {
         "id":"4234",
         "name":"Puerto Bermudez",
         "code":"06",
         "label":"Puerto Bermudez, Oxapampa",
         "search":"puerto bermudez oxapampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4228"
      },
      {
         "id":"4235",
         "name":"Villa Rica",
         "code":"07",
         "label":"Villa Rica, Oxapampa",
         "search":"villa rica oxapampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4228"
      }
   ],
   "4205":[
      {
         "id":"4206",
         "name":"Chaupimarca",
         "code":"01",
         "label":"Chaupimarca, Pasco",
         "search":"chaupimarca pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4207",
         "name":"Huachon",
         "code":"02",
         "label":"Huachon, Pasco",
         "search":"huachon pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4208",
         "name":"Huariaca",
         "code":"03",
         "label":"Huariaca, Pasco",
         "search":"huariaca pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4209",
         "name":"Huayllay",
         "code":"04",
         "label":"Huayllay, Pasco",
         "search":"huayllay pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4210",
         "name":"Ninacaca",
         "code":"05",
         "label":"Ninacaca, Pasco",
         "search":"ninacaca pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4211",
         "name":"Pallanchacra",
         "code":"06",
         "label":"Pallanchacra, Pasco",
         "search":"pallanchacra pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4212",
         "name":"Paucartambo",
         "code":"07",
         "label":"Paucartambo, Pasco",
         "search":"paucartambo pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4213",
         "name":"San Fco.De Asis de Yarusyacan",
         "code":"08",
         "label":"San Fco.De Asis de Yarusyacan, Pasco",
         "search":"san fco.de asis de yarusyacan pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4214",
         "name":"Simon Bolivar",
         "code":"09",
         "label":"Simon Bolivar, Pasco",
         "search":"simon bolivar pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4215",
         "name":"Ticlacayan",
         "code":"10",
         "label":"Ticlacayan, Pasco",
         "search":"ticlacayan pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4216",
         "name":"Tinyahuarco",
         "code":"11",
         "label":"Tinyahuarco, Pasco",
         "search":"tinyahuarco pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4217",
         "name":"Vicco",
         "code":"12",
         "label":"Vicco, Pasco",
         "search":"vicco pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      },
      {
         "id":"4218",
         "name":"Yanacancha",
         "code":"13",
         "label":"Yanacancha, Pasco",
         "search":"yanacancha pasco",
         "children_count":"0",
         "level":"3",
         "parent_id":"4205"
      }
   ],
   "4247":[
      {
         "id":"4248",
         "name":"Ayabaca",
         "code":"01",
         "label":"Ayabaca, Ayabaca",
         "search":"ayabaca ayabaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"4247"
      },
      {
         "id":"4249",
         "name":"Frias",
         "code":"02",
         "label":"Frias, Ayabaca",
         "search":"frias ayabaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"4247"
      },
      {
         "id":"4250",
         "name":"Jilili",
         "code":"03",
         "label":"Jilili, Ayabaca",
         "search":"jilili ayabaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"4247"
      },
      {
         "id":"4251",
         "name":"Lagunas",
         "code":"04",
         "label":"Lagunas, Ayabaca",
         "search":"lagunas ayabaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"4247"
      },
      {
         "id":"4252",
         "name":"Montero",
         "code":"05",
         "label":"Montero, Ayabaca",
         "search":"montero ayabaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"4247"
      },
      {
         "id":"4253",
         "name":"Pacaipampa",
         "code":"06",
         "label":"Pacaipampa, Ayabaca",
         "search":"pacaipampa ayabaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"4247"
      },
      {
         "id":"4254",
         "name":"Paimas",
         "code":"07",
         "label":"Paimas, Ayabaca",
         "search":"paimas ayabaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"4247"
      },
      {
         "id":"4255",
         "name":"Sapillica",
         "code":"08",
         "label":"Sapillica, Ayabaca",
         "search":"sapillica ayabaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"4247"
      },
      {
         "id":"4256",
         "name":"Sicchez",
         "code":"09",
         "label":"Sicchez, Ayabaca",
         "search":"sicchez ayabaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"4247"
      },
      {
         "id":"4257",
         "name":"Suyo",
         "code":"10",
         "label":"Suyo, Ayabaca",
         "search":"suyo ayabaca",
         "children_count":"0",
         "level":"3",
         "parent_id":"4247"
      }
   ],
   "4258":[
      {
         "id":"4260",
         "name":"Canchaque",
         "code":"02",
         "label":"Canchaque, Huancabamba",
         "search":"canchaque huancabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4258"
      },
      {
         "id":"4261",
         "name":"El Carmen de la Frontera",
         "code":"03",
         "label":"El Carmen de la Frontera, Huancabamba",
         "search":"el carmen de la frontera huancabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4258"
      },
      {
         "id":"4259",
         "name":"Huancabamba",
         "code":"01",
         "label":"Huancabamba, Huancabamba",
         "search":"huancabamba huancabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4258"
      },
      {
         "id":"4262",
         "name":"Huarmaca",
         "code":"04",
         "label":"Huarmaca, Huancabamba",
         "search":"huarmaca huancabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4258"
      },
      {
         "id":"4263",
         "name":"Lalaquiz",
         "code":"05",
         "label":"Lalaquiz, Huancabamba",
         "search":"lalaquiz huancabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4258"
      },
      {
         "id":"4264",
         "name":"San Miguel de El Faique",
         "code":"06",
         "label":"San Miguel de El Faique, Huancabamba",
         "search":"san miguel de el faique huancabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4258"
      },
      {
         "id":"4265",
         "name":"Sondor",
         "code":"07",
         "label":"Sondor, Huancabamba",
         "search":"sondor huancabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4258"
      },
      {
         "id":"4266",
         "name":"Sondorillo",
         "code":"08",
         "label":"Sondorillo, Huancabamba",
         "search":"sondorillo huancabamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4258"
      }
   ],
   "4267":[
      {
         "id":"4269",
         "name":"Buenos Aires",
         "code":"02",
         "label":"Buenos Aires, Morropon",
         "search":"buenos aires morropon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4267"
      },
      {
         "id":"4270",
         "name":"Chalaco",
         "code":"03",
         "label":"Chalaco, Morropon",
         "search":"chalaco morropon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4267"
      },
      {
         "id":"4268",
         "name":"Chulucanas",
         "code":"01",
         "label":"Chulucanas, Morropon",
         "search":"chulucanas morropon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4267"
      },
      {
         "id":"4271",
         "name":"La Matanza",
         "code":"04",
         "label":"La Matanza, Morropon",
         "search":"la matanza morropon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4267"
      },
      {
         "id":"4272",
         "name":"Morropon",
         "code":"05",
         "label":"Morropon, Morropon",
         "search":"morropon morropon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4267"
      },
      {
         "id":"4273",
         "name":"Salitral",
         "code":"06",
         "label":"Salitral, Morropon",
         "search":"salitral morropon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4267"
      },
      {
         "id":"4274",
         "name":"San Juan de Bigote",
         "code":"07",
         "label":"San Juan de Bigote, Morropon",
         "search":"san juan de bigote morropon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4267"
      },
      {
         "id":"4275",
         "name":"Santa Catalina de Mossa",
         "code":"08",
         "label":"Santa Catalina de Mossa, Morropon",
         "search":"santa catalina de mossa morropon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4267"
      },
      {
         "id":"4276",
         "name":"Santo Domingo",
         "code":"09",
         "label":"Santo Domingo, Morropon",
         "search":"santo domingo morropon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4267"
      },
      {
         "id":"4277",
         "name":"Yamango",
         "code":"10",
         "label":"Yamango, Morropon",
         "search":"yamango morropon",
         "children_count":"0",
         "level":"3",
         "parent_id":"4267"
      }
   ],
   "4278":[
      {
         "id":"4280",
         "name":"Amotape",
         "code":"02",
         "label":"Amotape, Paita",
         "search":"amotape paita",
         "children_count":"0",
         "level":"3",
         "parent_id":"4278"
      },
      {
         "id":"4281",
         "name":"Arenal",
         "code":"03",
         "label":"Arenal, Paita",
         "search":"arenal paita",
         "children_count":"0",
         "level":"3",
         "parent_id":"4278"
      },
      {
         "id":"4282",
         "name":"Colan",
         "code":"04",
         "label":"Colan, Paita",
         "search":"colan paita",
         "children_count":"0",
         "level":"3",
         "parent_id":"4278"
      },
      {
         "id":"4283",
         "name":"La Huaca",
         "code":"05",
         "label":"La Huaca, Paita",
         "search":"la huaca paita",
         "children_count":"0",
         "level":"3",
         "parent_id":"4278"
      },
      {
         "id":"4279",
         "name":"Paita",
         "code":"01",
         "label":"Paita, Paita",
         "search":"paita paita",
         "children_count":"0",
         "level":"3",
         "parent_id":"4278"
      },
      {
         "id":"4284",
         "name":"Tamarindo",
         "code":"06",
         "label":"Tamarindo, Paita",
         "search":"tamarindo paita",
         "children_count":"0",
         "level":"3",
         "parent_id":"4278"
      },
      {
         "id":"4285",
         "name":"Vichayal",
         "code":"07",
         "label":"Vichayal, Paita",
         "search":"vichayal paita",
         "children_count":"0",
         "level":"3",
         "parent_id":"4278"
      }
   ],
   "4237":[
      {
         "id":"4239",
         "name":"Castilla",
         "code":"04",
         "label":"Castilla, Piura",
         "search":"castilla piura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4237"
      },
      {
         "id":"4240",
         "name":"Catacaos",
         "code":"05",
         "label":"Catacaos, Piura",
         "search":"catacaos piura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4237"
      },
      {
         "id":"4241",
         "name":"Cura Mori",
         "code":"07",
         "label":"Cura Mori, Piura",
         "search":"cura mori piura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4237"
      },
      {
         "id":"4242",
         "name":"El Tallan",
         "code":"08",
         "label":"El Tallan, Piura",
         "search":"el tallan piura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4237"
      },
      {
         "id":"4243",
         "name":"La Arena",
         "code":"09",
         "label":"La Arena, Piura",
         "search":"la arena piura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4237"
      },
      {
         "id":"4244",
         "name":"La Union",
         "code":"10",
         "label":"La Union, Piura",
         "search":"la union piura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4237"
      },
      {
         "id":"4245",
         "name":"Las Lomas",
         "code":"11",
         "label":"Las Lomas, Piura",
         "search":"las lomas piura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4237"
      },
      {
         "id":"4238",
         "name":"Piura",
         "code":"01",
         "label":"Piura, Piura",
         "search":"piura piura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4237"
      },
      {
         "id":"4246",
         "name":"Tambo Grande",
         "code":"14",
         "label":"Tambo Grande, Piura",
         "search":"tambo grande piura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4237"
      }
   ],
   "4302":[
      {
         "id":"4304",
         "name":"Bellavista de la Union",
         "code":"02",
         "label":"Bellavista de la Union, Sechura",
         "search":"bellavista de la union sechura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4302"
      },
      {
         "id":"4305",
         "name":"Bernal",
         "code":"03",
         "label":"Bernal, Sechura",
         "search":"bernal sechura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4302"
      },
      {
         "id":"4306",
         "name":"Cristo Nos Valga",
         "code":"04",
         "label":"Cristo Nos Valga, Sechura",
         "search":"cristo nos valga sechura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4302"
      },
      {
         "id":"4308",
         "name":"Rinconada Llicuar",
         "code":"06",
         "label":"Rinconada Llicuar, Sechura",
         "search":"rinconada llicuar sechura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4302"
      },
      {
         "id":"4303",
         "name":"Sechura",
         "code":"01",
         "label":"Sechura, Sechura",
         "search":"sechura sechura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4302"
      },
      {
         "id":"4307",
         "name":"Vice",
         "code":"05",
         "label":"Vice, Sechura",
         "search":"vice sechura",
         "children_count":"0",
         "level":"3",
         "parent_id":"4302"
      }
   ],
   "4286":[
      {
         "id":"4288",
         "name":"Bellavista",
         "code":"02",
         "label":"Bellavista, Sullana",
         "search":"bellavista sullana",
         "children_count":"0",
         "level":"3",
         "parent_id":"4286"
      },
      {
         "id":"4289",
         "name":"Ignacio Escudero",
         "code":"03",
         "label":"Ignacio Escudero, Sullana",
         "search":"ignacio escudero sullana",
         "children_count":"0",
         "level":"3",
         "parent_id":"4286"
      },
      {
         "id":"4290",
         "name":"Lancones",
         "code":"04",
         "label":"Lancones, Sullana",
         "search":"lancones sullana",
         "children_count":"0",
         "level":"3",
         "parent_id":"4286"
      },
      {
         "id":"4291",
         "name":"Marcavelica",
         "code":"05",
         "label":"Marcavelica, Sullana",
         "search":"marcavelica sullana",
         "children_count":"0",
         "level":"3",
         "parent_id":"4286"
      },
      {
         "id":"4292",
         "name":"Miguel Checa",
         "code":"06",
         "label":"Miguel Checa, Sullana",
         "search":"miguel checa sullana",
         "children_count":"0",
         "level":"3",
         "parent_id":"4286"
      },
      {
         "id":"4293",
         "name":"Querecotillo",
         "code":"07",
         "label":"Querecotillo, Sullana",
         "search":"querecotillo sullana",
         "children_count":"0",
         "level":"3",
         "parent_id":"4286"
      },
      {
         "id":"4294",
         "name":"Salitral",
         "code":"08",
         "label":"Salitral, Sullana",
         "search":"salitral sullana",
         "children_count":"0",
         "level":"3",
         "parent_id":"4286"
      },
      {
         "id":"4287",
         "name":"Sullana",
         "code":"01",
         "label":"Sullana, Sullana",
         "search":"sullana sullana",
         "children_count":"0",
         "level":"3",
         "parent_id":"4286"
      }
   ],
   "4295":[
      {
         "id":"4297",
         "name":"El Alto",
         "code":"02",
         "label":"El Alto, Talara",
         "search":"el alto talara",
         "children_count":"0",
         "level":"3",
         "parent_id":"4295"
      },
      {
         "id":"4298",
         "name":"La Brea",
         "code":"03",
         "label":"La Brea, Talara",
         "search":"la brea talara",
         "children_count":"0",
         "level":"3",
         "parent_id":"4295"
      },
      {
         "id":"4299",
         "name":"Lobitos",
         "code":"04",
         "label":"Lobitos, Talara",
         "search":"lobitos talara",
         "children_count":"0",
         "level":"3",
         "parent_id":"4295"
      },
      {
         "id":"4300",
         "name":"Los Organos",
         "code":"05",
         "label":"Los Organos, Talara",
         "search":"los organos talara",
         "children_count":"0",
         "level":"3",
         "parent_id":"4295"
      },
      {
         "id":"4301",
         "name":"Mancora",
         "code":"06",
         "label":"Mancora, Talara",
         "search":"mancora talara",
         "children_count":"0",
         "level":"3",
         "parent_id":"4295"
      },
      {
         "id":"4296",
         "name":"Pariqas",
         "code":"01",
         "label":"Pariqas, Talara",
         "search":"pariqas talara",
         "children_count":"0",
         "level":"3",
         "parent_id":"4295"
      }
   ],
   "4326":[
      {
         "id":"4328",
         "name":"Achaya",
         "code":"02",
         "label":"Achaya, Azangaro",
         "search":"achaya azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4329",
         "name":"Arapa",
         "code":"03",
         "label":"Arapa, Azangaro",
         "search":"arapa azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4330",
         "name":"Asillo",
         "code":"04",
         "label":"Asillo, Azangaro",
         "search":"asillo azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4327",
         "name":"Azangaro",
         "code":"01",
         "label":"Azangaro, Azangaro",
         "search":"azangaro azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4331",
         "name":"Caminaca",
         "code":"05",
         "label":"Caminaca, Azangaro",
         "search":"caminaca azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4332",
         "name":"Chupa",
         "code":"06",
         "label":"Chupa, Azangaro",
         "search":"chupa azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4333",
         "name":"Jose Domingo Choquehuanca",
         "code":"07",
         "label":"Jose Domingo Choquehuanca, Azangaro",
         "search":"jose domingo choquehuanca azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4334",
         "name":"Muqani",
         "code":"08",
         "label":"Muqani, Azangaro",
         "search":"muqani azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4335",
         "name":"Potoni",
         "code":"09",
         "label":"Potoni, Azangaro",
         "search":"potoni azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4336",
         "name":"Saman",
         "code":"10",
         "label":"Saman, Azangaro",
         "search":"saman azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4337",
         "name":"San Anton",
         "code":"11",
         "label":"San Anton, Azangaro",
         "search":"san anton azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4338",
         "name":"San Jose",
         "code":"12",
         "label":"San Jose, Azangaro",
         "search":"san jose azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4339",
         "name":"San Juan de Salinas",
         "code":"13",
         "label":"San Juan de Salinas, Azangaro",
         "search":"san juan de salinas azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4340",
         "name":"Santiago de Pupuja",
         "code":"14",
         "label":"Santiago de Pupuja, Azangaro",
         "search":"santiago de pupuja azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      },
      {
         "id":"4341",
         "name":"Tirapata",
         "code":"15",
         "label":"Tirapata, Azangaro",
         "search":"tirapata azangaro",
         "children_count":"0",
         "level":"3",
         "parent_id":"4326"
      }
   ],
   "4342":[
      {
         "id":"4344",
         "name":"Ajoyani",
         "code":"02",
         "label":"Ajoyani, Carabaya",
         "search":"ajoyani carabaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4342"
      },
      {
         "id":"4345",
         "name":"Ayapata",
         "code":"03",
         "label":"Ayapata, Carabaya",
         "search":"ayapata carabaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4342"
      },
      {
         "id":"4346",
         "name":"Coasa",
         "code":"04",
         "label":"Coasa, Carabaya",
         "search":"coasa carabaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4342"
      },
      {
         "id":"4347",
         "name":"Corani",
         "code":"05",
         "label":"Corani, Carabaya",
         "search":"corani carabaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4342"
      },
      {
         "id":"4348",
         "name":"Crucero",
         "code":"06",
         "label":"Crucero, Carabaya",
         "search":"crucero carabaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4342"
      },
      {
         "id":"4349",
         "name":"Ituata",
         "code":"07",
         "label":"Ituata, Carabaya",
         "search":"ituata carabaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4342"
      },
      {
         "id":"4343",
         "name":"Macusani",
         "code":"01",
         "label":"Macusani, Carabaya",
         "search":"macusani carabaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4342"
      },
      {
         "id":"4350",
         "name":"Ollachea",
         "code":"08",
         "label":"Ollachea, Carabaya",
         "search":"ollachea carabaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4342"
      },
      {
         "id":"4351",
         "name":"San Gaban",
         "code":"09",
         "label":"San Gaban, Carabaya",
         "search":"san gaban carabaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4342"
      },
      {
         "id":"4352",
         "name":"Usicayos",
         "code":"10",
         "label":"Usicayos, Carabaya",
         "search":"usicayos carabaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4342"
      }
   ],
   "4353":[
      {
         "id":"4355",
         "name":"Desaguadero",
         "code":"02",
         "label":"Desaguadero, Chucuito",
         "search":"desaguadero chucuito",
         "children_count":"0",
         "level":"3",
         "parent_id":"4353"
      },
      {
         "id":"4356",
         "name":"Huacullani",
         "code":"03",
         "label":"Huacullani, Chucuito",
         "search":"huacullani chucuito",
         "children_count":"0",
         "level":"3",
         "parent_id":"4353"
      },
      {
         "id":"4354",
         "name":"Juli",
         "code":"01",
         "label":"Juli, Chucuito",
         "search":"juli chucuito",
         "children_count":"0",
         "level":"3",
         "parent_id":"4353"
      },
      {
         "id":"4357",
         "name":"Kelluyo",
         "code":"04",
         "label":"Kelluyo, Chucuito",
         "search":"kelluyo chucuito",
         "children_count":"0",
         "level":"3",
         "parent_id":"4353"
      },
      {
         "id":"4358",
         "name":"Pisacoma",
         "code":"05",
         "label":"Pisacoma, Chucuito",
         "search":"pisacoma chucuito",
         "children_count":"0",
         "level":"3",
         "parent_id":"4353"
      },
      {
         "id":"4359",
         "name":"Pomata",
         "code":"06",
         "label":"Pomata, Chucuito",
         "search":"pomata chucuito",
         "children_count":"0",
         "level":"3",
         "parent_id":"4353"
      },
      {
         "id":"4360",
         "name":"Zepita",
         "code":"07",
         "label":"Zepita, Chucuito",
         "search":"zepita chucuito",
         "children_count":"0",
         "level":"3",
         "parent_id":"4353"
      }
   ],
   "4361":[
      {
         "id":"4363",
         "name":"Capazo",
         "code":"02",
         "label":"Capazo, El Collao",
         "search":"capazo el collao",
         "children_count":"0",
         "level":"3",
         "parent_id":"4361"
      },
      {
         "id":"4366",
         "name":"Conduriri",
         "code":"05",
         "label":"Conduriri, El Collao",
         "search":"conduriri el collao",
         "children_count":"0",
         "level":"3",
         "parent_id":"4361"
      },
      {
         "id":"4362",
         "name":"Ilave",
         "code":"01",
         "label":"Ilave, El Collao",
         "search":"ilave el collao",
         "children_count":"0",
         "level":"3",
         "parent_id":"4361"
      },
      {
         "id":"4364",
         "name":"Pilcuyo",
         "code":"03",
         "label":"Pilcuyo, El Collao",
         "search":"pilcuyo el collao",
         "children_count":"0",
         "level":"3",
         "parent_id":"4361"
      },
      {
         "id":"4365",
         "name":"Santa Rosa",
         "code":"04",
         "label":"Santa Rosa, El Collao",
         "search":"santa rosa el collao",
         "children_count":"0",
         "level":"3",
         "parent_id":"4361"
      }
   ],
   "4367":[
      {
         "id":"4369",
         "name":"Cojata",
         "code":"02",
         "label":"Cojata, Huancane",
         "search":"cojata huancane",
         "children_count":"0",
         "level":"3",
         "parent_id":"4367"
      },
      {
         "id":"4368",
         "name":"Huancane",
         "code":"01",
         "label":"Huancane, Huancane",
         "search":"huancane huancane",
         "children_count":"0",
         "level":"3",
         "parent_id":"4367"
      },
      {
         "id":"4370",
         "name":"Huatasani",
         "code":"03",
         "label":"Huatasani, Huancane",
         "search":"huatasani huancane",
         "children_count":"0",
         "level":"3",
         "parent_id":"4367"
      },
      {
         "id":"4371",
         "name":"Inchupalla",
         "code":"04",
         "label":"Inchupalla, Huancane",
         "search":"inchupalla huancane",
         "children_count":"0",
         "level":"3",
         "parent_id":"4367"
      },
      {
         "id":"4372",
         "name":"Pusi",
         "code":"05",
         "label":"Pusi, Huancane",
         "search":"pusi huancane",
         "children_count":"0",
         "level":"3",
         "parent_id":"4367"
      },
      {
         "id":"4373",
         "name":"Rosaspata",
         "code":"06",
         "label":"Rosaspata, Huancane",
         "search":"rosaspata huancane",
         "children_count":"0",
         "level":"3",
         "parent_id":"4367"
      },
      {
         "id":"4374",
         "name":"Taraco",
         "code":"07",
         "label":"Taraco, Huancane",
         "search":"taraco huancane",
         "children_count":"0",
         "level":"3",
         "parent_id":"4367"
      },
      {
         "id":"4375",
         "name":"Vilque Chico",
         "code":"08",
         "label":"Vilque Chico, Huancane",
         "search":"vilque chico huancane",
         "children_count":"0",
         "level":"3",
         "parent_id":"4367"
      }
   ],
   "4376":[
      {
         "id":"4378",
         "name":"Cabanilla",
         "code":"02",
         "label":"Cabanilla, Lampa",
         "search":"cabanilla lampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4376"
      },
      {
         "id":"4379",
         "name":"Calapuja",
         "code":"03",
         "label":"Calapuja, Lampa",
         "search":"calapuja lampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4376"
      },
      {
         "id":"4377",
         "name":"Lampa",
         "code":"01",
         "label":"Lampa, Lampa",
         "search":"lampa lampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4376"
      },
      {
         "id":"4380",
         "name":"Nicasio",
         "code":"04",
         "label":"Nicasio, Lampa",
         "search":"nicasio lampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4376"
      },
      {
         "id":"4381",
         "name":"Ocuviri",
         "code":"05",
         "label":"Ocuviri, Lampa",
         "search":"ocuviri lampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4376"
      },
      {
         "id":"4382",
         "name":"Palca",
         "code":"06",
         "label":"Palca, Lampa",
         "search":"palca lampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4376"
      },
      {
         "id":"4383",
         "name":"Paratia",
         "code":"07",
         "label":"Paratia, Lampa",
         "search":"paratia lampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4376"
      },
      {
         "id":"4384",
         "name":"Pucara",
         "code":"08",
         "label":"Pucara, Lampa",
         "search":"pucara lampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4376"
      },
      {
         "id":"4385",
         "name":"Santa Lucia",
         "code":"09",
         "label":"Santa Lucia, Lampa",
         "search":"santa lucia lampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4376"
      },
      {
         "id":"4386",
         "name":"Vilavila",
         "code":"10",
         "label":"Vilavila, Lampa",
         "search":"vilavila lampa",
         "children_count":"0",
         "level":"3",
         "parent_id":"4376"
      }
   ],
   "4387":[
      {
         "id":"4389",
         "name":"Antauta",
         "code":"02",
         "label":"Antauta, Melgar",
         "search":"antauta melgar",
         "children_count":"0",
         "level":"3",
         "parent_id":"4387"
      },
      {
         "id":"4388",
         "name":"Ayaviri",
         "code":"01",
         "label":"Ayaviri, Melgar",
         "search":"ayaviri melgar",
         "children_count":"0",
         "level":"3",
         "parent_id":"4387"
      },
      {
         "id":"4390",
         "name":"Cupi",
         "code":"03",
         "label":"Cupi, Melgar",
         "search":"cupi melgar",
         "children_count":"0",
         "level":"3",
         "parent_id":"4387"
      },
      {
         "id":"4391",
         "name":"Llalli",
         "code":"04",
         "label":"Llalli, Melgar",
         "search":"llalli melgar",
         "children_count":"0",
         "level":"3",
         "parent_id":"4387"
      },
      {
         "id":"4392",
         "name":"Macari",
         "code":"05",
         "label":"Macari, Melgar",
         "search":"macari melgar",
         "children_count":"0",
         "level":"3",
         "parent_id":"4387"
      },
      {
         "id":"4393",
         "name":"Nuqoa",
         "code":"06",
         "label":"Nuqoa, Melgar",
         "search":"nuqoa melgar",
         "children_count":"0",
         "level":"3",
         "parent_id":"4387"
      },
      {
         "id":"4394",
         "name":"Orurillo",
         "code":"07",
         "label":"Orurillo, Melgar",
         "search":"orurillo melgar",
         "children_count":"0",
         "level":"3",
         "parent_id":"4387"
      },
      {
         "id":"4395",
         "name":"Santa Rosa",
         "code":"08",
         "label":"Santa Rosa, Melgar",
         "search":"santa rosa melgar",
         "children_count":"0",
         "level":"3",
         "parent_id":"4387"
      },
      {
         "id":"4396",
         "name":"Umachiri",
         "code":"09",
         "label":"Umachiri, Melgar",
         "search":"umachiri melgar",
         "children_count":"0",
         "level":"3",
         "parent_id":"4387"
      }
   ],
   "4397":[
      {
         "id":"4399",
         "name":"Conima",
         "code":"02",
         "label":"Conima, Moho",
         "search":"conima moho",
         "children_count":"0",
         "level":"3",
         "parent_id":"4397"
      },
      {
         "id":"4400",
         "name":"Huayrapata",
         "code":"03",
         "label":"Huayrapata, Moho",
         "search":"huayrapata moho",
         "children_count":"0",
         "level":"3",
         "parent_id":"4397"
      },
      {
         "id":"4398",
         "name":"Moho",
         "code":"01",
         "label":"Moho, Moho",
         "search":"moho moho",
         "children_count":"0",
         "level":"3",
         "parent_id":"4397"
      },
      {
         "id":"4401",
         "name":"Tilali",
         "code":"04",
         "label":"Tilali, Moho",
         "search":"tilali moho",
         "children_count":"0",
         "level":"3",
         "parent_id":"4397"
      }
   ],
   "4310":[
      {
         "id":"4312",
         "name":"Acora",
         "code":"02",
         "label":"Acora, Puno",
         "search":"acora puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4313",
         "name":"Amantani",
         "code":"03",
         "label":"Amantani, Puno",
         "search":"amantani puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4314",
         "name":"Atuncolla",
         "code":"04",
         "label":"Atuncolla, Puno",
         "search":"atuncolla puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4315",
         "name":"Capachica",
         "code":"05",
         "label":"Capachica, Puno",
         "search":"capachica puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4316",
         "name":"Chucuito",
         "code":"06",
         "label":"Chucuito, Puno",
         "search":"chucuito puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4317",
         "name":"Coata",
         "code":"07",
         "label":"Coata, Puno",
         "search":"coata puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4318",
         "name":"Huata",
         "code":"08",
         "label":"Huata, Puno",
         "search":"huata puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4319",
         "name":"Maqazo",
         "code":"09",
         "label":"Maqazo, Puno",
         "search":"maqazo puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4320",
         "name":"Paucarcolla",
         "code":"10",
         "label":"Paucarcolla, Puno",
         "search":"paucarcolla puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4321",
         "name":"Pichacani",
         "code":"11",
         "label":"Pichacani, Puno",
         "search":"pichacani puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4322",
         "name":"Plateria",
         "code":"12",
         "label":"Plateria, Puno",
         "search":"plateria puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4311",
         "name":"Puno",
         "code":"01",
         "label":"Puno, Puno",
         "search":"puno puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4323",
         "name":"San Antonio",
         "code":"13",
         "label":"San Antonio, Puno",
         "search":"san antonio puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4324",
         "name":"Tiquillaca",
         "code":"14",
         "label":"Tiquillaca, Puno",
         "search":"tiquillaca puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      },
      {
         "id":"4325",
         "name":"Vilque",
         "code":"15",
         "label":"Vilque, Puno",
         "search":"vilque puno",
         "children_count":"0",
         "level":"3",
         "parent_id":"4310"
      }
   ],
   "4402":[
      {
         "id":"4404",
         "name":"Ananea",
         "code":"02",
         "label":"Ananea, San Antonio de Putina",
         "search":"ananea san antonio de putina",
         "children_count":"0",
         "level":"3",
         "parent_id":"4402"
      },
      {
         "id":"4405",
         "name":"Pedro Vilca Apaza",
         "code":"03",
         "label":"Pedro Vilca Apaza, San Antonio de Putina",
         "search":"pedro vilca apaza san antonio de putina",
         "children_count":"0",
         "level":"3",
         "parent_id":"4402"
      },
      {
         "id":"4403",
         "name":"Putina",
         "code":"01",
         "label":"Putina, San Antonio de Putina",
         "search":"putina san antonio de putina",
         "children_count":"0",
         "level":"3",
         "parent_id":"4402"
      },
      {
         "id":"4406",
         "name":"Quilcapuncu",
         "code":"04",
         "label":"Quilcapuncu, San Antonio de Putina",
         "search":"quilcapuncu san antonio de putina",
         "children_count":"0",
         "level":"3",
         "parent_id":"4402"
      },
      {
         "id":"4407",
         "name":"Sina",
         "code":"05",
         "label":"Sina, San Antonio de Putina",
         "search":"sina san antonio de putina",
         "children_count":"0",
         "level":"3",
         "parent_id":"4402"
      }
   ],
   "4408":[
      {
         "id":"4410",
         "name":"Cabana",
         "code":"02",
         "label":"Cabana, San Roman",
         "search":"cabana san roman",
         "children_count":"0",
         "level":"3",
         "parent_id":"4408"
      },
      {
         "id":"4411",
         "name":"Cabanillas",
         "code":"03",
         "label":"Cabanillas, San Roman",
         "search":"cabanillas san roman",
         "children_count":"0",
         "level":"3",
         "parent_id":"4408"
      },
      {
         "id":"4412",
         "name":"Caracoto",
         "code":"04",
         "label":"Caracoto, San Roman",
         "search":"caracoto san roman",
         "children_count":"0",
         "level":"3",
         "parent_id":"4408"
      },
      {
         "id":"4409",
         "name":"Juliaca",
         "code":"01",
         "label":"Juliaca, San Roman",
         "search":"juliaca san roman",
         "children_count":"0",
         "level":"3",
         "parent_id":"4408"
      }
   ],
   "4413":[
      {
         "id":"4422",
         "name":"Alto Inambari",
         "code":"09",
         "label":"Alto Inambari, Sandia",
         "search":"alto inambari sandia",
         "children_count":"0",
         "level":"3",
         "parent_id":"4413"
      },
      {
         "id":"4415",
         "name":"Cuyocuyo",
         "code":"02",
         "label":"Cuyocuyo, Sandia",
         "search":"cuyocuyo sandia",
         "children_count":"0",
         "level":"3",
         "parent_id":"4413"
      },
      {
         "id":"4416",
         "name":"Limbani",
         "code":"03",
         "label":"Limbani, Sandia",
         "search":"limbani sandia",
         "children_count":"0",
         "level":"3",
         "parent_id":"4413"
      },
      {
         "id":"4417",
         "name":"Patambuco",
         "code":"04",
         "label":"Patambuco, Sandia",
         "search":"patambuco sandia",
         "children_count":"0",
         "level":"3",
         "parent_id":"4413"
      },
      {
         "id":"4418",
         "name":"Phara",
         "code":"05",
         "label":"Phara, Sandia",
         "search":"phara sandia",
         "children_count":"0",
         "level":"3",
         "parent_id":"4413"
      },
      {
         "id":"4419",
         "name":"Quiaca",
         "code":"06",
         "label":"Quiaca, Sandia",
         "search":"quiaca sandia",
         "children_count":"0",
         "level":"3",
         "parent_id":"4413"
      },
      {
         "id":"4420",
         "name":"San Juan del Oro",
         "code":"07",
         "label":"San Juan del Oro, Sandia",
         "search":"san juan del oro sandia",
         "children_count":"0",
         "level":"3",
         "parent_id":"4413"
      },
      {
         "id":"4414",
         "name":"Sandia",
         "code":"01",
         "label":"Sandia, Sandia",
         "search":"sandia sandia",
         "children_count":"0",
         "level":"3",
         "parent_id":"4413"
      },
      {
         "id":"4421",
         "name":"Yanahuaya",
         "code":"08",
         "label":"Yanahuaya, Sandia",
         "search":"yanahuaya sandia",
         "children_count":"0",
         "level":"3",
         "parent_id":"4413"
      }
   ],
   "4423":[
      {
         "id":"4425",
         "name":"Anapia",
         "code":"02",
         "label":"Anapia, Yunguyo",
         "search":"anapia yunguyo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4423"
      },
      {
         "id":"4426",
         "name":"Copani",
         "code":"03",
         "label":"Copani, Yunguyo",
         "search":"copani yunguyo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4423"
      },
      {
         "id":"4427",
         "name":"Cuturapi",
         "code":"04",
         "label":"Cuturapi, Yunguyo",
         "search":"cuturapi yunguyo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4423"
      },
      {
         "id":"4428",
         "name":"Ollaraya",
         "code":"05",
         "label":"Ollaraya, Yunguyo",
         "search":"ollaraya yunguyo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4423"
      },
      {
         "id":"4429",
         "name":"Tinicachi",
         "code":"06",
         "label":"Tinicachi, Yunguyo",
         "search":"tinicachi yunguyo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4423"
      },
      {
         "id":"4430",
         "name":"Unicachi",
         "code":"07",
         "label":"Unicachi, Yunguyo",
         "search":"unicachi yunguyo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4423"
      },
      {
         "id":"4424",
         "name":"Yunguyo",
         "code":"01",
         "label":"Yunguyo, Yunguyo",
         "search":"yunguyo yunguyo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4423"
      }
   ],
   "4439":[
      {
         "id":"4441",
         "name":"Alto Biavo",
         "code":"02",
         "label":"Alto Biavo, Bellavista",
         "search":"alto biavo bellavista",
         "children_count":"0",
         "level":"3",
         "parent_id":"4439"
      },
      {
         "id":"4442",
         "name":"Bajo Biavo",
         "code":"03",
         "label":"Bajo Biavo, Bellavista",
         "search":"bajo biavo bellavista",
         "children_count":"0",
         "level":"3",
         "parent_id":"4439"
      },
      {
         "id":"4440",
         "name":"Bellavista",
         "code":"01",
         "label":"Bellavista, Bellavista",
         "search":"bellavista bellavista",
         "children_count":"0",
         "level":"3",
         "parent_id":"4439"
      },
      {
         "id":"4443",
         "name":"Huallaga",
         "code":"04",
         "label":"Huallaga, Bellavista",
         "search":"huallaga bellavista",
         "children_count":"0",
         "level":"3",
         "parent_id":"4439"
      },
      {
         "id":"4444",
         "name":"San Pablo",
         "code":"05",
         "label":"San Pablo, Bellavista",
         "search":"san pablo bellavista",
         "children_count":"0",
         "level":"3",
         "parent_id":"4439"
      },
      {
         "id":"4445",
         "name":"San Rafael",
         "code":"06",
         "label":"San Rafael, Bellavista",
         "search":"san rafael bellavista",
         "children_count":"0",
         "level":"3",
         "parent_id":"4439"
      }
   ],
   "4446":[
      {
         "id":"4448",
         "name":"Agua Blanca",
         "code":"02",
         "label":"Agua Blanca, El Dorado",
         "search":"agua blanca el dorado",
         "children_count":"0",
         "level":"3",
         "parent_id":"4446"
      },
      {
         "id":"4447",
         "name":"San Jose de Sisa",
         "code":"01",
         "label":"San Jose de Sisa, El Dorado",
         "search":"san jose de sisa el dorado",
         "children_count":"0",
         "level":"3",
         "parent_id":"4446"
      },
      {
         "id":"4449",
         "name":"San Martin",
         "code":"03",
         "label":"San Martin, El Dorado",
         "search":"san martin el dorado",
         "children_count":"0",
         "level":"3",
         "parent_id":"4446"
      },
      {
         "id":"4450",
         "name":"Santa Rosa",
         "code":"04",
         "label":"Santa Rosa, El Dorado",
         "search":"santa rosa el dorado",
         "children_count":"0",
         "level":"3",
         "parent_id":"4446"
      },
      {
         "id":"4451",
         "name":"Shatoja",
         "code":"05",
         "label":"Shatoja, El Dorado",
         "search":"shatoja el dorado",
         "children_count":"0",
         "level":"3",
         "parent_id":"4446"
      }
   ],
   "4452":[
      {
         "id":"4454",
         "name":"Alto Saposoa",
         "code":"02",
         "label":"Alto Saposoa, Huallaga",
         "search":"alto saposoa huallaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"4452"
      },
      {
         "id":"4455",
         "name":"El Eslabon",
         "code":"03",
         "label":"El Eslabon, Huallaga",
         "search":"el eslabon huallaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"4452"
      },
      {
         "id":"4456",
         "name":"Piscoyacu",
         "code":"04",
         "label":"Piscoyacu, Huallaga",
         "search":"piscoyacu huallaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"4452"
      },
      {
         "id":"4457",
         "name":"Sacanche",
         "code":"05",
         "label":"Sacanche, Huallaga",
         "search":"sacanche huallaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"4452"
      },
      {
         "id":"4453",
         "name":"Saposoa",
         "code":"01",
         "label":"Saposoa, Huallaga",
         "search":"saposoa huallaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"4452"
      },
      {
         "id":"4458",
         "name":"Tingo de Saposoa",
         "code":"06",
         "label":"Tingo de Saposoa, Huallaga",
         "search":"tingo de saposoa huallaga",
         "children_count":"0",
         "level":"3",
         "parent_id":"4452"
      }
   ],
   "4459":[
      {
         "id":"4461",
         "name":"Alonso de Alvarado",
         "code":"02",
         "label":"Alonso de Alvarado, Lamas",
         "search":"alonso de alvarado lamas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4459"
      },
      {
         "id":"4462",
         "name":"Barranquita",
         "code":"03",
         "label":"Barranquita, Lamas",
         "search":"barranquita lamas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4459"
      },
      {
         "id":"4463",
         "name":"Caynarachi",
         "code":"04",
         "label":"Caynarachi, Lamas",
         "search":"caynarachi lamas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4459"
      },
      {
         "id":"4464",
         "name":"Cuqumbuqui",
         "code":"05",
         "label":"Cuqumbuqui, Lamas",
         "search":"cuqumbuqui lamas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4459"
      },
      {
         "id":"4460",
         "name":"Lamas",
         "code":"01",
         "label":"Lamas, Lamas",
         "search":"lamas lamas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4459"
      },
      {
         "id":"4465",
         "name":"Pinto Recodo",
         "code":"06",
         "label":"Pinto Recodo, Lamas",
         "search":"pinto recodo lamas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4459"
      },
      {
         "id":"4466",
         "name":"Rumisapa",
         "code":"07",
         "label":"Rumisapa, Lamas",
         "search":"rumisapa lamas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4459"
      },
      {
         "id":"4467",
         "name":"San Roque de Cumbaza",
         "code":"08",
         "label":"San Roque de Cumbaza, Lamas",
         "search":"san roque de cumbaza lamas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4459"
      },
      {
         "id":"4468",
         "name":"Shanao",
         "code":"09",
         "label":"Shanao, Lamas",
         "search":"shanao lamas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4459"
      },
      {
         "id":"4469",
         "name":"Tabalosos",
         "code":"10",
         "label":"Tabalosos, Lamas",
         "search":"tabalosos lamas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4459"
      },
      {
         "id":"4470",
         "name":"Zapatero",
         "code":"11",
         "label":"Zapatero, Lamas",
         "search":"zapatero lamas",
         "children_count":"0",
         "level":"3",
         "parent_id":"4459"
      }
   ],
   "4471":[
      {
         "id":"4473",
         "name":"Campanilla",
         "code":"02",
         "label":"Campanilla, Mariscal Caceres",
         "search":"campanilla mariscal caceres",
         "children_count":"0",
         "level":"3",
         "parent_id":"4471"
      },
      {
         "id":"4474",
         "name":"Huicungo",
         "code":"03",
         "label":"Huicungo, Mariscal Caceres",
         "search":"huicungo mariscal caceres",
         "children_count":"0",
         "level":"3",
         "parent_id":"4471"
      },
      {
         "id":"4472",
         "name":"Juanjui",
         "code":"01",
         "label":"Juanjui, Mariscal Caceres",
         "search":"juanjui mariscal caceres",
         "children_count":"0",
         "level":"3",
         "parent_id":"4471"
      },
      {
         "id":"4475",
         "name":"Pachiza",
         "code":"04",
         "label":"Pachiza, Mariscal Caceres",
         "search":"pachiza mariscal caceres",
         "children_count":"0",
         "level":"3",
         "parent_id":"4471"
      },
      {
         "id":"4476",
         "name":"Pajarillo",
         "code":"05",
         "label":"Pajarillo, Mariscal Caceres",
         "search":"pajarillo mariscal caceres",
         "children_count":"0",
         "level":"3",
         "parent_id":"4471"
      }
   ],
   "4432":[
      {
         "id":"4434",
         "name":"Calzada",
         "code":"02",
         "label":"Calzada, Moyobamba",
         "search":"calzada moyobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4432"
      },
      {
         "id":"4435",
         "name":"Habana",
         "code":"03",
         "label":"Habana, Moyobamba",
         "search":"habana moyobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4432"
      },
      {
         "id":"4436",
         "name":"Jepelacio",
         "code":"04",
         "label":"Jepelacio, Moyobamba",
         "search":"jepelacio moyobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4432"
      },
      {
         "id":"4433",
         "name":"Moyobamba",
         "code":"01",
         "label":"Moyobamba, Moyobamba",
         "search":"moyobamba moyobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4432"
      },
      {
         "id":"4437",
         "name":"Soritor",
         "code":"05",
         "label":"Soritor, Moyobamba",
         "search":"soritor moyobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4432"
      },
      {
         "id":"4438",
         "name":"Yantalo",
         "code":"06",
         "label":"Yantalo, Moyobamba",
         "search":"yantalo moyobamba",
         "children_count":"0",
         "level":"3",
         "parent_id":"4432"
      }
   ],
   "4477":[
      {
         "id":"4479",
         "name":"Buenos Aires",
         "code":"02",
         "label":"Buenos Aires, Picota",
         "search":"buenos aires picota",
         "children_count":"0",
         "level":"3",
         "parent_id":"4477"
      },
      {
         "id":"4480",
         "name":"Caspisapa",
         "code":"03",
         "label":"Caspisapa, Picota",
         "search":"caspisapa picota",
         "children_count":"0",
         "level":"3",
         "parent_id":"4477"
      },
      {
         "id":"4478",
         "name":"Picota",
         "code":"01",
         "label":"Picota, Picota",
         "search":"picota picota",
         "children_count":"0",
         "level":"3",
         "parent_id":"4477"
      },
      {
         "id":"4481",
         "name":"Pilluana",
         "code":"04",
         "label":"Pilluana, Picota",
         "search":"pilluana picota",
         "children_count":"0",
         "level":"3",
         "parent_id":"4477"
      },
      {
         "id":"4482",
         "name":"Pucacaca",
         "code":"05",
         "label":"Pucacaca, Picota",
         "search":"pucacaca picota",
         "children_count":"0",
         "level":"3",
         "parent_id":"4477"
      },
      {
         "id":"4483",
         "name":"San Cristobal",
         "code":"06",
         "label":"San Cristobal, Picota",
         "search":"san cristobal picota",
         "children_count":"0",
         "level":"3",
         "parent_id":"4477"
      },
      {
         "id":"4484",
         "name":"San Hilarion",
         "code":"07",
         "label":"San Hilarion, Picota",
         "search":"san hilarion picota",
         "children_count":"0",
         "level":"3",
         "parent_id":"4477"
      },
      {
         "id":"4485",
         "name":"Shamboyacu",
         "code":"08",
         "label":"Shamboyacu, Picota",
         "search":"shamboyacu picota",
         "children_count":"0",
         "level":"3",
         "parent_id":"4477"
      },
      {
         "id":"4486",
         "name":"Tingo de Ponasa",
         "code":"09",
         "label":"Tingo de Ponasa, Picota",
         "search":"tingo de ponasa picota",
         "children_count":"0",
         "level":"3",
         "parent_id":"4477"
      },
      {
         "id":"4487",
         "name":"Tres Unidos",
         "code":"10",
         "label":"Tres Unidos, Picota",
         "search":"tres unidos picota",
         "children_count":"0",
         "level":"3",
         "parent_id":"4477"
      }
   ],
   "4488":[
      {
         "id":"4490",
         "name":"Awajun",
         "code":"02",
         "label":"Awajun, Rioja",
         "search":"awajun rioja",
         "children_count":"0",
         "level":"3",
         "parent_id":"4488"
      },
      {
         "id":"4491",
         "name":"Elias Soplin Vargas",
         "code":"03",
         "label":"Elias Soplin Vargas, Rioja",
         "search":"elias soplin vargas rioja",
         "children_count":"0",
         "level":"3",
         "parent_id":"4488"
      },
      {
         "id":"4492",
         "name":"Nueva Cajamarca",
         "code":"04",
         "label":"Nueva Cajamarca, Rioja",
         "search":"nueva cajamarca rioja",
         "children_count":"0",
         "level":"3",
         "parent_id":"4488"
      },
      {
         "id":"4493",
         "name":"Pardo Miguel",
         "code":"05",
         "label":"Pardo Miguel, Rioja",
         "search":"pardo miguel rioja",
         "children_count":"0",
         "level":"3",
         "parent_id":"4488"
      },
      {
         "id":"4494",
         "name":"Posic",
         "code":"06",
         "label":"Posic, Rioja",
         "search":"posic rioja",
         "children_count":"0",
         "level":"3",
         "parent_id":"4488"
      },
      {
         "id":"4489",
         "name":"Rioja",
         "code":"01",
         "label":"Rioja, Rioja",
         "search":"rioja rioja",
         "children_count":"0",
         "level":"3",
         "parent_id":"4488"
      },
      {
         "id":"4495",
         "name":"San Fernando",
         "code":"07",
         "label":"San Fernando, Rioja",
         "search":"san fernando rioja",
         "children_count":"0",
         "level":"3",
         "parent_id":"4488"
      },
      {
         "id":"4496",
         "name":"Yorongos",
         "code":"08",
         "label":"Yorongos, Rioja",
         "search":"yorongos rioja",
         "children_count":"0",
         "level":"3",
         "parent_id":"4488"
      },
      {
         "id":"4497",
         "name":"Yuracyacu",
         "code":"09",
         "label":"Yuracyacu, Rioja",
         "search":"yuracyacu rioja",
         "children_count":"0",
         "level":"3",
         "parent_id":"4488"
      }
   ],
   "4498":[
      {
         "id":"4500",
         "name":"Alberto Leveau",
         "code":"02",
         "label":"Alberto Leveau, San Martin",
         "search":"alberto leveau san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4501",
         "name":"Cacatachi",
         "code":"03",
         "label":"Cacatachi, San Martin",
         "search":"cacatachi san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4502",
         "name":"Chazuta",
         "code":"04",
         "label":"Chazuta, San Martin",
         "search":"chazuta san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4503",
         "name":"Chipurana",
         "code":"05",
         "label":"Chipurana, San Martin",
         "search":"chipurana san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4504",
         "name":"El Porvenir",
         "code":"06",
         "label":"El Porvenir, San Martin",
         "search":"el porvenir san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4505",
         "name":"Huimbayoc",
         "code":"07",
         "label":"Huimbayoc, San Martin",
         "search":"huimbayoc san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4506",
         "name":"Juan Guerra",
         "code":"08",
         "label":"Juan Guerra, San Martin",
         "search":"juan guerra san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4507",
         "name":"La Banda de Shilcayo",
         "code":"09",
         "label":"La Banda de Shilcayo, San Martin",
         "search":"la banda de shilcayo san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4508",
         "name":"Morales",
         "code":"10",
         "label":"Morales, San Martin",
         "search":"morales san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4509",
         "name":"Papaplaya",
         "code":"11",
         "label":"Papaplaya, San Martin",
         "search":"papaplaya san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4510",
         "name":"San Antonio",
         "code":"12",
         "label":"San Antonio, San Martin",
         "search":"san antonio san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4511",
         "name":"Sauce",
         "code":"13",
         "label":"Sauce, San Martin",
         "search":"sauce san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4512",
         "name":"Shapaja",
         "code":"14",
         "label":"Shapaja, San Martin",
         "search":"shapaja san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      },
      {
         "id":"4499",
         "name":"Tarapoto",
         "code":"01",
         "label":"Tarapoto, San Martin",
         "search":"tarapoto san martin",
         "children_count":"0",
         "level":"3",
         "parent_id":"4498"
      }
   ],
   "4513":[
      {
         "id":"4515",
         "name":"Nuevo Progreso",
         "code":"02",
         "label":"Nuevo Progreso, Tocache",
         "search":"nuevo progreso tocache",
         "children_count":"0",
         "level":"3",
         "parent_id":"4513"
      },
      {
         "id":"4516",
         "name":"Polvora",
         "code":"03",
         "label":"Polvora, Tocache",
         "search":"polvora tocache",
         "children_count":"0",
         "level":"3",
         "parent_id":"4513"
      },
      {
         "id":"4517",
         "name":"Shunte",
         "code":"04",
         "label":"Shunte, Tocache",
         "search":"shunte tocache",
         "children_count":"0",
         "level":"3",
         "parent_id":"4513"
      },
      {
         "id":"4514",
         "name":"Tocache",
         "code":"01",
         "label":"Tocache, Tocache",
         "search":"tocache tocache",
         "children_count":"0",
         "level":"3",
         "parent_id":"4513"
      },
      {
         "id":"4518",
         "name":"Uchiza",
         "code":"05",
         "label":"Uchiza, Tocache",
         "search":"uchiza tocache",
         "children_count":"0",
         "level":"3",
         "parent_id":"4513"
      }
   ],
   "4531":[
      {
         "id":"4533",
         "name":"Cairani",
         "code":"02",
         "label":"Cairani, Candarave",
         "search":"cairani candarave",
         "children_count":"0",
         "level":"3",
         "parent_id":"4531"
      },
      {
         "id":"4534",
         "name":"Camilaca",
         "code":"03",
         "label":"Camilaca, Candarave",
         "search":"camilaca candarave",
         "children_count":"0",
         "level":"3",
         "parent_id":"4531"
      },
      {
         "id":"4532",
         "name":"Candarave",
         "code":"01",
         "label":"Candarave, Candarave",
         "search":"candarave candarave",
         "children_count":"0",
         "level":"3",
         "parent_id":"4531"
      },
      {
         "id":"4535",
         "name":"Curibaya",
         "code":"04",
         "label":"Curibaya, Candarave",
         "search":"curibaya candarave",
         "children_count":"0",
         "level":"3",
         "parent_id":"4531"
      },
      {
         "id":"4536",
         "name":"Huanuara",
         "code":"05",
         "label":"Huanuara, Candarave",
         "search":"huanuara candarave",
         "children_count":"0",
         "level":"3",
         "parent_id":"4531"
      },
      {
         "id":"4537",
         "name":"Quilahuani",
         "code":"06",
         "label":"Quilahuani, Candarave",
         "search":"quilahuani candarave",
         "children_count":"0",
         "level":"3",
         "parent_id":"4531"
      }
   ],
   "4538":[
      {
         "id":"4540",
         "name":"Ilabaya",
         "code":"02",
         "label":"Ilabaya, Jorge Basadre",
         "search":"ilabaya jorge basadre",
         "children_count":"0",
         "level":"3",
         "parent_id":"4538"
      },
      {
         "id":"4541",
         "name":"Ite",
         "code":"03",
         "label":"Ite, Jorge Basadre",
         "search":"ite jorge basadre",
         "children_count":"0",
         "level":"3",
         "parent_id":"4538"
      },
      {
         "id":"4539",
         "name":"Locumba",
         "code":"01",
         "label":"Locumba, Jorge Basadre",
         "search":"locumba jorge basadre",
         "children_count":"0",
         "level":"3",
         "parent_id":"4538"
      }
   ],
   "4520":[
      {
         "id":"4522",
         "name":"Alto de la Alianza",
         "code":"02",
         "label":"Alto de la Alianza, Tacna",
         "search":"alto de la alianza tacna",
         "children_count":"0",
         "level":"3",
         "parent_id":"4520"
      },
      {
         "id":"4523",
         "name":"Calana",
         "code":"03",
         "label":"Calana, Tacna",
         "search":"calana tacna",
         "children_count":"0",
         "level":"3",
         "parent_id":"4520"
      },
      {
         "id":"4524",
         "name":"Ciudad Nueva",
         "code":"04",
         "label":"Ciudad Nueva, Tacna",
         "search":"ciudad nueva tacna",
         "children_count":"0",
         "level":"3",
         "parent_id":"4520"
      },
      {
         "id":"4530",
         "name":"Cor Gregorio Albarrac\u00edn",
         "code":"10",
         "label":"Cor Gregorio Albarrac\u00edn, Tacna",
         "search":"cor gregorio albarrac\u00edn tacna",
         "children_count":"0",
         "level":"3",
         "parent_id":"4520"
      },
      {
         "id":"4525",
         "name":"Inclan",
         "code":"05",
         "label":"Inclan, Tacna",
         "search":"inclan tacna",
         "children_count":"0",
         "level":"3",
         "parent_id":"4520"
      },
      {
         "id":"4526",
         "name":"Pachia",
         "code":"06",
         "label":"Pachia, Tacna",
         "search":"pachia tacna",
         "children_count":"0",
         "level":"3",
         "parent_id":"4520"
      },
      {
         "id":"4527",
         "name":"Palca",
         "code":"07",
         "label":"Palca, Tacna",
         "search":"palca tacna",
         "children_count":"0",
         "level":"3",
         "parent_id":"4520"
      },
      {
         "id":"4528",
         "name":"Pocollay",
         "code":"08",
         "label":"Pocollay, Tacna",
         "search":"pocollay tacna",
         "children_count":"0",
         "level":"3",
         "parent_id":"4520"
      },
      {
         "id":"4529",
         "name":"Sama",
         "code":"09",
         "label":"Sama, Tacna",
         "search":"sama tacna",
         "children_count":"0",
         "level":"3",
         "parent_id":"4520"
      },
      {
         "id":"4521",
         "name":"Tacna",
         "code":"01",
         "label":"Tacna, Tacna",
         "search":"tacna tacna",
         "children_count":"0",
         "level":"3",
         "parent_id":"4520"
      }
   ],
   "4542":[
      {
         "id":"4544",
         "name":"Chucatamani",
         "code":"02",
         "label":"Chucatamani, Tarata",
         "search":"chucatamani tarata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4542"
      },
      {
         "id":"4545",
         "name":"Estique",
         "code":"03",
         "label":"Estique, Tarata",
         "search":"estique tarata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4542"
      },
      {
         "id":"4546",
         "name":"Estique-Pampa",
         "code":"04",
         "label":"Estique-Pampa, Tarata",
         "search":"estique-pampa tarata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4542"
      },
      {
         "id":"4547",
         "name":"Sitajara",
         "code":"05",
         "label":"Sitajara, Tarata",
         "search":"sitajara tarata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4542"
      },
      {
         "id":"4548",
         "name":"Susapaya",
         "code":"06",
         "label":"Susapaya, Tarata",
         "search":"susapaya tarata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4542"
      },
      {
         "id":"4543",
         "name":"Tarata",
         "code":"01",
         "label":"Tarata, Tarata",
         "search":"tarata tarata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4542"
      },
      {
         "id":"4549",
         "name":"Tarucachi",
         "code":"07",
         "label":"Tarucachi, Tarata",
         "search":"tarucachi tarata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4542"
      },
      {
         "id":"4550",
         "name":"Ticaco",
         "code":"08",
         "label":"Ticaco, Tarata",
         "search":"ticaco tarata",
         "children_count":"0",
         "level":"3",
         "parent_id":"4542"
      }
   ],
   "4559":[
      {
         "id":"4561",
         "name":"Casitas",
         "code":"02",
         "label":"Casitas, Contralmirante Villar",
         "search":"casitas contralmirante villar",
         "children_count":"0",
         "level":"3",
         "parent_id":"4559"
      },
      {
         "id":"4560",
         "name":"Zorritos",
         "code":"01",
         "label":"Zorritos, Contralmirante Villar",
         "search":"zorritos contralmirante villar",
         "children_count":"0",
         "level":"3",
         "parent_id":"4559"
      }
   ],
   "4552":[
      {
         "id":"4554",
         "name":"Corrales",
         "code":"02",
         "label":"Corrales, Tumbes",
         "search":"corrales tumbes",
         "children_count":"0",
         "level":"3",
         "parent_id":"4552"
      },
      {
         "id":"4555",
         "name":"La Cruz",
         "code":"03",
         "label":"La Cruz, Tumbes",
         "search":"la cruz tumbes",
         "children_count":"0",
         "level":"3",
         "parent_id":"4552"
      },
      {
         "id":"4556",
         "name":"Pampas de Hospital",
         "code":"04",
         "label":"Pampas de Hospital, Tumbes",
         "search":"pampas de hospital tumbes",
         "children_count":"0",
         "level":"3",
         "parent_id":"4552"
      },
      {
         "id":"4557",
         "name":"San Jacinto",
         "code":"05",
         "label":"San Jacinto, Tumbes",
         "search":"san jacinto tumbes",
         "children_count":"0",
         "level":"3",
         "parent_id":"4552"
      },
      {
         "id":"4558",
         "name":"San Juan de la Virgen",
         "code":"06",
         "label":"San Juan de la Virgen, Tumbes",
         "search":"san juan de la virgen tumbes",
         "children_count":"0",
         "level":"3",
         "parent_id":"4552"
      },
      {
         "id":"4553",
         "name":"Tumbes",
         "code":"01",
         "label":"Tumbes, Tumbes",
         "search":"tumbes tumbes",
         "children_count":"0",
         "level":"3",
         "parent_id":"4552"
      }
   ],
   "4562":[
      {
         "id":"4564",
         "name":"Aguas Verdes",
         "code":"02",
         "label":"Aguas Verdes, Zarumilla",
         "search":"aguas verdes zarumilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"4562"
      },
      {
         "id":"4565",
         "name":"Matapalo",
         "code":"03",
         "label":"Matapalo, Zarumilla",
         "search":"matapalo zarumilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"4562"
      },
      {
         "id":"4566",
         "name":"Papayal",
         "code":"04",
         "label":"Papayal, Zarumilla",
         "search":"papayal zarumilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"4562"
      },
      {
         "id":"4563",
         "name":"Zarumilla",
         "code":"01",
         "label":"Zarumilla, Zarumilla",
         "search":"zarumilla zarumilla",
         "children_count":"0",
         "level":"3",
         "parent_id":"4562"
      }
   ],
   "4575":[
      {
         "id":"4576",
         "name":"Raymondi",
         "code":"01",
         "label":"Raymondi, Atalaya",
         "search":"raymondi atalaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4575"
      },
      {
         "id":"4577",
         "name":"Sepahua",
         "code":"02",
         "label":"Sepahua, Atalaya",
         "search":"sepahua atalaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4575"
      },
      {
         "id":"4578",
         "name":"Tahuania",
         "code":"03",
         "label":"Tahuania, Atalaya",
         "search":"tahuania atalaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4575"
      },
      {
         "id":"4579",
         "name":"Yurua",
         "code":"04",
         "label":"Yurua, Atalaya",
         "search":"yurua atalaya",
         "children_count":"0",
         "level":"3",
         "parent_id":"4575"
      }
   ],
   "4568":[
      {
         "id":"4569",
         "name":"Calleria",
         "code":"01",
         "label":"Calleria, Coronel Portillo",
         "search":"calleria coronel portillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4568"
      },
      {
         "id":"4570",
         "name":"Campoverde",
         "code":"02",
         "label":"Campoverde, Coronel Portillo",
         "search":"campoverde coronel portillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4568"
      },
      {
         "id":"4571",
         "name":"Iparia",
         "code":"03",
         "label":"Iparia, Coronel Portillo",
         "search":"iparia coronel portillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4568"
      },
      {
         "id":"4572",
         "name":"Masisea",
         "code":"04",
         "label":"Masisea, Coronel Portillo",
         "search":"masisea coronel portillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4568"
      },
      {
         "id":"4574",
         "name":"Nueva Requena",
         "code":"06",
         "label":"Nueva Requena, Coronel Portillo",
         "search":"nueva requena coronel portillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4568"
      },
      {
         "id":"4573",
         "name":"Yarinacocha",
         "code":"05",
         "label":"Yarinacocha, Coronel Portillo",
         "search":"yarinacocha coronel portillo",
         "children_count":"0",
         "level":"3",
         "parent_id":"4568"
      }
   ],
   "4580":[
      {
         "id":"4583",
         "name":"Curimana",
         "code":"03",
         "label":"Curimana, Padre Abad",
         "search":"curimana padre abad",
         "children_count":"0",
         "level":"3",
         "parent_id":"4580"
      },
      {
         "id":"4582",
         "name":"Irazola",
         "code":"02",
         "label":"Irazola, Padre Abad",
         "search":"irazola padre abad",
         "children_count":"0",
         "level":"3",
         "parent_id":"4580"
      },
      {
         "id":"4581",
         "name":"Padre Abad",
         "code":"01",
         "label":"Padre Abad, Padre Abad",
         "search":"padre abad padre abad",
         "children_count":"0",
         "level":"3",
         "parent_id":"4580"
      }
   ],
   "4584":[
      {
         "id":"4585",
         "name":"Purus",
         "code":"01",
         "label":"Purus, Purus",
         "search":"purus purus",
         "children_count":"0",
         "level":"3",
         "parent_id":"4584"
      }
   ]
}