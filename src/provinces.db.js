module.exports = {
   "2534": [
      {
         "id":"2557",
         "name":"Bagua",
         "code":"02",
         "label":"Bagua, Amazonas",
         "search":"bagua amazonas",
         "children_count":"5",
         "level":"2",
         "parent_id":"2534"
      },
      {
         "id":"2563",
         "name":"Bongara",
         "code":"03",
         "label":"Bongara, Amazonas",
         "search":"bongara amazonas",
         "children_count":"12",
         "level":"2",
         "parent_id":"2534"
      },
      {
         "id":"2535",
         "name":"Chachapoyas",
         "code":"01",
         "label":"Chachapoyas, Amazonas",
         "search":"chachapoyas amazonas",
         "children_count":"21",
         "level":"2",
         "parent_id":"2534"
      },
      {
         "id":"2576",
         "name":"Condorcanqui",
         "code":"04",
         "label":"Condorcanqui, Amazonas",
         "search":"condorcanqui amazonas",
         "children_count":"3",
         "level":"2",
         "parent_id":"2534"
      },
      {
         "id":"2580",
         "name":"Luya",
         "code":"05",
         "label":"Luya, Amazonas",
         "search":"luya amazonas",
         "children_count":"23",
         "level":"2",
         "parent_id":"2534"
      },
      {
         "id":"2604",
         "name":"Rodriguez de Mendoza",
         "code":"06",
         "label":"Rodriguez de Mendoza, Amazonas",
         "search":"rodriguez de mendoza amazonas",
         "children_count":"12",
         "level":"2",
         "parent_id":"2534"
      },
      {
         "id":"2617",
         "name":"Utcubamba",
         "code":"07",
         "label":"Utcubamba, Amazonas",
         "search":"utcubamba amazonas",
         "children_count":"7",
         "level":"2",
         "parent_id":"2534"
      }
   ],
   "2625":[
      {
         "id":"2639",
         "name":"Aija",
         "code":"02",
         "label":"Aija, Ancash",
         "search":"aija ancash",
         "children_count":"5",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2645",
         "name":"Antonio Raymondi",
         "code":"03",
         "label":"Antonio Raymondi, Ancash",
         "search":"antonio raymondi ancash",
         "children_count":"6",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2652",
         "name":"Asuncion",
         "code":"04",
         "label":"Asuncion, Ancash",
         "search":"asuncion ancash",
         "children_count":"2",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2655",
         "name":"Bolognesi",
         "code":"05",
         "label":"Bolognesi, Ancash",
         "search":"bolognesi ancash",
         "children_count":"15",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2671",
         "name":"Carhuaz",
         "code":"06",
         "label":"Carhuaz, Ancash",
         "search":"carhuaz ancash",
         "children_count":"11",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2683",
         "name":"Carlos Fermin Fitzcarrald",
         "code":"07",
         "label":"Carlos Fermin Fitzcarrald, Ancash",
         "search":"carlos fermin fitzcarrald ancash",
         "children_count":"3",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2687",
         "name":"Casma",
         "code":"08",
         "label":"Casma, Ancash",
         "search":"casma ancash",
         "children_count":"4",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2692",
         "name":"Corongo",
         "code":"09",
         "label":"Corongo, Ancash",
         "search":"corongo ancash",
         "children_count":"7",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2626",
         "name":"Huaraz",
         "code":"01",
         "label":"Huaraz, Ancash",
         "search":"huaraz ancash",
         "children_count":"12",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2700",
         "name":"Huari",
         "code":"10",
         "label":"Huari, Ancash",
         "search":"huari ancash",
         "children_count":"16",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2717",
         "name":"Huarmey",
         "code":"11",
         "label":"Huarmey, Ancash",
         "search":"huarmey ancash",
         "children_count":"5",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2723",
         "name":"Huaylas",
         "code":"12",
         "label":"Huaylas, Ancash",
         "search":"huaylas ancash",
         "children_count":"10",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2734",
         "name":"Mariscal Luzuriaga",
         "code":"13",
         "label":"Mariscal Luzuriaga, Ancash",
         "search":"mariscal luzuriaga ancash",
         "children_count":"8",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2743",
         "name":"Ocros",
         "code":"14",
         "label":"Ocros, Ancash",
         "search":"ocros ancash",
         "children_count":"10",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2754",
         "name":"Pallasca",
         "code":"15",
         "label":"Pallasca, Ancash",
         "search":"pallasca ancash",
         "children_count":"11",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2766",
         "name":"Pomabamba",
         "code":"16",
         "label":"Pomabamba, Ancash",
         "search":"pomabamba ancash",
         "children_count":"4",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2771",
         "name":"Recuay",
         "code":"17",
         "label":"Recuay, Ancash",
         "search":"recuay ancash",
         "children_count":"10",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2782",
         "name":"Santa",
         "code":"18",
         "label":"Santa, Ancash",
         "search":"santa ancash",
         "children_count":"9",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2792",
         "name":"Sihuas",
         "code":"19",
         "label":"Sihuas, Ancash",
         "search":"sihuas ancash",
         "children_count":"10",
         "level":"2",
         "parent_id":"2625"
      },
      {
         "id":"2803",
         "name":"Yungay",
         "code":"20",
         "label":"Yungay, Ancash",
         "search":"yungay ancash",
         "children_count":"8",
         "level":"2",
         "parent_id":"2625"
      }
   ],
   "2812":[
      {
         "id":"2813",
         "name":"Abancay",
         "code":"01",
         "label":"Abancay, Apurimac",
         "search":"abancay apurimac",
         "children_count":"9",
         "level":"2",
         "parent_id":"2812"
      },
      {
         "id":"2823",
         "name":"Andahuaylas",
         "code":"02",
         "label":"Andahuaylas, Apurimac",
         "search":"andahuaylas apurimac",
         "children_count":"19",
         "level":"2",
         "parent_id":"2812"
      },
      {
         "id":"2843",
         "name":"Antabamba",
         "code":"03",
         "label":"Antabamba, Apurimac",
         "search":"antabamba apurimac",
         "children_count":"7",
         "level":"2",
         "parent_id":"2812"
      },
      {
         "id":"2851",
         "name":"Aymaraes",
         "code":"04",
         "label":"Aymaraes, Apurimac",
         "search":"aymaraes apurimac",
         "children_count":"17",
         "level":"2",
         "parent_id":"2812"
      },
      {
         "id":"2876",
         "name":"Chincheros",
         "code":"06",
         "label":"Chincheros, Apurimac",
         "search":"chincheros apurimac",
         "children_count":"8",
         "level":"2",
         "parent_id":"2812"
      },
      {
         "id":"2869",
         "name":"Cotabambas",
         "code":"05",
         "label":"Cotabambas, Apurimac",
         "search":"cotabambas apurimac",
         "children_count":"6",
         "level":"2",
         "parent_id":"2812"
      },
      {
         "id":"2885",
         "name":"Grau",
         "code":"07",
         "label":"Grau, Apurimac",
         "search":"grau apurimac",
         "children_count":"14",
         "level":"2",
         "parent_id":"2812"
      }
   ],
   "2900":[
      {
         "id":"2901",
         "name":"Arequipa",
         "code":"01",
         "label":"Arequipa, Arequipa, Arequipa",
         "search":"arequipa arequipa arequipa",
         "children_count":"29",
         "level":"2",
         "parent_id":"2900"
      },
      {
         "id":"2931",
         "name":"Camana",
         "code":"02",
         "label":"Camana, Arequipa",
         "search":"camana arequipa",
         "children_count":"8",
         "level":"2",
         "parent_id":"2900"
      },
      {
         "id":"2940",
         "name":"Caraveli",
         "code":"03",
         "label":"Caraveli, Arequipa",
         "search":"caraveli arequipa",
         "children_count":"13",
         "level":"2",
         "parent_id":"2900"
      },
      {
         "id":"2954",
         "name":"Castilla",
         "code":"04",
         "label":"Castilla, Arequipa",
         "search":"castilla arequipa",
         "children_count":"16",
         "level":"2",
         "parent_id":"2900"
      },
      {
         "id":"2971",
         "name":"Caylloma",
         "code":"05",
         "label":"Caylloma, Arequipa",
         "search":"caylloma arequipa",
         "children_count":"20",
         "level":"2",
         "parent_id":"2900"
      },
      {
         "id":"2992",
         "name":"Condesuyos",
         "code":"06",
         "label":"Condesuyos, Arequipa",
         "search":"condesuyos arequipa",
         "children_count":"8",
         "level":"2",
         "parent_id":"2900"
      },
      {
         "id":"3001",
         "name":"Islay",
         "code":"07",
         "label":"Islay, Arequipa",
         "search":"islay arequipa",
         "children_count":"6",
         "level":"2",
         "parent_id":"2900"
      },
      {
         "id":"3008",
         "name":"La Union",
         "code":"08",
         "label":"La Union, Arequipa",
         "search":"la union arequipa",
         "children_count":"11",
         "level":"2",
         "parent_id":"2900"
      }
   ],
   "3020":[
      {
         "id":"3037",
         "name":"Cangallo",
         "code":"02",
         "label":"Cangallo, Ayacucho",
         "search":"cangallo ayacucho",
         "children_count":"6",
         "level":"2",
         "parent_id":"3020"
      },
      {
         "id":"3021",
         "name":"Huamanga",
         "code":"01",
         "label":"Huamanga, Ayacucho",
         "search":"huamanga ayacucho",
         "children_count":"15",
         "level":"2",
         "parent_id":"3020"
      },
      {
         "id":"3044",
         "name":"Huanca Sancos",
         "code":"03",
         "label":"Huanca Sancos, Ayacucho",
         "search":"huanca sancos ayacucho",
         "children_count":"4",
         "level":"2",
         "parent_id":"3020"
      },
      {
         "id":"3049",
         "name":"Huanta",
         "code":"04",
         "label":"Huanta, Ayacucho",
         "search":"huanta ayacucho",
         "children_count":"8",
         "level":"2",
         "parent_id":"3020"
      },
      {
         "id":"3058",
         "name":"La Mar",
         "code":"05",
         "label":"La Mar, Ayacucho",
         "search":"la mar ayacucho",
         "children_count":"8",
         "level":"2",
         "parent_id":"3020"
      },
      {
         "id":"3067",
         "name":"Lucanas",
         "code":"06",
         "label":"Lucanas, Ayacucho",
         "search":"lucanas ayacucho",
         "children_count":"21",
         "level":"2",
         "parent_id":"3020"
      },
      {
         "id":"3089",
         "name":"Parinacochas",
         "code":"07",
         "label":"Parinacochas, Ayacucho",
         "search":"parinacochas ayacucho",
         "children_count":"8",
         "level":"2",
         "parent_id":"3020"
      },
      {
         "id":"3098",
         "name":"Paucar del Sara Sara",
         "code":"08",
         "label":"Paucar del Sara Sara, Ayacucho",
         "search":"paucar del sara sara ayacucho",
         "children_count":"10",
         "level":"2",
         "parent_id":"3020"
      },
      {
         "id":"3109",
         "name":"Sucre",
         "code":"09",
         "label":"Sucre, Ayacucho",
         "search":"sucre ayacucho",
         "children_count":"11",
         "level":"2",
         "parent_id":"3020"
      },
      {
         "id":"3121",
         "name":"Victor Fajardo",
         "code":"10",
         "label":"Victor Fajardo, Ayacucho",
         "search":"victor fajardo ayacucho",
         "children_count":"12",
         "level":"2",
         "parent_id":"3020"
      },
      {
         "id":"3134",
         "name":"Vilcas Huaman",
         "code":"11",
         "label":"Vilcas Huaman, Ayacucho",
         "search":"vilcas huaman ayacucho",
         "children_count":"8",
         "level":"2",
         "parent_id":"3020"
      }
   ],
   "3143":[
      {
         "id":"3157",
         "name":"Cajabamba",
         "code":"02",
         "label":"Cajabamba, Cajamarca",
         "search":"cajabamba cajamarca",
         "children_count":"4",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3144",
         "name":"Cajamarca",
         "code":"01",
         "label":"Cajamarca, Cajamarca, Cajamarca",
         "search":"cajamarca cajamarca cajamarca",
         "children_count":"12",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3162",
         "name":"Celendin",
         "code":"03",
         "label":"Celendin, Cajamarca",
         "search":"celendin cajamarca",
         "children_count":"12",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3175",
         "name":"Chota",
         "code":"04",
         "label":"Chota, Cajamarca",
         "search":"chota cajamarca",
         "children_count":"19",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3195",
         "name":"Contumaza",
         "code":"05",
         "label":"Contumaza, Cajamarca",
         "search":"contumaza cajamarca",
         "children_count":"8",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3204",
         "name":"Cutervo",
         "code":"06",
         "label":"Cutervo, Cajamarca",
         "search":"cutervo cajamarca",
         "children_count":"15",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3220",
         "name":"Hualgayoc",
         "code":"07",
         "label":"Hualgayoc, Cajamarca",
         "search":"hualgayoc cajamarca",
         "children_count":"3",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3224",
         "name":"Jaen",
         "code":"08",
         "label":"Jaen, Cajamarca",
         "search":"jaen cajamarca",
         "children_count":"12",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3237",
         "name":"San Ignacio",
         "code":"09",
         "label":"San Ignacio, Cajamarca",
         "search":"san ignacio cajamarca",
         "children_count":"7",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3245",
         "name":"San Marcos",
         "code":"10",
         "label":"San Marcos, Cajamarca",
         "search":"san marcos cajamarca",
         "children_count":"7",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3253",
         "name":"San Miguel",
         "code":"11",
         "label":"San Miguel, Cajamarca",
         "search":"san miguel cajamarca",
         "children_count":"13",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3267",
         "name":"San Pablo",
         "code":"12",
         "label":"San Pablo, Cajamarca",
         "search":"san pablo cajamarca",
         "children_count":"4",
         "level":"2",
         "parent_id":"3143"
      },
      {
         "id":"3272",
         "name":"Santa Cruz",
         "code":"13",
         "label":"Santa Cruz, Cajamarca",
         "search":"santa cruz cajamarca",
         "children_count":"11",
         "level":"2",
         "parent_id":"3143"
      }
   ],
   "3292":[
      {
         "id":"3302",
         "name":"Acomayo",
         "code":"02",
         "label":"Acomayo, Cusco",
         "search":"acomayo cusco",
         "children_count":"7",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3310",
         "name":"Anta",
         "code":"03",
         "label":"Anta, Cusco",
         "search":"anta cusco",
         "children_count":"9",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3320",
         "name":"Calca",
         "code":"04",
         "label":"Calca, Cusco",
         "search":"calca cusco",
         "children_count":"8",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3329",
         "name":"Canas",
         "code":"05",
         "label":"Canas, Cusco",
         "search":"canas cusco",
         "children_count":"8",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3338",
         "name":"Canchis",
         "code":"06",
         "label":"Canchis, Cusco",
         "search":"canchis cusco",
         "children_count":"8",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3347",
         "name":"Chumbivilcas",
         "code":"07",
         "label":"Chumbivilcas, Cusco",
         "search":"chumbivilcas cusco",
         "children_count":"8",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3293",
         "name":"Cusco",
         "code":"01",
         "label":"Cusco, Cusco, Cusco",
         "search":"cusco cusco cusco",
         "children_count":"8",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3356",
         "name":"Espinar",
         "code":"08",
         "label":"Espinar, Cusco",
         "search":"espinar cusco",
         "children_count":"8",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3365",
         "name":"La Convencion",
         "code":"09",
         "label":"La Convencion, Cusco",
         "search":"la convencion cusco",
         "children_count":"10",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3376",
         "name":"Paruro",
         "code":"10",
         "label":"Paruro, Cusco",
         "search":"paruro cusco",
         "children_count":"9",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3386",
         "name":"Paucartambo",
         "code":"11",
         "label":"Paucartambo, Cusco",
         "search":"paucartambo cusco",
         "children_count":"6",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3393",
         "name":"Quispicanchi",
         "code":"12",
         "label":"Quispicanchi, Cusco",
         "search":"quispicanchi cusco",
         "children_count":"12",
         "level":"2",
         "parent_id":"3292"
      },
      {
         "id":"3406",
         "name":"Urubamba",
         "code":"13",
         "label":"Urubamba, Cusco",
         "search":"urubamba cusco",
         "children_count":"7",
         "level":"2",
         "parent_id":"3292"
      }
   ],
   "3414":[
      {
         "id":"3435",
         "name":"Acobamba",
         "code":"02",
         "label":"Acobamba, Huancavelica",
         "search":"acobamba huancavelica",
         "children_count":"8",
         "level":"2",
         "parent_id":"3414"
      },
      {
         "id":"3444",
         "name":"Angaraes",
         "code":"03",
         "label":"Angaraes, Huancavelica",
         "search":"angaraes huancavelica",
         "children_count":"12",
         "level":"2",
         "parent_id":"3414"
      },
      {
         "id":"3457",
         "name":"Castrovirreyna",
         "code":"04",
         "label":"Castrovirreyna, Huancavelica",
         "search":"castrovirreyna huancavelica",
         "children_count":"13",
         "level":"2",
         "parent_id":"3414"
      },
      {
         "id":"3471",
         "name":"Churcampa",
         "code":"05",
         "label":"Churcampa, Huancavelica",
         "search":"churcampa huancavelica",
         "children_count":"10",
         "level":"2",
         "parent_id":"3414"
      },
      {
         "id":"3415",
         "name":"Huancavelica",
         "code":"01",
         "label":"Huancavelica, Huancavelica, Huancavelica",
         "search":"huancavelica huancavelica huancavelica",
         "children_count":"19",
         "level":"2",
         "parent_id":"3414"
      },
      {
         "id":"3482",
         "name":"Huaytara",
         "code":"06",
         "label":"Huaytara, Huancavelica",
         "search":"huaytara huancavelica",
         "children_count":"16",
         "level":"2",
         "parent_id":"3414"
      },
      {
         "id":"3499",
         "name":"Tayacaja",
         "code":"07",
         "label":"Tayacaja, Huancavelica",
         "search":"tayacaja huancavelica",
         "children_count":"18",
         "level":"2",
         "parent_id":"3414"
      }
   ],
   "3518":[
      {
         "id":"3531",
         "name":"Ambo",
         "code":"02",
         "label":"Ambo, Huanuco",
         "search":"ambo huanuco",
         "children_count":"8",
         "level":"2",
         "parent_id":"3518"
      },
      {
         "id":"3540",
         "name":"Dos de Mayo",
         "code":"03",
         "label":"Dos de Mayo, Huanuco",
         "search":"dos de mayo huanuco",
         "children_count":"9",
         "level":"2",
         "parent_id":"3518"
      },
      {
         "id":"3550",
         "name":"Huacaybamba",
         "code":"04",
         "label":"Huacaybamba, Huanuco",
         "search":"huacaybamba huanuco",
         "children_count":"4",
         "level":"2",
         "parent_id":"3518"
      },
      {
         "id":"3555",
         "name":"Huamalies",
         "code":"05",
         "label":"Huamalies, Huanuco",
         "search":"huamalies huanuco",
         "children_count":"11",
         "level":"2",
         "parent_id":"3518"
      },
      {
         "id":"3519",
         "name":"Huanuco",
         "code":"01",
         "label":"Huanuco, Huanuco, Huanuco",
         "search":"huanuco huanuco huanuco",
         "children_count":"11",
         "level":"2",
         "parent_id":"3518"
      },
      {
         "id":"3589",
         "name":"Lauricocha",
         "code":"10",
         "label":"Lauricocha, Huanuco",
         "search":"lauricocha huanuco",
         "children_count":"7",
         "level":"2",
         "parent_id":"3518"
      },
      {
         "id":"3567",
         "name":"Leoncio Prado",
         "code":"06",
         "label":"Leoncio Prado, Huanuco",
         "search":"leoncio prado huanuco",
         "children_count":"6",
         "level":"2",
         "parent_id":"3518"
      },
      {
         "id":"3574",
         "name":"Maraqon",
         "code":"07",
         "label":"Maraqon, Huanuco",
         "search":"maraqon huanuco",
         "children_count":"3",
         "level":"2",
         "parent_id":"3518"
      },
      {
         "id":"3578",
         "name":"Pachitea",
         "code":"08",
         "label":"Pachitea, Huanuco",
         "search":"pachitea huanuco",
         "children_count":"4",
         "level":"2",
         "parent_id":"3518"
      },
      {
         "id":"3583",
         "name":"Puerto Inca",
         "code":"09",
         "label":"Puerto Inca, Huanuco",
         "search":"puerto inca huanuco",
         "children_count":"5",
         "level":"2",
         "parent_id":"3518"
      },
      {
         "id":"3597",
         "name":"Yarowilca",
         "code":"11",
         "label":"Yarowilca, Huanuco",
         "search":"yarowilca huanuco",
         "children_count":"8",
         "level":"2",
         "parent_id":"3518"
      }
   ],
   "3606":[
      {
         "id":"3622",
         "name":"Chincha",
         "code":"02",
         "label":"Chincha, Ica",
         "search":"chincha ica",
         "children_count":"11",
         "level":"2",
         "parent_id":"3606"
      },
      {
         "id":"3607",
         "name":"Ica",
         "code":"01",
         "label":"Ica, Ica, Ica",
         "search":"ica ica ica",
         "children_count":"14",
         "level":"2",
         "parent_id":"3606"
      },
      {
         "id":"3634",
         "name":"Nazca",
         "code":"03",
         "label":"Nazca, Ica",
         "search":"nazca ica",
         "children_count":"5",
         "level":"2",
         "parent_id":"3606"
      },
      {
         "id":"3640",
         "name":"Palpa",
         "code":"04",
         "label":"Palpa, Ica",
         "search":"palpa ica",
         "children_count":"5",
         "level":"2",
         "parent_id":"3606"
      },
      {
         "id":"3646",
         "name":"Pisco",
         "code":"05",
         "label":"Pisco, Ica",
         "search":"pisco ica",
         "children_count":"8",
         "level":"2",
         "parent_id":"3606"
      }
   ],
   "3655":[
      {
         "id":"3701",
         "name":"Chanchamayo",
         "code":"03",
         "label":"Chanchamayo, Junin",
         "search":"chanchamayo junin",
         "children_count":"6",
         "level":"2",
         "parent_id":"3655"
      },
      {
         "id":"3778",
         "name":"Chupaca",
         "code":"09",
         "label":"Chupaca, Junin",
         "search":"chupaca junin",
         "children_count":"9",
         "level":"2",
         "parent_id":"3655"
      },
      {
         "id":"3685",
         "name":"Concepcion",
         "code":"02",
         "label":"Concepcion, Junin",
         "search":"concepcion junin",
         "children_count":"15",
         "level":"2",
         "parent_id":"3655"
      },
      {
         "id":"3656",
         "name":"Huancayo",
         "code":"01",
         "label":"Huancayo, Junin",
         "search":"huancayo junin",
         "children_count":"28",
         "level":"2",
         "parent_id":"3655"
      },
      {
         "id":"3708",
         "name":"Jauja",
         "code":"04",
         "label":"Jauja, Junin",
         "search":"jauja junin",
         "children_count":"34",
         "level":"2",
         "parent_id":"3655"
      },
      {
         "id":"3743",
         "name":"Junin",
         "code":"05",
         "label":"Junin, Junin, Junin",
         "search":"junin junin junin",
         "children_count":"4",
         "level":"2",
         "parent_id":"3655"
      },
      {
         "id":"3748",
         "name":"Satipo",
         "code":"06",
         "label":"Satipo, Junin",
         "search":"satipo junin",
         "children_count":"8",
         "level":"2",
         "parent_id":"3655"
      },
      {
         "id":"3757",
         "name":"Tarma",
         "code":"07",
         "label":"Tarma, Junin",
         "search":"tarma junin",
         "children_count":"9",
         "level":"2",
         "parent_id":"3655"
      },
      {
         "id":"3767",
         "name":"Yauli",
         "code":"08",
         "label":"Yauli, Junin",
         "search":"yauli junin",
         "children_count":"10",
         "level":"2",
         "parent_id":"3655"
      }
   ],
   "3788":[
      {
         "id":"3801",
         "name":"Ascope",
         "code":"02",
         "label":"Ascope, La Libertad",
         "search":"ascope la libertad",
         "children_count":"8",
         "level":"2",
         "parent_id":"3788"
      },
      {
         "id":"3810",
         "name":"Bolivar",
         "code":"03",
         "label":"Bolivar, La Libertad",
         "search":"bolivar la libertad",
         "children_count":"6",
         "level":"2",
         "parent_id":"3788"
      },
      {
         "id":"3817",
         "name":"Chepen",
         "code":"04",
         "label":"Chepen, La Libertad",
         "search":"chepen la libertad",
         "children_count":"3",
         "level":"2",
         "parent_id":"3788"
      },
      {
         "id":"3875",
         "name":"Gran Chimu",
         "code":"11",
         "label":"Gran Chimu, La Libertad",
         "search":"gran chimu la libertad",
         "children_count":"4",
         "level":"2",
         "parent_id":"3788"
      },
      {
         "id":"3821",
         "name":"Julcan",
         "code":"05",
         "label":"Julcan, La Libertad",
         "search":"julcan la libertad",
         "children_count":"4",
         "level":"2",
         "parent_id":"3788"
      },
      {
         "id":"3826",
         "name":"Otuzco",
         "code":"06",
         "label":"Otuzco, La Libertad",
         "search":"otuzco la libertad",
         "children_count":"10",
         "level":"2",
         "parent_id":"3788"
      },
      {
         "id":"3837",
         "name":"Pacasmayo",
         "code":"07",
         "label":"Pacasmayo, La Libertad",
         "search":"pacasmayo la libertad",
         "children_count":"5",
         "level":"2",
         "parent_id":"3788"
      },
      {
         "id":"3843",
         "name":"Pataz",
         "code":"08",
         "label":"Pataz, La Libertad",
         "search":"pataz la libertad",
         "children_count":"13",
         "level":"2",
         "parent_id":"3788"
      },
      {
         "id":"3857",
         "name":"Sanchez Carrion",
         "code":"09",
         "label":"Sanchez Carrion, La Libertad",
         "search":"sanchez carrion la libertad",
         "children_count":"8",
         "level":"2",
         "parent_id":"3788"
      },
      {
         "id":"3866",
         "name":"Santiago de Chuco",
         "code":"10",
         "label":"Santiago de Chuco, La Libertad",
         "search":"santiago de chuco la libertad",
         "children_count":"8",
         "level":"2",
         "parent_id":"3788"
      },
      {
         "id":"3789",
         "name":"Trujillo",
         "code":"01",
         "label":"Trujillo, La Libertad",
         "search":"trujillo la libertad",
         "children_count":"11",
         "level":"2",
         "parent_id":"3788"
      },
      {
         "id":"3880",
         "name":"Viru",
         "code":"12",
         "label":"Viru, La Libertad",
         "search":"viru la libertad",
         "children_count":"3",
         "level":"2",
         "parent_id":"3788"
      }
   ],
   "3884":[
      {
         "id":"3885",
         "name":"Chiclayo",
         "code":"01",
         "label":"Chiclayo, Lambayeque",
         "search":"chiclayo lambayeque",
         "children_count":"20",
         "level":"2",
         "parent_id":"3884"
      },
      {
         "id":"3906",
         "name":"Ferreqafe",
         "code":"02",
         "label":"Ferreqafe, Lambayeque",
         "search":"ferreqafe lambayeque",
         "children_count":"6",
         "level":"2",
         "parent_id":"3884"
      },
      {
         "id":"3913",
         "name":"Lambayeque",
         "code":"03",
         "label":"Lambayeque, Lambayeque, Lambayeque",
         "search":"lambayeque lambayeque lambayeque",
         "children_count":"12",
         "level":"2",
         "parent_id":"3884"
      }
   ],
   "3926":[
      {
         "id":"3971",
         "name":"Barranca",
         "code":"02",
         "label":"Barranca, Lima",
         "search":"barranca lima",
         "children_count":"5",
         "level":"2",
         "parent_id":"3926"
      },
      {
         "id":"3977",
         "name":"Cajatambo",
         "code":"03",
         "label":"Cajatambo, Lima",
         "search":"cajatambo lima",
         "children_count":"5",
         "level":"2",
         "parent_id":"3926"
      },
      {
         "id":"3285",
         "name":"Callao",
         "code":"01",
         "label":"Callao, Callao, Lima",
         "search":"callao callao lima",
         "children_count":"6",
         "level":"2",
         "parent_id":"3926"
      },
      {
         "id":"3991",
         "name":"Ca\u00f1ete",
         "code":"05",
         "label":"Ca\u00f1ete, Lima",
         "search":"ca\u00f1ete lima",
         "children_count":"16",
         "level":"2",
         "parent_id":"3926"
      },
      {
         "id":"3983",
         "name":"Canta",
         "code":"04",
         "label":"Canta, Lima",
         "search":"canta lima",
         "children_count":"7",
         "level":"2",
         "parent_id":"3926"
      },
      {
         "id":"4008",
         "name":"Huaral",
         "code":"06",
         "label":"Huaral, Lima",
         "search":"huaral lima",
         "children_count":"12",
         "level":"2",
         "parent_id":"3926"
      },
      {
         "id":"4021",
         "name":"Huarochiri",
         "code":"07",
         "label":"Huarochiri, Lima",
         "search":"huarochiri lima",
         "children_count":"32",
         "level":"2",
         "parent_id":"3926"
      },
      {
         "id":"4054",
         "name":"Huaura",
         "code":"08",
         "label":"Huaura, Lima",
         "search":"huaura lima",
         "children_count":"12",
         "level":"2",
         "parent_id":"3926"
      },
      {
         "id":"3927",
         "name":"Lima",
         "code":"01",
         "label":"Lima, Lima, Lima",
         "search":"lima lima lima",
         "children_count":"43",
         "level":"2",
         "parent_id":"3926"
      },
      {
         "id":"4067",
         "name":"Oyon",
         "code":"09",
         "label":"Oyon, Lima",
         "search":"oyon lima",
         "children_count":"6",
         "level":"2",
         "parent_id":"3926"
      },
      {
         "id":"4074",
         "name":"Yauyos",
         "code":"10",
         "label":"Yauyos, Lima",
         "search":"yauyos lima",
         "children_count":"33",
         "level":"2",
         "parent_id":"3926"
      }
   ],
   "4108":[
      {
         "id":"4123",
         "name":"Alto Amazonas",
         "code":"02",
         "label":"Alto Amazonas, Loreto",
         "search":"alto amazonas loreto",
         "children_count":"11",
         "level":"2",
         "parent_id":"4108"
      },
      {
         "id":"4135",
         "name":"Loreto",
         "code":"03",
         "label":"Loreto, Loreto",
         "search":"loreto loreto",
         "children_count":"5",
         "level":"2",
         "parent_id":"4108"
      },
      {
         "id":"4141",
         "name":"Mariscal Ramon Castilla",
         "code":"04",
         "label":"Mariscal Ramon Castilla, Loreto",
         "search":"mariscal ramon castilla loreto",
         "children_count":"4",
         "level":"2",
         "parent_id":"4108"
      },
      {
         "id":"4109",
         "name":"Maynas",
         "code":"01",
         "label":"Maynas, Loreto",
         "search":"maynas loreto",
         "children_count":"13",
         "level":"2",
         "parent_id":"4108"
      },
      {
         "id":"4146",
         "name":"Requena",
         "code":"05",
         "label":"Requena, Loreto",
         "search":"requena loreto",
         "children_count":"11",
         "level":"2",
         "parent_id":"4108"
      },
      {
         "id":"4158",
         "name":"Ucayali",
         "code":"06",
         "label":"Ucayali, Loreto",
         "search":"ucayali loreto",
         "children_count":"6",
         "level":"2",
         "parent_id":"4108"
      }
   ],
   "4165":[
      {
         "id":"4171",
         "name":"Manu",
         "code":"02",
         "label":"Manu, Madre de Dios",
         "search":"manu madre de dios",
         "children_count":"4",
         "level":"2",
         "parent_id":"4165"
      },
      {
         "id":"4176",
         "name":"Tahuamanu",
         "code":"03",
         "label":"Tahuamanu, Madre de Dios",
         "search":"tahuamanu madre de dios",
         "children_count":"3",
         "level":"2",
         "parent_id":"4165"
      },
      {
         "id":"4166",
         "name":"Tambopata",
         "code":"01",
         "label":"Tambopata, Madre de Dios",
         "search":"tambopata madre de dios",
         "children_count":"4",
         "level":"2",
         "parent_id":"4165"
      }
   ],
   "4180":[
      {
         "id":"4188",
         "name":"General Sanchez Cerro",
         "code":"02",
         "label":"General Sanchez Cerro, Moquegua",
         "search":"general sanchez cerro moquegua",
         "children_count":"11",
         "level":"2",
         "parent_id":"4180"
      },
      {
         "id":"4200",
         "name":"Ilo",
         "code":"03",
         "label":"Ilo, Moquegua",
         "search":"ilo moquegua",
         "children_count":"3",
         "level":"2",
         "parent_id":"4180"
      },
      {
         "id":"4181",
         "name":"Mariscal Nieto",
         "code":"01",
         "label":"Mariscal Nieto, Moquegua",
         "search":"mariscal nieto moquegua",
         "children_count":"6",
         "level":"2",
         "parent_id":"4180"
      }
   ],
   "4204":[
      {
         "id":"4219",
         "name":"Daniel Alcides Carrion",
         "code":"02",
         "label":"Daniel Alcides Carrion, Pasco",
         "search":"daniel alcides carrion pasco",
         "children_count":"8",
         "level":"2",
         "parent_id":"4204"
      },
      {
         "id":"4228",
         "name":"Oxapampa",
         "code":"03",
         "label":"Oxapampa, Pasco",
         "search":"oxapampa pasco",
         "children_count":"7",
         "level":"2",
         "parent_id":"4204"
      },
      {
         "id":"4205",
         "name":"Pasco",
         "code":"01",
         "label":"Pasco, Pasco",
         "search":"pasco pasco",
         "children_count":"13",
         "level":"2",
         "parent_id":"4204"
      }
   ],
   "4236":[
      {
         "id":"4247",
         "name":"Ayabaca",
         "code":"02",
         "label":"Ayabaca, Piura",
         "search":"ayabaca piura",
         "children_count":"10",
         "level":"2",
         "parent_id":"4236"
      },
      {
         "id":"4258",
         "name":"Huancabamba",
         "code":"03",
         "label":"Huancabamba, Piura",
         "search":"huancabamba piura",
         "children_count":"8",
         "level":"2",
         "parent_id":"4236"
      },
      {
         "id":"4267",
         "name":"Morropon",
         "code":"04",
         "label":"Morropon, Piura",
         "search":"morropon piura",
         "children_count":"10",
         "level":"2",
         "parent_id":"4236"
      },
      {
         "id":"4278",
         "name":"Paita",
         "code":"05",
         "label":"Paita, Piura",
         "search":"paita piura",
         "children_count":"7",
         "level":"2",
         "parent_id":"4236"
      },
      {
         "id":"4237",
         "name":"Piura",
         "code":"01",
         "label":"Piura, Piura, Piura",
         "search":"piura piura piura",
         "children_count":"9",
         "level":"2",
         "parent_id":"4236"
      },
      {
         "id":"4302",
         "name":"Sechura",
         "code":"08",
         "label":"Sechura, Piura",
         "search":"sechura piura",
         "children_count":"6",
         "level":"2",
         "parent_id":"4236"
      },
      {
         "id":"4286",
         "name":"Sullana",
         "code":"06",
         "label":"Sullana, Piura",
         "search":"sullana piura",
         "children_count":"8",
         "level":"2",
         "parent_id":"4236"
      },
      {
         "id":"4295",
         "name":"Talara",
         "code":"07",
         "label":"Talara, Piura",
         "search":"talara piura",
         "children_count":"6",
         "level":"2",
         "parent_id":"4236"
      }
   ],
   "4309":[
      {
         "id":"4326",
         "name":"Azangaro",
         "code":"02",
         "label":"Azangaro, Puno",
         "search":"azangaro puno",
         "children_count":"15",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4342",
         "name":"Carabaya",
         "code":"03",
         "label":"Carabaya, Puno",
         "search":"carabaya puno",
         "children_count":"10",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4353",
         "name":"Chucuito",
         "code":"04",
         "label":"Chucuito, Puno",
         "search":"chucuito puno",
         "children_count":"7",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4361",
         "name":"El Collao",
         "code":"05",
         "label":"El Collao, Puno",
         "search":"el collao puno",
         "children_count":"5",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4367",
         "name":"Huancane",
         "code":"06",
         "label":"Huancane, Puno",
         "search":"huancane puno",
         "children_count":"8",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4376",
         "name":"Lampa",
         "code":"07",
         "label":"Lampa, Puno",
         "search":"lampa puno",
         "children_count":"10",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4387",
         "name":"Melgar",
         "code":"08",
         "label":"Melgar, Puno",
         "search":"melgar puno",
         "children_count":"9",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4397",
         "name":"Moho",
         "code":"09",
         "label":"Moho, Puno",
         "search":"moho puno",
         "children_count":"4",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4310",
         "name":"Puno",
         "code":"01",
         "label":"Puno, Puno, Puno",
         "search":"puno puno puno",
         "children_count":"15",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4402",
         "name":"San Antonio de Putina",
         "code":"10",
         "label":"San Antonio de Putina, Puno",
         "search":"san antonio de putina puno",
         "children_count":"5",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4408",
         "name":"San Roman",
         "code":"11",
         "label":"San Roman, Puno",
         "search":"san roman puno",
         "children_count":"4",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4413",
         "name":"Sandia",
         "code":"12",
         "label":"Sandia, Puno",
         "search":"sandia puno",
         "children_count":"9",
         "level":"2",
         "parent_id":"4309"
      },
      {
         "id":"4423",
         "name":"Yunguyo",
         "code":"13",
         "label":"Yunguyo, Puno",
         "search":"yunguyo puno",
         "children_count":"7",
         "level":"2",
         "parent_id":"4309"
      }
   ],
   "4431":[
      {
         "id":"4439",
         "name":"Bellavista",
         "code":"02",
         "label":"Bellavista, San Martin",
         "search":"bellavista san martin",
         "children_count":"6",
         "level":"2",
         "parent_id":"4431"
      },
      {
         "id":"4446",
         "name":"El Dorado",
         "code":"03",
         "label":"El Dorado, San Martin",
         "search":"el dorado san martin",
         "children_count":"5",
         "level":"2",
         "parent_id":"4431"
      },
      {
         "id":"4452",
         "name":"Huallaga",
         "code":"04",
         "label":"Huallaga, San Martin",
         "search":"huallaga san martin",
         "children_count":"6",
         "level":"2",
         "parent_id":"4431"
      },
      {
         "id":"4459",
         "name":"Lamas",
         "code":"05",
         "label":"Lamas, San Martin",
         "search":"lamas san martin",
         "children_count":"11",
         "level":"2",
         "parent_id":"4431"
      },
      {
         "id":"4471",
         "name":"Mariscal Caceres",
         "code":"06",
         "label":"Mariscal Caceres, San Martin",
         "search":"mariscal caceres san martin",
         "children_count":"5",
         "level":"2",
         "parent_id":"4431"
      },
      {
         "id":"4432",
         "name":"Moyobamba",
         "code":"01",
         "label":"Moyobamba, San Martin",
         "search":"moyobamba san martin",
         "children_count":"6",
         "level":"2",
         "parent_id":"4431"
      },
      {
         "id":"4477",
         "name":"Picota",
         "code":"07",
         "label":"Picota, San Martin",
         "search":"picota san martin",
         "children_count":"10",
         "level":"2",
         "parent_id":"4431"
      },
      {
         "id":"4488",
         "name":"Rioja",
         "code":"08",
         "label":"Rioja, San Martin",
         "search":"rioja san martin",
         "children_count":"9",
         "level":"2",
         "parent_id":"4431"
      },
      {
         "id":"4498",
         "name":"San Martin",
         "code":"09",
         "label":"San Martin, San Martin",
         "search":"san martin san martin",
         "children_count":"14",
         "level":"2",
         "parent_id":"4431"
      },
      {
         "id":"4513",
         "name":"Tocache",
         "code":"10",
         "label":"Tocache, San Martin",
         "search":"tocache san martin",
         "children_count":"5",
         "level":"2",
         "parent_id":"4431"
      }
   ],
   "4519":[
      {
         "id":"4531",
         "name":"Candarave",
         "code":"02",
         "label":"Candarave, Tacna",
         "search":"candarave tacna",
         "children_count":"6",
         "level":"2",
         "parent_id":"4519"
      },
      {
         "id":"4538",
         "name":"Jorge Basadre",
         "code":"03",
         "label":"Jorge Basadre, Tacna",
         "search":"jorge basadre tacna",
         "children_count":"3",
         "level":"2",
         "parent_id":"4519"
      },
      {
         "id":"4520",
         "name":"Tacna",
         "code":"01",
         "label":"Tacna, Tacna, Tacna",
         "search":"tacna tacna tacna",
         "children_count":"10",
         "level":"2",
         "parent_id":"4519"
      },
      {
         "id":"4542",
         "name":"Tarata",
         "code":"04",
         "label":"Tarata, Tacna",
         "search":"tarata tacna",
         "children_count":"8",
         "level":"2",
         "parent_id":"4519"
      }
   ],
   "4551":[
      {
         "id":"4559",
         "name":"Contralmirante Villar",
         "code":"02",
         "label":"Contralmirante Villar, Tumbes",
         "search":"contralmirante villar tumbes",
         "children_count":"2",
         "level":"2",
         "parent_id":"4551"
      },
      {
         "id":"4552",
         "name":"Tumbes",
         "code":"01",
         "label":"Tumbes, Tumbes, Tumbes",
         "search":"tumbes tumbes tumbes",
         "children_count":"6",
         "level":"2",
         "parent_id":"4551"
      },
      {
         "id":"4562",
         "name":"Zarumilla",
         "code":"03",
         "label":"Zarumilla, Tumbes",
         "search":"zarumilla tumbes",
         "children_count":"4",
         "level":"2",
         "parent_id":"4551"
      }
   ],
   "4567":[
      {
         "id":"4575",
         "name":"Atalaya",
         "code":"02",
         "label":"Atalaya, Ucayali",
         "search":"atalaya ucayali",
         "children_count":"4",
         "level":"2",
         "parent_id":"4567"
      },
      {
         "id":"4568",
         "name":"Coronel Portillo",
         "code":"01",
         "label":"Coronel Portillo, Ucayali",
         "search":"coronel portillo ucayali",
         "children_count":"6",
         "level":"2",
         "parent_id":"4567"
      },
      {
         "id":"4580",
         "name":"Padre Abad",
         "code":"03",
         "label":"Padre Abad, Ucayali",
         "search":"padre abad ucayali",
         "children_count":"3",
         "level":"2",
         "parent_id":"4567"
      },
      {
         "id":"4584",
         "name":"Purus",
         "code":"04",
         "label":"Purus, Ucayali",
         "search":"purus ucayali",
         "children_count":"1",
         "level":"2",
         "parent_id":"4567"
      }
   ]
}