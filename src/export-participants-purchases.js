const db = require('./firestore-connect')
const excel = require('node-excel-export')
const path = require('path')
const fs = require('fs')

const COL_PARTICIPANTS = 'participants'
const COL_PURCHASES = 'purchases'
const OUTPUT_FILENAME = 'participants-purchases-db'
const OUTPUT_FILE = './output/participants-purchases-db.xlsx'

// store process arguments
// const args = process.argv.slice(2)

const outputPath = path.resolve(OUTPUT_FILE)
const styles = {
   headerDark: {
      fill: {
         fgColor: {
            rgb: 'FF000000'
         }
      },
      font: {
         color: {
            rgb: 'FFFFFFFF'
         },
         sz: 14,
         bold: true,
         underline: true
      }
   },
   cellPink: {
      fill: {
         fgColor: {
            rgb: 'FFFFCCFF'
         }
      }
   },
   cellGreen: {
      fill: {
         fgColor: {
            rgb: 'FF00FF00'
         }
      }
   }
}

const getPurchases = (participantData) => {
   const {
      document,
      documentType,
      email,
      name,
      surname,
      phone,
      department,
      province,
      district,
      totalPurchases,
      dataprotectionChecked,
      termsChecked,
      useofinfoChecked,
      created,
   } = participantData

   const outputArray = []

   return db.collection(COL_PURCHASES)
      .where('document', '==', document)
      .get()
      .then(snapshot => {
         if (!snapshot.empty) {
            snapshot.forEach(docPur => {
               const {
                  purchaseCode,
                  ticketSerial,
                  purchaseDate,
                  purchaseAmount,
                  paidwCMRChecked,
               } = docPur.data()

               outputArray.push({
                  // Purchase data
                  purchaseCode: purchaseCode,
                  ticketSerial: ticketSerial,
                  purchaseAmount: purchaseAmount,
                  purchaseDate: purchaseDate,
                  paidwCMRChecked: paidwCMRChecked,
                  ticketCreatedAt: docPur.data().created,

                  // Participant data
                  documentType: documentType,
                  document: document,
                  name: name,
                  surname: surname,
                  email: email,
                  phone: phone,
                  department: department,
                  province: province,
                  district: district,
                  totalPurchases: totalPurchases,
                  dataprotectionChecked: dataprotectionChecked,
                  termsChecked: termsChecked,
                  useofinfoChecked: useofinfoChecked,
                  participantCreatedAt: created,
               })
            })

            return outputArray
         }
      })
}

const getData = () => {
   db.collection(COL_PARTICIPANTS)
      .get()
      .then(snapshot => {
         if (!snapshot.empty) {
            let participants = []
            snapshot.forEach(docPart => {
               participants.push(docPart.data())
            })
            return participants
         }
      })
      .then(participants => {
         const promises = []
         if (participants && participants.length > 0) {
            participants.map(participant => {
               promises.push(getPurchases(participant))
            })

            return Promise.all(promises)
         }

         return []
      })
      .then(dataArray => {
         if (dataArray) {
            const outputArray = []

            dataArray.map(data => {
               if (data && data.length > 0) {
                  data.map(object => {
                     outputArray.push(object)
                  })
               }
            })

            return outputArray
         }
      }).then(outputData => {
         try {
            const { headerDark, cellWidth, cellWidthLg, cellWidthSm } = {
               cellWidth: 120,
               cellWidthLg: 200,
               cellWidthSm: 50,
               headerDark: {
                  fill: {
                     fgColor: {
                       rgb: 'dddddd'
                     }
                   },
                   font: {
                     color: {
                       rgb: '000000'
                     },
                     bold: true
                   }
               }
            }

            // Create the excel report.
            // This function will return Buffer
            const report = excel.buildExport(
               [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
                  {
                     //name: 'Registros', // <- Specify sheet name (optional)
                     //heading: excelConfig.heading,
                     specification: {
                        // Purchase data
                        purchaseCode: {
                           headerStyle: headerDark,
                           displayName: 'Número ticket',
                           width: cellWidth
                        },
                        ticketSerial: { 
                           headerStyle: headerDark,
                           displayName: 'Serie ticket',
                           width: cellWidth
                        },
                        purchaseAmount: {
                           headerStyle: headerDark,
                           displayName: 'Monto de compra',
                           width: cellWidth
                        },
                        purchaseDate: {
                           headerStyle: headerDark,
                           displayName: 'Fecha de compra',
                           cellStyle: () => {
                              return { numFmt: 'd-mm-yyyy' }
                           },
                           width: cellWidth,
                        },
                        paidwCMRChecked: {
                           headerStyle: headerDark,
                           displayName: '¿Pagó con CMR?',
                           width: cellWidth,
                           cellFormat: function(value, row) {
                              return value ? 'sí' : 'no';
                           }
                        },
                        ticketCreatedAt: { 
                           headerStyle: headerDark,
                           displayName: 'Fecha reg. ticket',
                           width: cellWidth,
                           cellStyle: () => {
                              return { numFmt: 'd-mm-yyyy hh:mm:ss' }
                           },
                        },

                        // Participant data
                        documentType: {
                           headerStyle: headerDark,
                           displayName: 'Tipo documento',
                           width: cellWidth 
                        },
                        document: { 
                           headerStyle: headerDark,
                           displayName: 'Documento',
                           width: cellWidth
                        },
                        name: {
                           headerStyle: headerDark,
                           displayName: 'Nombres',
                           width: cellWidthLg
                        },
                        surname: {
                           headerStyle: headerDark,
                           displayName: 'Apellidos',
                           width: cellWidthLg
                        },
                        email: {
                           headerStyle: headerDark,
                           displayName: 'Email',
                           width: cellWidthLg
                        },
                        phone: {
                           headerStyle: headerDark,
                           displayName: 'Teléfono/Celular',
                           width: cellWidth
                        },
                        department: { 
                           headerStyle: headerDark,
                           displayName: 'Departamento',
                           width: cellWidth 
                        },
                        province: { 
                           headerStyle: headerDark,
                           displayName: 'Provincia',
                           width: cellWidth
                        },
                        district: { 
                           headerStyle: headerDark,
                           displayName: 'Distrito',
                           width: cellWidth
                        },
                        totalPurchases: {
                           headerStyle: headerDark,
                           displayName: 'Ticket registrados',
                           width: cellWidthSm
                        },
                        dataprotectionChecked: {
                           headerStyle: headerDark,
                           displayName: '¿Aceptó politica protección de datos?',
                           width: cellWidth,
                           cellFormat: function(value, row) {
                              return value ? 'sí' : 'no';
                           }
                        },
                        termsChecked: {
                           headerStyle: headerDark,
                           displayName: '¿Aceptó términos y condiciones?',
                           width: cellWidth,
                           cellFormat: function(value, row) {
                              return value ? 'sí' : 'no';
                           }
                        },
                        useofinfoChecked: {
                           headerStyle: headerDark,
                           displayName: '¿Aceptó uso de información?',
                           width: cellWidth,
                           cellFormat: function(value, row) {
                              return value ? 'sí' : 'no';
                           }
                        },
                        participantCreatedAt: { 
                           headerStyle: headerDark,
                           displayName: 'Fecha reg. participante',
                           width: cellWidth,
                        },
                     },
                     data: outputData // <-- Report data
                  }
               ]
            )

            fs.appendFile(OUTPUT_FILE, report, err => {
               console.error(err)

            })

            // You can then return this straight
            //res.attachment('report.xlsx'); // This is sails.js specific (in general you need to set headers)
            //return res.send(report);
         } catch (error) {
            console.error(error);

         }
      })
}

getData()